package com.demo.pepowworkerapp.Pager;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.demo.pepowworkerapp.Fragments.Completed;
import com.demo.pepowworkerapp.Fragments.In_Progress;
import com.demo.pepowworkerapp.Fragments.Schedule;

public class pager extends FragmentStatePagerAdapter {

    int tabCount;
    public pager(FragmentManager fm,int tabCount) {
        super(fm);
        this.tabCount= tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Schedule schedule = new Schedule();
                return schedule;
            case 1:
                In_Progress in_progress = new In_Progress();
                return in_progress;
            case 2:
                Completed completed = new Completed();
                return completed;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
