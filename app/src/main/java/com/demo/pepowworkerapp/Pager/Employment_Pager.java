package com.demo.pepowworkerapp.Pager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.demo.pepowworkerapp.Fragments.Completed;
import com.demo.pepowworkerapp.Fragments.Completed_Employment;
import com.demo.pepowworkerapp.Fragments.In_Progress;
import com.demo.pepowworkerapp.Fragments.In_Progress_Employment;
import com.demo.pepowworkerapp.Fragments.Schedule;
import com.demo.pepowworkerapp.Fragments.Schedule_Employment;

public class Employment_Pager extends FragmentStatePagerAdapter {
    int tabCount;
    public Employment_Pager(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount= tabCount;
    }
    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Schedule_Employment schedule_employment = new Schedule_Employment();
                return schedule_employment;
            case 1:
                In_Progress_Employment in_progress_employment = new In_Progress_Employment();
                return in_progress_employment;
            case 2:
                Completed_Employment completed_employment = new Completed_Employment();
                return completed_employment;
            default:
                return null;
        }
    }
    @Override
    public int getCount() {
        return tabCount;
    }
}
