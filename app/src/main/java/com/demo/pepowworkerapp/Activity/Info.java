package com.demo.pepowworkerapp.Activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.demo.pepowworkerapp.Adapter.RecyclerViewAdapter;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.InFo_Post;
import com.demo.pepowworkerapp.Utils.Dates;
import com.demo.pepowworkerapp.Utils.SharedPreferenceClass;
import com.demo.pepowworkerapp.network.RetrofitInterface;
import com.demo.pepowworkerapp.network.ServiceGenerator;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

import static android.content.res.Resources.getSystem;

public class Info extends AppCompatActivity implements BSImagePicker.OnSingleImageSelectedListener,RecyclerViewAdapter.OnItemClickListener,
        BSImagePicker.OnMultiImageSelectedListener, BSImagePicker.ImageLoaderDelegate{
    private ViewGroup mSelectedImagesContainer;
    int counter=0;
    Uri uri,uri1;
    Bitmap bitmap;
    Button img,save;
    String imageString;
     ArrayList<String> playerList = new ArrayList<String>();
    String playerlist[];
    ImageView defoult;
    ArrayList<Object> array_image1;
    public static final String KEY_YEAR = "skill_years";
    public static final String KEY_WHERE = "skill_where";
    public static final String KEY_ABOUT_WORK = "about_work";
    public static final String KEY_IMAGES = "skill_images";
    public static final String KEY_SKILL_ID = "skill_id";
    //------sharedpreference keys-------//
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String TEXT = "text";
    public static final String TEXT1 = "text1";
    public static final String TEXT2 = "text2";
    EditText nameField1,nameField2;
    Button btnAdd,save_and_add;
    TextView nameField;
    private SharedPreferenceClass sharedPreferenceClass;
    private String user_id;
    private String skill_id;
    private String text;
    private String text1;
    private String text2;
    TextInputLayout textinputyear,textinputwhere;
    SharedPreferences sharedPreferences;
    private JsonObject jsonObject;
    String m;
    String imageString1;
    private int count;
    private Dialog dialog;
    private DatePicker datePicker;
    private Calendar mCalendar;
    private String expMonth,expYear;
    private Button date_time_set;
    private Dates dates;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        dates = new Dates();
        sharedPreferenceClass = new SharedPreferenceClass(this);
        sharedPreferenceClass = new SharedPreferenceClass(Info.this);
        user_id = sharedPreferenceClass.getValue_string("user_id");
        skill_id = sharedPreferenceClass.getValue_string("skill_ido");
        defoult=findViewById(R.id.defoult);
        save_and_add=findViewById(R.id.save_and_add);
        textinputwhere=findViewById(R.id.textinputwhere);
        mCalendar = Calendar.getInstance();
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);
        month++;
        expMonth= String.valueOf(month);
        expYear= String.valueOf(year);
        loadIdDefault();
        //status bar colour change
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.themecolourblue));
        //Toolbar setup
        Toolbar toolbar_skill = (Toolbar) findViewById(R.id.toolbaradd);
        setSupportActionBar(toolbar_skill);
        //give a heading to toolbar
        getSupportActionBar().setTitle("Info Details");
        toolbar_skill.setTitleTextColor(Color.WHITE);
        toolbar_skill.setNavigationIcon(R.drawable.back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //backbutton implementation
        toolbar_skill.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(Info.this,Skill_Details.class);
                startActivity(intent);
                finish();
                onBackPressed();
            }
        });
        nameField = findViewById(R.id.pickdate);
        nameField1=(EditText)findViewById(R.id.edwhere);
        nameField2=(EditText)findViewById(R.id.edaboutwork);
        save=(Button) findViewById(R.id.save);
        nameField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initMonthPicker();;

            }

        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostInfo();
               //savePhoto(bitmap);
                Intent i = new Intent(getApplicationContext(),Skill_Details.class);
                startActivity(i);
                finish();
            }
        });
        img=findViewById(R.id.imagetake);
        inIt();
        permission();

        save_and_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //savePhoto();
                PostInfo();
                Intent intent = new Intent(Info.this,Info.class);
                startActivity(intent);
                finish();
            }
        });
        //Custom font style change
        fontChanged();

        nameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    public void initMonthPicker(){
        count=0;
        dialog = new Dialog(Info.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout);
        dialog.show();
        datePicker = (DatePicker) dialog.findViewById(R.id.date_picker);
        date_time_set = (Button) dialog.findViewById(R.id.date_time_set);
        datePicker.init(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH), null);
        datePicker.setMaxDate(System.currentTimeMillis());
        int year    = datePicker.getYear();
        int month   = datePicker.getMonth();
        int day     = datePicker.getDayOfMonth();

      /*  datePicker.init(year, month, day, new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int month_i = monthOfYear + 1;
                Log.e("selected month:", Integer.toString(month_i));
                //Add whatever you need to handle Date changes
            }
        });*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            int daySpinnerId = getSystem().getIdentifier("day", "id", "android");
            if (daySpinnerId != 0)
            {
                View daySpinner = datePicker.findViewById(daySpinnerId);
                if (daySpinner != null)
                {
                    daySpinner.setVisibility(View.GONE);
                }
            }

            int monthSpinnerId = getSystem().getIdentifier("month", "id", "android");
            if (monthSpinnerId != 0)
            {
                View monthSpinner = datePicker.findViewById(monthSpinnerId);
                if (monthSpinner != null)
                {
                    monthSpinner.setVisibility(View.GONE);
                }
            }

            int yearSpinnerId = getSystem().getIdentifier("year", "id", "android");
            if (yearSpinnerId != 0)
            {
                View yearSpinner = datePicker.findViewById(yearSpinnerId);
                if (yearSpinner != null)
                {
                    yearSpinner.setVisibility(View.VISIBLE);
                }
            }
        } else { //Older SDK versions
            Field f[] = datePicker.getClass().getDeclaredFields();
            for (Field field : f)
            {
                if(field.getName().equals("mDayPicker") || field.getName().equals("mDaySpinner"))
                {
                    field.setAccessible(true);
                    Object dayPicker = null;
                    try {
                        dayPicker = field.get(datePicker);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    ((View) dayPicker).setVisibility(View.GONE);
                }

                if(field.getName().equals("mMonthPicker") || field.getName().equals("mMonthSpinner"))
                {
                    field.setAccessible(true);
                    Object monthPicker = null;
                    try {
                        monthPicker = field.get(datePicker);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    ((View) monthPicker).setVisibility(View.GONE);
                }

                if(field.getName().equals("mYearPicker") || field.getName().equals("mYearSpinner"))
                {
                    field.setAccessible(true);
                    Object yearPicker = null;
                    try {
                        yearPicker = field.get(datePicker);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    ((View) yearPicker).setVisibility(View.VISIBLE);
                }
            }
        }
        date_time_set.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view){
                dialog.dismiss();
               // Integer month = datePicker.getMonth()+1;
                Integer year = datePicker.getYear();
               // expMonth = (month.toString().length() == 1 ? "0" + month.toString() : month.toString());
                expYear = year.toString();
                try {
                    dates.convertStringToMonthYear(expYear);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Log.d("expMonth",expYear);
                String dateMonth = dates.getformattedTime();
                nameField.setText(expYear);

            }
        });
    }


    private void fontChanged() {
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/Uber Move Text.ttf");
        nameField.setTypeface(font);
        nameField1.setTypeface(font);
        nameField2.setTypeface(font);
        save_and_add.setTypeface(font);
        save.setTypeface(font);

        textinputwhere.setTypeface(font);
        img.setTypeface(font);

    }
    private void inIt() {
        mSelectedImagesContainer = (ViewGroup) findViewById(R.id.selected_photos_container);
        img = findViewById(R.id.imagetake);
    }
    /*checking permission for multiple image picker*/
    private void permission() {
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dexter.withActivity(Info.this)
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    BSImagePicker multiSelectionPicker = new BSImagePicker.Builder("com.demo.pepowworkerapp.fileprovider")
                                            .isMultiSelect() //Set this if you want to use multi selection mode.
                                            .setMinimumMultiSelectCount(1) //Default: 1.
                                            .setMaximumMultiSelectCount(5) //Default: Integer.MAX_VALUE (i.e. User can select as many images as he/she wants)
                                            .setMultiSelectBarBgColor(android.R.color.white) //Default: #FFFFFF. You can also set it to a translucent color.
                                            .setMultiSelectTextColor(R.color.primary_text) //Default: #212121(Dark grey). This is the message in the multi-select bottom bar.
                                            .setMultiSelectDoneTextColor(R.color.colorAccent) //Default: #388e3c(Green). This is the color of the "Done" TextView.
                                            .setOverSelectTextColor(R.color.error_text) //Default: #b71c1c. This is the color of the message shown when user tries to select more than maximum select count.
                                            .disableOverSelectionMessage() //You can also decide not to show this over select message.
                                            .build();
                                    multiSelectionPicker.show(getSupportFragmentManager(), "picker");
                                }

                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    showSettingsDialog();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }

                        }).check();
            }
        });
    }
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Info.this);
        builder.setTitle("title");
        builder.setMessage("message");

        // Set language level to 8 if error shows.

        builder.setPositiveButton("go to setting", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Info.this.openSettings();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }
        private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);

      }
      /*loading images into glid view*/
      @Override
       public void loadImage(File imageFile, ImageView ivImage) {
           Glide.with(Info.this).load(imageFile).into(ivImage);
       }
      /*after selecting images */
       @Override
       public void onMultiImageSelected(List<Uri> uriList, String tag) {
        mSelectedImagesContainer.removeAllViews();
        mSelectedImagesContainer.setVisibility(View.VISIBLE);
        int wdpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
        int htpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, getResources().getDisplayMetrics());
        for (Uri uri : uriList) {
            View imageHolder = LayoutInflater.from(this).inflate(R.layout.image_item, null);
            ImageView thumbnail = (ImageView) imageHolder.findViewById(R.id.media_image);
            Glide.with(this)
                    .load(uri.toString())
                    .into(thumbnail);
            Log.d("imagess", String.valueOf(uri));
            try {
                // You can update this bitmap to your server
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] imageBytes = baos.toByteArray();
                 imageString1 = Base64.encodeToString(imageBytes, Base64.DEFAULT);

                counter++;
                mSelectedImagesContainer.addView(imageHolder);
                thumbnail.setLayoutParams(new FrameLayout.LayoutParams(wdpx, htpx));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
        private void PostInfo() {
        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put("skill_years",nameField.getText().toString());
        hashMap.put("skill_where",nameField1.getText().toString());
        hashMap.put("about_work",nameField2.getText().toString());
        hashMap.put("skill_id",skill_id);
        hashMap.put("user_id",user_id);
        hashMap.put("skill_images","");
        Toast.makeText(Info.this,"one", Toast.LENGTH_SHORT).show();
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://newsunshinegroups.com/pepow/api/");
        Call<InFo_Post> call = jsonpost.post(hashMap);
        call.enqueue(new Callback<InFo_Post>() {
            @Override
            public void onResponse(Call<InFo_Post> call, retrofit2.Response<InFo_Post> response) {
                if (response.isSuccessful()){
                    Log.d("response", String.valueOf(response.body().getMsg()));
                    Toast.makeText(Info.this, "success", Toast.LENGTH_SHORT).show();
                }else {
                    Log.d("else", String.valueOf(response.errorBody()));
                    Log.d("else1",response.message());
                    Toast.makeText(Info.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InFo_Post> call, Throwable t) {
                Log.d("Onfail",t.getMessage());
                Toast.makeText(Info.this, "failed to uploading images", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void loadIdDefault() {
        Picasso.with(this).load(R.drawable.uploadphoto)
                .into(defoult);
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {

    }

    @Override
    public void onItemClick(RecyclerViewAdapter.MyViewHolder item, int position) {

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_option, menu);
        return false;

    }


}
