package com.demo.pepowworkerapp.Activity;

import android.content.Intent;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.demo.pepowworkerapp.R;

public class Success_Activity_Password_Change extends AppCompatActivity {
    Button loginbtn;
    TextView tvsucc,tvsuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success___password__change);
        loginbtn=findViewById(R.id.ok1);
        tvsucc=findViewById(R.id.tvsucc);
        tvsuccess=findViewById(R.id.tvsuccess);

        //status bar colour change
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));

        //custom font style
        fontChanged();


    }
    //custom font style
    private void fontChanged() {
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/Uber Move Text.ttf");
        tvsucc.setTypeface(font);
        tvsuccess.setTypeface(font);
        loginbtn.setTypeface(font);
    }

    public void loginbtn(View view)
    {
        Intent intent=new Intent(Success_Activity_Password_Change .this,LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
