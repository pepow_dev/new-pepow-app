package com.demo.pepowworkerapp.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Utils.InternetConnection;
import com.demo.pepowworkerapp.Utils.SharedPreferenceClass;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotpasswordPhoneActivity extends AppCompatActivity {
    private EditText et_num1;
    private Button button;
    private String user_id;
    String phone_num;
    SharedPreferences sharedPreferences;
    private SharedPreferenceClass sharedPreferenceClass;
    private InternetConnection internetConnection;
    public static String CHECK_NUMBER_URL= "http://newsunshinegroups.com/pepow/api/check_mobileno.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword_phone);
        et_num1=findViewById(R.id.etnum1);
        button=findViewById(R.id.submit1);
        Typeface custom_font = Typeface.createFromAsset(getAssets(),"fonts/Uber Move Text.ttf");
        button.setTypeface(custom_font);
        internetConnection = new InternetConnection(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        //status bar colour change
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
        //custom font style
        fontChanged();
        sharedPreferenceClass = new SharedPreferenceClass(this);
        sharedPreferenceClass = new SharedPreferenceClass(ForgotpasswordPhoneActivity.this);
        user_id = sharedPreferenceClass.getValue_string("user_id");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences=getApplicationContext().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=sharedPreferences.edit();
                    editor.putString("phone",et_num1.getText().toString());
                    editor.apply();
                    numverify();
                }
            });
        }

        private void fontChanged() {
            Typeface font = Typeface.createFromAsset(getAssets(),"fonts/Uber Move Text.ttf");
            et_num1.setTypeface(font);
        }

        /*validation for entered number*/
        private void numverify() {
            String phoneNumber = "";
              if(et_num1.getText().toString().equals(""))
            {
                Snackbar.make(findViewById(android.R.id.content), "Enter your mobile no.", Snackbar.LENGTH_LONG).show();
                et_num1.requestFocus();
                et_num1.setCursorVisible(true);

            }
            else if(et_num1.length() <10 )
            {
                Snackbar.make(findViewById(android.R.id.content), "Phone number should be 10 digits", Snackbar.LENGTH_LONG).show();
                et_num1.requestFocus();
                et_num1.setCursorVisible(true);
            }
            else if(phoneNumber.matches("^[7-9][0-9]{9}$"))
            {
                Snackbar.make(findViewById(android.R.id.content), "Invalid number ", Snackbar.LENGTH_LONG).show();
            }

            else{
                if (internetConnection.isConnectingToInternet())
                {
                    nextt();
                    sharedPreferences=getApplicationContext().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor=sharedPreferences.edit();
                    editor.putString("phone",et_num1.getText().toString());
                    editor.apply();
                }
                else
                {
                    Toast.makeText(ForgotpasswordPhoneActivity.this, "Check your network conection", Toast.LENGTH_LONG).show();
                }
            }

        }
        /*sending data into server*/
        private void nextt() {
            StringRequest stringRequest = new StringRequest(Request.Method.POST,CHECK_NUMBER_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("msg");
                        Toast.makeText(getApplicationContext(),status + " "  +message + "  ",Toast.LENGTH_LONG).show();

                        if (status.equalsIgnoreCase("Success")) {
                               String s1=et_num1.getText().toString();
                               Intent intent = new Intent(ForgotpasswordPhoneActivity.this, Forgot_Password_Otp_Activity.class);
                               intent.putExtra("9876543212",s1);
                               startActivity(intent);
                               finish();
                        }
                        else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(ForgotpasswordPhoneActivity.this, error.toString(), Toast.LENGTH_LONG).show();

                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", user_id);
                    params.put("phone_no", et_num1.getText().toString());
                    return params;
                }
            };
            stringRequest.setShouldCache(false);
            RequestQueue requestQueue = Volley.newRequestQueue(ForgotpasswordPhoneActivity.this);
            requestQueue.add(stringRequest);
        }


    }

