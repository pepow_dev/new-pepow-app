package com.demo.pepowworkerapp.Activity;

import android.content.Intent;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Utils.SharedPreferenceClass;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 7000;
    ImageView img;
    private SharedPreferenceClass sharedPreferenceClass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        img=findViewById(R.id.image);
        sharedPreferenceClass = new SharedPreferenceClass(SplashActivity.this);
        Animation anim = AnimationUtils.loadAnimation(this,R.anim.myanimation);
        img.setAnimation(anim);
//status bar colour change
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               String user_id = sharedPreferenceClass.getValue_string("user_id");
              if (!user_id.equals("")) {
                    Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }

                else {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }


            }
        },SPLASH_TIME_OUT);
    }
}
