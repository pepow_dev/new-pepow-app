package com.demo.pepowworkerapp.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Utils.InternetConnection;
import com.demo.pepowworkerapp.Utils.SharedPreferenceClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Password_Change_Activity extends AppCompatActivity {
    EditText et_pwd;
    EditText et_repwd;
    Button btnsubmit;
    private InternetConnection internetConnection;
    String pass,cnpasss,phone;
    public static final String KEY_PHONENO = "phone_no";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_CONFIRMPASSWORD = "confirmpassword";
    private SharedPreferences sharedPreferences;
    private SharedPreferenceClass sharedPreferenceClass;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password__change_);
        et_pwd = findViewById(R.id.newpwd);
        et_repwd = findViewById(R.id.cnfpwd);
        btnsubmit = findViewById(R.id.sub);
        internetConnection = new InternetConnection(this);
        sharedPreferenceClass = new SharedPreferenceClass(this);


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        //status bar colour change
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
//custom font style
        fontChanged();
        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pwdvalidation();


            }
        });
    }
    //custom font style
    private void fontChanged() {
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/Uber Move Text.ttf");
        et_pwd.setTypeface(font);
        et_repwd.setTypeface(font);
        btnsubmit.setTypeface(font);
    }

    /*validation for entered password*/
    private void pwdvalidation() {
        String passwordInput = et_pwd.getText().toString().trim();
        final String password = et_pwd.getText().toString();
        final String confirmpassword = et_repwd.getText().toString();


        if (et_pwd.getText().toString().equals("")) {
            Snackbar.make(findViewById(android.R.id.content), "Field can't be empty", Snackbar.LENGTH_LONG).show();
            et_pwd.requestFocus();
            et_pwd.setCursorVisible(true);


        } else if (et_pwd.toString().length() <= 7) {
            Snackbar.make(findViewById(android.R.id.content), "Minimum 8 characters", Snackbar.LENGTH_LONG).show();
            et_pwd.requestFocus();
            et_pwd.setCursorVisible(true);

        } else if (et_repwd.getText().toString().equals("")) {
            Snackbar.make(findViewById(android.R.id.content), "Re-enter your password ", Snackbar.LENGTH_LONG).show();

            et_repwd.requestFocus();
            et_repwd.setCursorVisible(true);
        } else if ((et_pwd.getText().toString().equals("")) || et_repwd.getText().toString().equals("")) {
            Snackbar.make(findViewById(android.R.id.content), "Both passwords are not matching", Snackbar.LENGTH_LONG).show();
            et_pwd.requestFocus();
            et_pwd.setCursorVisible(true);
        }
        else if ((et_repwd.length() <= 7)) {
            Snackbar.make(findViewById(android.R.id.content), "Minimum 8 characters", Snackbar.LENGTH_LONG).show();
            et_repwd.requestFocus();
            et_repwd.setCursorVisible(true);

        } else if ((et_pwd.length() <= 7) || et_repwd.length() <= 7) {
            Snackbar.make(findViewById(android.R.id.content), "Minimum 8 characters", Snackbar.LENGTH_LONG).show();
            et_pwd.requestFocus();
            et_pwd.setCursorVisible(true);


        } else if (passwordInput.isEmpty()) {
            Snackbar.make(findViewById(android.R.id.content),
                    "Field can't be empty", Snackbar.LENGTH_LONG).show();
            et_pwd.requestFocus();
            et_pwd.setCursorVisible(true);


        } else if (!confirmpassword.equals(password)) {

            Snackbar.make(findViewById(android.R.id.content), "Both passwords are not matching", Snackbar.LENGTH_LONG).show();
        }
        else {

            if (internetConnection.isConnectingToInternet()) {
                submit();
            } else {
                Snackbar.make(findViewById(android.R.id.content), "Check your internet connection", Snackbar.LENGTH_LONG).show();
            }
        }

    }
    /*submition of entered password for changed*/
    private void submit() {
        sharedPreferences=getApplicationContext().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        final   String phno=sharedPreferences.getString("phone", "");
        final String password = et_pwd.getText().toString();
        final String confirmpassword = et_repwd.getText().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://newsunshinegroups.com/pepow/api/forgotpassword.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("msg");
                    Toast.makeText(getApplicationContext(),status + " "  +message + "  ",Toast.LENGTH_LONG).show();

                    if (status.equalsIgnoreCase("Success")) {
                        String  ph_no=jsonObject.getString("phone_no");

                        sharedPreferenceClass.setPhone_num(ph_no);


                    } else {

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                // params.put(KEY_PHONE,phone);
                //params.put("uniq_id",user_id);
                params.put(KEY_PHONENO,phno);
                params.put(KEY_PASSWORD,password);
                params.put(KEY_CONFIRMPASSWORD, confirmpassword);


                return params;
            }
        };
        stringRequest.setShouldCache(false);
        int socketTimeout = 30000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        stringRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(Password_Change_Activity.this);
        requestQueue.add(stringRequest);
       successPasswordChange();




    }
/*after successfully openning new activity*/
    private void successPasswordChange() {
        Intent btn = new Intent(Password_Change_Activity.this,Success_Activity_Password_Change.class);
        startActivity(btn);
        finish();
    }


}

