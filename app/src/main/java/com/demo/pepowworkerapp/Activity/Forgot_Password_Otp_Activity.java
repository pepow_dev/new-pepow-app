package com.demo.pepowworkerapp.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Utils.InternetConnection;
import com.demo.pepowworkerapp.Utils.Server_Links;
import com.goodiebag.pinview.Pinview;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Forgot_Password_Otp_Activity extends AppCompatActivity {
    public static final String KEY_SUBMITOTP = "submit_otp";
    public static String NEW_BASE_URL= "http://newsunshinegroups.com/pepow/api/user_otp_validate.php";
    private Button submit1;
    TextView textotp;
    public String TAG = "SignuptwoActivity";
    Pinview pinview;
    PinEntryEditText pinEntry;
    private String newotp,newphone;
    String entered_otp=" ";
    public static String NEW_OTP_URL= "http://newsunshinegroups.com/pepow/api/user_otp_validate.php";
    SmsVerifyCatcher smsVerifyCatcher;
    TextView tv1,timmer,resend;
    private CountDownTimer cTimer;
    SharedPreferences sharedPreferences;
    private InternetConnection internetConnection;
    private String phone_no;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot__password__otp_);
        savedInstanceState = getIntent().getExtras();
        String s2 = savedInstanceState.getString(("9876543212"));
        tv1 = findViewById(R.id.textnum1);
        timmer = findViewById(R.id.timmerforgot);
        resend=findViewById(R.id.resendforgot);
        textotp=findViewById(R.id.textotp);

        tv1.setText(s2);
        submit1 = findViewById(R.id.pinviewsubmit1);
        pinEntry = (PinEntryEditText) findViewById(R.id.txt_pin_entry1);
        internetConnection = new InternetConnection(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        //status bar colour change
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
//custom font style
        fontChanged();
        if (pinEntry != null) {
            pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {

                    entered_otp = str.toString();
                    if (str.toString().equalsIgnoreCase("otp")) {
                        timmer.setVisibility(View.GONE);
                        cTimer.cancel();



                    } else {
                        resend.setVisibility(View.GONE);
                        timmer.setVisibility(View.GONE);
                        cTimer.cancel();

                    }
                }
            });

        }
        else {
            resend.setVisibility(View.VISIBLE);

        }
        submit1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpgenerate1();
                sharedPreferences=getApplicationContext().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putString("submitotp",pinEntry.getText().toString());
                editor.apply();
            }
        });

        cTimer =    new CountDownTimer(60000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {
                timmer.setText(""+String.format("%d min %d sec",
                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                timmer.setVisibility(View.GONE);
                cTimer.cancel();
                resend.setVisibility(View.VISIBLE);

            }
        }.start();
        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = parseCode(message);//Parse verification code
                Log.d("Agilanbu OTP", code);
                pinEntry.setText(code);//set code in edit text
            }
        });

    }

    private void fontChanged() {
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/Uber Move Text.ttf");
        pinEntry.setTypeface(font);
        textotp.setTypeface(font);
        resend.setTypeface(font);
        timmer.setTypeface(font);
        tv1.setTypeface(font);
        submit1.setTypeface(font);
    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }
    @Override
    protected void onStart() {
        smsVerifyCatcher.onStart();
        super.onStart();
    }
    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    private void otpgenerate1() {
        if(entered_otp.equalsIgnoreCase("")){
            Toast.makeText(getApplicationContext(), "please enter otp", Toast.LENGTH_SHORT).show();
        }
        else if(entered_otp.length() !=6)
        {
            Toast.makeText(getApplicationContext(),"please enter otp",Toast.LENGTH_LONG).show();
        }
        else {
            if (internetConnection.isConnectingToInternet()) {
                otpvalidate();

            }

        }
    }
    private void otpvalidate() {
        final String otp =entered_otp;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Server_Links.OTP_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    Toast.makeText(getApplicationContext(), status + " " + message + "  ", Toast.LENGTH_LONG).show();

                    if (status.equalsIgnoreCase("success")) {
                        Intent intent = new Intent(Forgot_Password_Otp_Activity.this, Password_Change_Activity.class);
                        startActivity(intent);
                        finish();
                    }
                    else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Forgot_Password_Otp_Activity.this, error.toString(), Toast.LENGTH_LONG).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put(KEY_SUBMITOTP, otp);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        int socketTimeout = 300;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(Forgot_Password_Otp_Activity.this);
        requestQueue.add(stringRequest);
    }
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                pinEntry.setText(message);
                //Do whatever you want with the code here
            }
        }
    };

    public void resend(View view) {
        next();
        resend.setVisibility(View.GONE);
        cTimer.start();
        timmer.setVisibility(View.VISIBLE);

    }
    private void next() {
        tv1.getText();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, NEW_OTP_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    Toast.makeText(getApplicationContext(),  " ggg"+message,Toast.LENGTH_LONG).show();

                    if (status.equalsIgnoreCase("success")) {
                        phone_no = jsonObject.getString("phone_no");
                        tv1.getText();
                        cTimer.cancel();
                        timmer.setVisibility(View.GONE);


                    }

                    else {
                        resend.setVisibility(View.VISIBLE);
                        //  Toast.makeText(Regd_phone_Activity.this,"Phone number  already exist", Toast.LENGTH_LONG).show();

                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(Regd_phone_Activity.this, "Check Your Internet Connection", Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("phone_no", tv1.getText().toString());
                // params.put("submit_otp", et_otp.getText().toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        RequestQueue requestQueue = Volley.newRequestQueue(Forgot_Password_Otp_Activity.this);
        requestQueue.add(stringRequest);

    }
}


