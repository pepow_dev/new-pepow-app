package com.demo.pepowworkerapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.demo.pepowworkerapp.Adapter.Info_Adapter;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.DataList;
import com.demo.pepowworkerapp.Response.Info_Response;
import com.demo.pepowworkerapp.Utils.SharedPreferenceClass;
import com.demo.pepowworkerapp.network.RetrofitInterface;
import com.demo.pepowworkerapp.network.ServiceGenerator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class Info_Deatils extends AppCompatActivity {
    public RecyclerView recyclerView;
    private String skill_id;
    public Info_Adapter info_adapter;
    private SharedPreferenceClass sharedPreferenceClass;
    Toolbar toolbar;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info__deatils);
        toolbar=findViewById(R.id.tool);
        toolbar.setTitle("Detail's");
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(Info_Deatils.this,Skill_Details.class);
                startActivity(intent);
                finish();
                onBackPressed();
            }
        });
        recyclerView=findViewById(R.id.recyclerview);

        //status bar colour change
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.themecolourblue));
        //get the skillid from sharedpreference class
        sharedPreferenceClass = new SharedPreferenceClass(this);
        sharedPreferenceClass = new SharedPreferenceClass(Info_Deatils.this);
        skill_id = sharedPreferenceClass.getValue_string("skill_ido");
        progressDialog=new ProgressDialog(this);
        progressDialog.setTitle("Loading....");
        progressDialog.setMessage("Retrieving data from server");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);
        progressDialog.setMax(20);
        progressDialog.show();
        Infoget();
    }
    /* getting info details with retrofit get api*/
    private void Infoget() {
        Map<String, String> data = new HashMap<>();
        data.put("skill_id",skill_id);
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://newsunshinegroups.com/");
        Call<Info_Response> call = jsonpost.postRawJSON6(data);
        call.enqueue(new Callback<Info_Response>() {
            @Override
            public void onResponse(Call<Info_Response> call, retrofit2.Response<Info_Response> response) {
                if (response.isSuccessful()){
                    progressDialog.dismiss();
                    List<DataList> dataLists= response.body().getData();
                    Log.d("list", String.valueOf(response.body().getData()));
                    info_adapter=new Info_Adapter(Info_Deatils.this,dataLists);
                    recyclerView.setLayoutManager(new LinearLayoutManager(Info_Deatils.this, LinearLayoutManager.VERTICAL, false));
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setAdapter(info_adapter);

                }else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<Info_Response> call, Throwable t) {
                progressDialog.dismiss();
                Log.d("error",t.getMessage());
                Toast.makeText(Info_Deatils.this, "something went wrong", Toast.LENGTH_SHORT).show();

            }
        });
    }
}
