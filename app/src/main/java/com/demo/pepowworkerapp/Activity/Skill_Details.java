package com.demo.pepowworkerapp.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.nfc.Tag;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.demo.pepowworkerapp.Adapter.AutoCompleteAdapter;
import com.demo.pepowworkerapp.Adapter.Info_Adapter;
import com.demo.pepowworkerapp.Adapter.PlaceAutoSuggestAdapter;
import com.demo.pepowworkerapp.Adapter.RecyclerViewAdapter;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.DataList;
import com.demo.pepowworkerapp.Response.Info_Response;
import com.demo.pepowworkerapp.Utils.InternetConnection;
import com.demo.pepowworkerapp.Utils.SharedPreferenceClass;
import com.demo.pepowworkerapp.network.RetrofitInterface;
import com.demo.pepowworkerapp.network.ServiceGenerator;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import mabbas007.tagsedittext.TagsEditText;
import retrofit2.Call;
import retrofit2.Callback;


public class Skill_Details extends AppCompatActivity implements TagsEditText.TagsEditListener, View.OnClickListener,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private SharedPreferenceClass sharedPreferenceClass;
    TextView tvexpr;
    TextInputLayout langtextinput,wrktextinput;
    private String user_id;
    private Button locationdialog;
    Button nextbutton;
    SharedPreferences sharedPreferences;
    private InternetConnection internetConnection;
    private TagsEditText mTagsEditText,editText;
    //Area autofill initialisation
    AutoCompleteAdapter adapter_loc;
    PlacesClient placesClient;
    Geocoder geocoder;
    List<Address> addresses;
    LatLng p1 = null;
    //----SKILL DETAILS KEY PARAMETRES
    String[] skills = {"Carpenter","Electrician","Mason","Painter","Driver","Plumber","Mechanic","Gardener","Labourer"};
    public static final String KEY_PRIMARY_SKILL = "primary_skill";
    public static final String KEY_SECONDRY_SKILL = "secondary_skill";
    public static final String KEY_EXPERIENCE_IN_YEAR = "experience_in_year";
    public static final String KEY_LANGUAGE_PROFICIENCY = "language_proficiency";
    public static final String KEY_WORK_LOCATION = "work_location";
    public static final String KEY_MY_EXPERTISE = "my_expertise";
    //------sharedpreference keys-------//
    public static final String PRIMARY = "primary";
    public static final String SECONDRY = "secondary";
    public static final String LANGUAGE = "language";
    public static final String WORKEX = "work";
    public static final String MYEXPERTISE = "myexp";
    public static final String TEXT5 = "text5";
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String One = "one";
    String  ss;
    private String text;
    private String text1;
    private String text2;
    private String text3;
    private String text4;
    private String text5;
    AutoCompleteTextView actv,actv1;
    ArrayAdapter<String> adapter,adapter1,tagAdapter;
    EditText editText3;
    String selectedItemText;
    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private String lat, lon;
    private static final int REQUEST_CODE_PERMISSION = 2;
    private String mPermission = android.Manifest.permission.ACCESS_FINE_LOCATION;
    private String add = "";
    Dialog dialog;
    String location,l;
    AutoCompleteTextView mAutocomplete;
    Button addlocation;
    Spinner spinner_yrs;
    //Editable Functionality
    private MenuItem editmenu;
    private MenuItem savemenu;
    Button nextactivity;
    ProgressDialog progressDialog;
    FloatingActionButton floatingActionButton;
    private int mSpacing = 20, mButtonHeight = 0, mButtonWidth = 0, mButton1Bottom = 0, mButton1Left = 0, mButton2Bottom = 0, mButton2Left = 0, mButton3Bottom = 0, mButton3Left = 0, mTextViewHeight = 200, mTextViewTop = 0, mTextViewLeft = 0;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skill__details);
        nextactivity=findViewById(R.id.nextactivty);
        SharedPreferences sharedPreferences1 = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        ss = sharedPreferences1.getString("OS", null);
        adapter = new ArrayAdapter<String> (this, android.R.layout.select_dialog_item, skills);
        actv =  findViewById(R.id.autoCompleteTextView);
        floatingActionButton=findViewById(R.id.fab_skill);
        locationdialog=findViewById(R.id.locationdialog);
        spinner_yrs = (Spinner)findViewById(R.id.secondary_spinner);
        geocoder = new Geocoder(this, Locale.getDefault());
        actv.setThreshold(1);
        actv.setAdapter(adapter);
        progressDialog=new ProgressDialog(Skill_Details.this);
        /*hastag text implementation in edittext*/
        actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
                if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Plumber")){
                    editText3.setHint("#Assemble"+ "#install" +"#repair pipes" +"#fittings");
                }else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Carpenter")) {
                    editText3.setHint("#Framing"+ "#formwork" +"#roofing");
                }else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Electrician")){
                    editText3.setHint("#Installation"+ "#Maintenance" +"#Machine Repairer & Rewinder");
                }else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Mason")){
                    editText3.setHint("#fireplaces"+ "#floors" +"#chimneys" +"#repair walls");
                }else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Painter")){
                    editText3.setHint("#painters"+ "#maintenance painters" +"#artisan painters" +"#painting and coating");
                }else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Driver")){
                    editText3.setHint("#Taxicab driver"+ "#Bus driver" +"#Pay driver" +"#Delivery" +"#Truck driver" +"#Motorman");
                }else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Mechanic")){
                    editText3.setHint("#auto"+ "#bicycle" +"#boiler" +"#general" +"#industrial maintenance" +"#Motorcycle");
                }else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Gardener")){
                    editText3.setHint("#caretaker"+ "#greenskeeper" +"#landscaper" +"#nurseryman" +"#seedsman");
                }else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("Labourer")){
                    editText3.setHint("#concrete cutting"+ "#cutting torch" +"#gunite");
                }
            }
        });
        flotingmethod();
        actv.setTextColor(Color.BLACK);
        adapter1 = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, skills);
        actv1=  findViewById(R.id.autoCompleteTextView2);
        actv1.setThreshold(1);
        actv1.setAdapter(adapter1);
        actv1.setTextColor(Color.BLACK);
        editText=findViewById(R.id.editText);
        editText3=findViewById(R.id.editText3);
        tvexpr=findViewById(R.id.textView);
        langtextinput=findViewById(R.id.langtextinput);
        wrktextinput=findViewById(R.id.wrktextinput);
        spinner_yrs = (Spinner)findViewById(R.id.secondary_spinner);

        //status bar colour change
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.themecolourblue));
        //Toolbar setup
        Toolbar toolbar_skill = (Toolbar) findViewById(R.id.toolbar_add);
        setSupportActionBar(toolbar_skill);
        //give a heading to toolbar
        getSupportActionBar().setTitle("Skill Details");
        toolbar_skill.setTitleTextColor(Color.WHITE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //backbutton implementation
        toolbar_skill.setNavigationIcon(R.drawable.arrowback);
        toolbar_skill.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        nextbutton=findViewById(R.id.nextbutton);
        nextbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setTitle("Loading....");
                progressDialog.setMessage("Sending data to server");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setProgress(0);
                progressDialog.setMax(20);
                progressDialog.show();
                postskill();
                saveData();
                dataDisabled();

            }
        });
        //get the userid from sharedpreference class
        sharedPreferenceClass = new SharedPreferenceClass(this);
        sharedPreferenceClass = new SharedPreferenceClass(Skill_Details.this);
        user_id = sharedPreferenceClass.getValue_string("user_id");
        internetConnection = new InternetConnection(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        // Get reference of widgets from XML layout
         Spinner spinner_yrs = (Spinner)findViewById(R.id.secondary_spinner);
        // Initializing a String Array
        String[] itemNames = getResources().getStringArray(R.array.years);
        final List<String> idskill = new ArrayList<>(Arrays.asList(itemNames));
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> idAdapter = new ArrayAdapter<String>(this,R.layout.spinner,idskill)
        {
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view=super.getDropDownView(position,convertView,parent);
                TextView tv_secondary_skill=(TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv_secondary_skill.setTextColor(Color.GRAY);
                }
                else {
                    tv_secondary_skill.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        idAdapter.setDropDownViewResource(R.layout.spinner);
        spinner_yrs.setAdapter(idAdapter);
        spinner_yrs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                 selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if(position > 0){
                    // Notify the selected item text

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mTagsEditText = (TagsEditText) findViewById(R.id.tagsEditText);
        mTagsEditText.setThreshold(1);
        mTagsEditText.setTagsListener(this);
        mTagsEditText.setTagsWithSpacesEnabled(true);
        mTagsEditText.setTagsBackground(R.drawable.square_default);
        mTagsEditText.setCloseDrawableRight(R.drawable.tag_close);
        editText.setTagsListener(this);
        editText.setTagsWithSpacesEnabled(true);
       // editText.setThreshold(1);
       // editText.setTagsBackground(R.drawable.square_default);
       // editText.setCloseDrawableRight(R.drawable.tag_close);
        tagAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.language));
        mTagsEditText.setAdapter(tagAdapter);
        //editText.setAdapter(tagAdapter);

        loadData();
        checkLocationPermission();
        locationdialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog=new Dialog(Skill_Details.this);
                dialog.setContentView(R.layout.location);
                addlocation=dialog.findViewById(R.id.addlocation);
                mAutocomplete=dialog.findViewById(R.id.autoloc);
                /*google key initialization*/
                String apiKey = getString(R.string.api_key);
                if(apiKey.isEmpty()){
                    editText.setText(getString(R.string.error));
                    return;
                }
                // Setup Places Client
                if (!Places.isInitialized()) {
                    Places.initialize(getApplicationContext(),apiKey);
                }
                placesClient = Places.createClient(getApplicationContext());
                initAutoCompleteTextView();

                addlocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    location=mAutocomplete.getText().toString();
                        SharedPreferences sharedPreferences = getSharedPreferences("pppp", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("location",location);
                     editor.apply();
                        SharedPreferences sharedPreferences2 = getSharedPreferences("pppp", MODE_PRIVATE);
                        l = sharedPreferences.getString("location",null);
                        String location = sharedPreferences.getString("location1",null);
                        if (l!=null){
                            editText.setText(l);
                        }
                       else if (location!=null){
                            String[] ll = location.split("\\s+");
                            for(int i = 0;i<ll.length;i++){
                                editText.setText(ll[i]);
                                editText.setEnabled(false);
                                editText.setTagsBackground(R.drawable.square_default_one);
                                editText.setCloseDrawableRight(R.color.grey);

                            }

                        }
                    dialog.cancel();

                    }
                });

                getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
                dialog.show();
            }
        });
        //Custom font style change
        fontChanged();
        nextactivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Skill_Details.this,Info_Deatils.class);
                startActivity(intent);
                finish();
            }
        });
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean firstOpened = preferences.getBoolean("ranBefore", true);
        if(firstOpened) {
            showOverlay();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("ranBefore", false);
            editor.apply();
        }
        }

    private void flotingmethod() {
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setTitle("Loading....");
                progressDialog.setMessage("Sending data to server");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setProgress(0);
                progressDialog.setMax(20);
                progressDialog.show();
                postskill1();
                saveData();
                dataDisabled();

            }
        });
    }

    private void showOverlay() {
            final Dialog dialog = new Dialog(Skill_Details.this, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.setContentView(R.layout.overlay_view);
            RelativeLayout layout = (RelativeLayout) dialog.findViewById(R.id.overlayLayout);
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    dialog.dismiss();
                }
            });

            dialog.show();

            LinearLayout button1Layout = (LinearLayout) dialog.findViewById(R.id.button1Layout);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(mButton3Left + mButtonWidth/2, mButton1Bottom + mButtonHeight + mSpacing, 0, 0);
            button1Layout.setLayoutParams(params);

        }
        private void initAutoCompleteTextView() {
            mAutocomplete.setThreshold(3);
            mAutocomplete.setOnItemClickListener(autocompleteClickListener);
            adapter_loc = new AutoCompleteAdapter(this, placesClient);
            mAutocomplete.setAdapter(adapter_loc);
        }
        private AdapterView.OnItemClickListener autocompleteClickListener=new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                  final  AutocompletePrediction item=adapter_loc.getItem(i);
                    String placeID = null;
                    if (item != null) {
                        placeID = item.getPlaceId();
                    }
    //                To specify which data types to return, pass an array of Place.Fields in your FetchPlaceRequest
    //                Use only those fields which are required.
                    List<com.google.android.libraries.places.api.model.Place.Field> placeFields = Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.ID, com.google.android.libraries.places.api.model.Place.Field.NAME, com.google.android.libraries.places.api.model.Place.Field.ADDRESS
                            , Place.Field.LAT_LNG);
                    FetchPlaceRequest request = null;
                    if (placeID != null) {
                        request = FetchPlaceRequest.builder(placeID, placeFields)
                                .build();
                    }
                    if (request != null)
                    {
                        placesClient.fetchPlace(request).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
                            @SuppressLint("SetTextI18n")
                            @Override
                            public void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
                                mAutocomplete.setText(fetchPlaceResponse.getPlace().getName());
                                mAutocomplete.dismissDropDown();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                e.printStackTrace();
                                editText.setText(e.getMessage());
                            }
                        });
                    }


                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        private void fontChanged() {
            Typeface font = Typeface.createFromAsset(getAssets(),"fonts/Uber Move Text.ttf");
            actv.setTypeface(font);
            actv1.setTypeface(font);
            editText.setTypeface(font);
            editText3.setTypeface(font);
            tvexpr.setTypeface(font);
            langtextinput.setTypeface(font);
            nextbutton.setTypeface(font);
            nextactivity.setTypeface(font);
        }
    /*all data enable method*/
        private void dataEnabled() {
            actv.setEnabled(true);
            actv1.setEnabled(true);
            actv.setTextColor(Color.BLACK);
            actv1.setTextColor(Color.BLACK);
            mTagsEditText.setEnabled(true);
            mTagsEditText.setTagsWithSpacesEnabled(true);
            mTagsEditText.setTagsBackground(R.drawable.square_default);
            mTagsEditText.setCloseDrawableRight(R.drawable.tag_close);
            editText.setEnabled(true);
            editText.setTagsBackground(R.drawable.square_default);
            editText.setCloseDrawableRight(R.drawable.tag_close);
            editText3.setEnabled(true);
            spinner_yrs.setEnabled(true);
        }
        /*all data disable method*/
        private void dataDisabled() {
            actv.setEnabled(false);
            actv1.setEnabled(false);
            actv.setTextColor(Color.GRAY);
            actv1.setTextColor(Color.GRAY);
            mTagsEditText.setEnabled(false);
            mTagsEditText.setTagsWithSpacesEnabled(true);
            mTagsEditText.setTagsBackground(R.drawable.square_default_one);
            mTagsEditText.setCloseDrawableRight(R.color.grey);
            editText.setTagsBackground(R.drawable.square_default_one);
            editText.setCloseDrawableRight(R.color.grey);
          //  mTagsEditText.notify();
           // editText.setEnabled(false);
            editText3.setEnabled(false);
        }
    /*checking location permission */
        private void checkLocationPermission() {
            try {
                if (ActivityCompat.checkSelfPermission(this, mPermission)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(this,
                            new String[]{mPermission}, REQUEST_CODE_PERMISSION);

                    // If any permission above not allowed by user, this condition will execute every time, else your else part will work
                } else {
                    buildGoogleApiClient();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            Log.e("Req Code", "" + requestCode);
            if (requestCode == REQUEST_CODE_PERMISSION) {
                if (grantResults.length == 1 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Success Stuff here
                    buildGoogleApiClient();
                } else {
                    // Failure Stuff
                }
            }

        }
        synchronized void buildGoogleApiClient() {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    /*load data from sharedpreferences*/
        private void loadData() {
            SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
            ss = sharedPreferences.getString(One, null);
            String  text = sharedPreferences.getString(PRIMARY, null);
            if (text !=null)
            {
                actv.setText(text);
                actv.setTextColor(Color.GRAY);
                actv.setEnabled(false);

            }

            else
            {
                actv.setEnabled(true);
                actv.setTextColor(Color.BLACK);
            }
            String   text1 = sharedPreferences.getString(SECONDRY, null);
            if (text1 !=null)
            {
                actv1.setText(text1);
                actv1.setTextColor(Color.GRAY);
                actv1.setEnabled(false);

            }

            else
            {
                actv1.setEnabled(true);
                actv1.setTextColor(Color.BLACK);
            }
            String text2 = sharedPreferences.getString(LANGUAGE, null);

            if (text2 !=null)
            {
                String[] ss = text2.split("\\s+");
                for(int i = 0;i<ss.length;i++){
                    mTagsEditText.setText(ss[i]);
                    mTagsEditText.setEnabled(false);
                   mTagsEditText.setTagsBackground(R.drawable.square_default_one);
                    mTagsEditText.setCloseDrawableRight(R.color.grey);
//                    mTagsEditText.notify();
                }


            }

            else
            {
                mTagsEditText.setEnabled(true);
            }

            String  text4 = sharedPreferences.getString(MYEXPERTISE, null);
            if (text4 !=null)
            {
                editText3.setText(text4);
                editText3.setEnabled(false);
            }
            else
            {
                editText3.setEnabled(true);
            }
            text5 = sharedPreferences.getString(TEXT5, "");
            int LastClick=sharedPreferences.getInt("LastClick",0);
            if (LastClick!=0){
                spinner_yrs.setSelection(LastClick);
                spinner_yrs.setEnabled(false);
            }else {
                spinner_yrs.setEnabled(true);
            }
            String location = sharedPreferences.getString("location1",null);
            if (location!=null){
                String[] ll = location.split("\\s+");
                for(int i = 0;i<ll.length;i++){
                    editText.setText(ll[i]);
                    editText.setEnabled(false);
                    editText.setTagsBackground(R.drawable.square_default_one);
                    editText.setCloseDrawableRight(R.color.grey);
                }
            }else {
                editText.setEnabled(true);
            }


        }
        /*save data from sharedprefereences*/
        private void saveData() {
            SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            if(actv.getText().toString().isEmpty())
            {
                Toast.makeText(this, "Data not  saved", Toast.LENGTH_SHORT).show();

            }
            else
            {
                editor.putString(PRIMARY, actv.getText().toString());

            }
            if(actv1.getText().toString().isEmpty())
            {
                Toast.makeText(this, "Data not  saved", Toast.LENGTH_SHORT).show();

            }
            else
            {
                editor.putString(SECONDRY, actv1.getText().toString());

            }
            String lng = mTagsEditText.getText().toString();


            if(lng.isEmpty())
            {
                Toast.makeText(this, "Data not  saved", Toast.LENGTH_SHORT).show();
            }
            else
            {

                editor.putString(LANGUAGE,lng);


            }
            if (editText.getText().toString().isEmpty()){

            }else
                {
                editor.putString(WORKEX, editText.getText().toString());
            }

            if(editText3.getText().toString().isEmpty())
            {
                Toast.makeText(this, "Data not  saved", Toast.LENGTH_SHORT).show();

            }
            else
            {
                editor.putString(MYEXPERTISE, editText3.getText().toString());

            }if (editText.getText().toString().isEmpty()){

            }else {
                editor.putString("location1",editText.getText().toString());
            }
           // editor.putInt("LastClick",Integer.parseInt(String.valueOf(spinner_yrs.getSelectedItemPosition())));
            editor.putInt("LastClick",spinner_yrs.getSelectedItemPosition());
            editor.apply();
        }
        /*posting all data into server volley calling*/
        private void postskill() {
            sharedPreferences = getApplicationContext().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            final String skill = actv.getText().toString();
            final String second_skill = actv1.getText().toString();
            final String language_pre = mTagsEditText.getText().toString();
            final String worklocation = editText.getText().toString();
            final String expertise = editText3.getText().toString();
            final String experience = selectedItemText.toString();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://newsunshinegroups.com/pepow/api/worker_skill.php", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("msg");
                        if (status.equalsIgnoreCase("success")){
                            JSONObject jsonObject1=jsonObject.getJSONObject("data");
                            String skill=jsonObject1.getString("skill_id");
                            String language = jsonObject1.getString("language_proficiency");
                            sharedPreferenceClass.setValue_string("skill_ido",skill);
                            SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("OS",skill);
                            editor.apply();
                            progressDialog.dismiss();
                            Toast.makeText(Skill_Details.this, "Success", Toast.LENGTH_SHORT).show();
                            saveData();
                            loadData1();
                            Intent intent =  new Intent(Skill_Details.this,Info.class);
                            startActivity(intent);
                            finish();

                        }else {
                            progressDialog.dismiss();

                            Snackbar.make(getWindow().getDecorView().getRootView(), "Something went wrong", Snackbar.LENGTH_LONG).show();
                        }
                    } catch (JSONException e1) {

                        progressDialog.dismiss();
                        e1.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    Snackbar.make(getWindow().getDecorView().getRootView(), "Please try again", Snackbar.LENGTH_LONG).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put(KEY_PRIMARY_SKILL, skill);
                    params.put(KEY_SECONDRY_SKILL, second_skill);
                    params.put(KEY_LANGUAGE_PROFICIENCY,language_pre);
                    params.put(KEY_WORK_LOCATION,worklocation);
                    params.put(KEY_MY_EXPERTISE,expertise);
                    params.put(KEY_EXPERIENCE_IN_YEAR,experience);
                    return params;
                }
            };
            stringRequest.setShouldCache(false);
            int socketTimeout = 60000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            RequestQueue requestQueue = Volley.newRequestQueue(Skill_Details.this);
            requestQueue.add(stringRequest);
        }

    /*posting all data into server volley calling*/
    private void postskill1() {
        sharedPreferences = getApplicationContext().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        final String skill = actv.getText().toString();
        final String second_skill = actv1.getText().toString();
        final String language_pre = mTagsEditText.getText().toString();
        final String worklocation = editText.getText().toString();
        final String expertise = editText3.getText().toString();
        final String experience = selectedItemText.toString();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://newsunshinegroups.com/pepow/api/worker_skill.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("msg");
                    if (status.equalsIgnoreCase("success")){
                        JSONObject jsonObject1=jsonObject.getJSONObject("data");
                        String skill=jsonObject1.getString("skill_id");
                        String language = jsonObject1.getString("language_proficiency");
                        sharedPreferenceClass.setValue_string("skill_ido",skill);
                        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("OS",skill);
                        editor.apply();
                        progressDialog.dismiss();
                        Toast.makeText(Skill_Details.this, "Success", Toast.LENGTH_SHORT).show();
                        saveData();
                        loadData1();
                        Intent intent =  new Intent(Skill_Details.this,Compl_Regd_Details.class);
                        startActivity(intent);
                        finish();

                    }else {
                        progressDialog.dismiss();

                        Snackbar.make(getWindow().getDecorView().getRootView(), "Something went wrong", Snackbar.LENGTH_LONG).show();
                    }
                } catch (JSONException e1) {

                    progressDialog.dismiss();
                    e1.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Snackbar.make(getWindow().getDecorView().getRootView(), "Please try again", Snackbar.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", user_id);
                params.put(KEY_PRIMARY_SKILL, skill);
                params.put(KEY_SECONDRY_SKILL, second_skill);
                params.put(KEY_LANGUAGE_PROFICIENCY,language_pre);
                params.put(KEY_WORK_LOCATION,worklocation);
                params.put(KEY_MY_EXPERTISE,expertise);
                params.put(KEY_EXPERIENCE_IN_YEAR,experience);
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        int socketTimeout = 60000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(Skill_Details.this);
        requestQueue.add(stringRequest);
    }
        @Override
        public void onClick(View view) {

        }
        @Override
        public void onTagsChanged(Collection<String> tags) {

        }

        @Override
        public void onEditingFinished() {

        }
    /*location changed method*/
        @Override
        public void onLocationChanged(Location location) {
            lat = String.valueOf(location.getLatitude());
            lon = String.valueOf(location.getLongitude());
            double latitude =Double.parseDouble(lat);
            double longitude =Double.parseDouble(lon);
            getAddress(latitude,longitude);
            updateUI();
        }

        @Override
        protected void onResume() {
            super.onResume();
           // Toast.makeText(this, "run", Toast.LENGTH_SHORT).show();
           /* if (mGoogleApiClient != null) {
                mGoogleApiClient.connect();
            }*/
        }

    /*getting address here*/
        public void getAddress(double lat, double lng) {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
                if (addresses != null && addresses.size()>0) {
                    Address obj = addresses.get(0);
                    add =  obj.getLocality();

                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
        void updateUI() {
           // editText.setText(add);
            SharedPreferences sharedPreferences = getSharedPreferences("pppp", MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("add",add);
            editor.apply();
            SharedPreferences sharedPreferences1 = getSharedPreferences("pppp", MODE_PRIVATE);
            l = sharedPreferences1.getString("location",null);
            String location = sharedPreferences.getString("location1",null);
            if (location!=null){

            }
            if (l!=null){
                editText.setText(l);
            }else if (location!=null){
                String[] ll = location.split("\\s+");
                for(int i = 0;i<ll.length;i++){
                    editText.setText(ll[i]);
                    editText.setEnabled(false);
                    editText.setTagsBackground(R.drawable.square_default_one);
                    editText.setCloseDrawableRight(R.color.grey);

                }

            }else {
                Toast.makeText(this, "empty11", Toast.LENGTH_SHORT).show();
            }
        }
        @Override
        public void onConnected(@Nullable Bundle bundle) {
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            // mLocationRequest.setInterval(10000);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED){
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest,this::onLocationChanged);
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                lat = String.valueOf(mLastLocation.getLatitude());
                lon = String.valueOf(mLastLocation.getLongitude());

            }//updateUI();
        }
        @Override
        public void onConnectionSuspended(int i) {

        }

        @Override
        public void onConnectionFailed( ConnectionResult connectionResult) {
            buildGoogleApiClient();
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.edit_option, menu);
            editmenu = menu.findItem(R.id.edit);
            editmenu.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    dataEnabled();
                    mTagsEditText.setAdapter(tagAdapter);
                    mTagsEditText.setTagsWithSpacesEnabled(true);
                    return false;
                }
            });
            return true;

        }

        private void loadData1() {
                actv.setEnabled(false);
                actv1.setEnabled(false);
                actv.setTextColor(Color.GRAY);
                actv1.setTextColor(Color.GRAY);
                mTagsEditText.setEnabled(false);
            mTagsEditText.setTagsBackground(R.drawable.square_default_one);
            mTagsEditText.setCloseDrawableRight(R.color.grey);
//            mTagsEditText.notify();
                editText3.setEnabled(false);
                spinner_yrs.setEnabled(false);
                editText.setEnabled(false);
            editText.setTagsBackground(R.drawable.square_default_one);
            editText.setCloseDrawableRight(R.color.grey);
                mTagsEditText.setTagsWithSpacesEnabled(true);
        }
    }
