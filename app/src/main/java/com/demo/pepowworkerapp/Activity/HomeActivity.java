
package com.demo.pepowworkerapp.Activity;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Utils.InternetConnection;
import com.demo.pepowworkerapp.Utils.SharedPreferenceClass;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "HomeActivity";
    private static HomeActivity instance;
    String user_id;
    private String profile_photo;
    String ss;
    String text5;
    SharedPreferences sharedPreferences;
    private static final String USER_PREFS = "SeekingDaddie";
    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor prefsEditor;
    private SharedPreferenceClass sharedPreferenceClass;
    Toolbar toolbar;
    Switch switchbutton;
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String FNAME = "fname";
    public static final String LNAME = "lname";
    public static final String ADDRESS = "address";
    public static final String AREA = "area";
    public static final String CITY = "city";
    public static final String DOB = "dob";
    public static final String IDNUM = "idno";

    public static final String BANKNAME = "bankname";
    public static final String IFSCCODE = "ifsccode";
    public static final String ACCOUNT = "account";
    public static final String BRANCHNAME = "branchname";

    public static final String PRIMARY = "primary";
    public static final String SECONDRY = "secondary";
    public static final String LANGUAGE = "language";
    public static final String WORKEX = "work";
    public static final String MYEXPERTISE = "myexp";
    public static final String TEXT5 = "text5";
    public static final String One = "one";

    public static final String SPINNER = "register_for";
    public static final String PERDAY = "rate_per_day";
    public static final String PERMONTH = "expected_salary";
    RatingBar ratingBar;
    ImageView imageview,imageView1,imageView2,imageView3,imageView4,imageView5,imageView6,imageView7,imageView8;
    TextView text;
    ProgressBar progressprofile,progressname;
    TextView profile_summary,profile_text,banking_text,skill_text,regis_text;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    LinearLayout l1;
    TextView general_profile;
     InternetConnection internetConnection;
     SharedPreferences.Editor editor,editor1,editor2,editor3;
    ActionBarDrawerToggle toggle;
    DrawerLayout drawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        profile_summary=findViewById(R.id.profile_summary);
        imageView1=findViewById(R.id.general_green);
        imageView2=findViewById(R.id.general_red);
        imageView3=findViewById(R.id.banking_green);
        imageView4=findViewById(R.id.banking_red);
        imageView5=findViewById(R.id.skill_green);
        imageView6=findViewById(R.id.skill_red);
        imageView7=findViewById(R.id.regis_green);
        imageView8=findViewById(R.id.regis_red);
        ratingBar=findViewById(R.id.ratingbar);

        profile_text=findViewById(R.id.general_text);
        banking_text=findViewById(R.id.banking_text);
        skill_text=findViewById(R.id.skill_text);
        regis_text=findViewById(R.id.regis_text);
        //Custom Font style
        Typeface custom_font = Typeface.createFromAsset(getAssets(),"fonts/Uber Move Text.ttf");
        profile_summary.setTypeface(custom_font);
        profile_text.setTypeface(custom_font);
        banking_text.setTypeface(custom_font);
        skill_text.setTypeface(custom_font);
        regis_text.setTypeface(custom_font);

        //status bar colour change
        instance=this;
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));

        //get the userid from sharedpreference class
        internetConnection = new InternetConnection(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        sharedPreferenceClass = new SharedPreferenceClass(this);
        sharedPreferenceClass = new SharedPreferenceClass(HomeActivity.this);
        user_id = sharedPreferenceClass.getValue_string("user_id");
        profile_photo=sharedPreferenceClass.getProfile_pic();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundResource(R.color.themecolourblue);
        toolbar.setTitleTextColor(Color.WHITE);
         drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
         toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open,R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toggle.getDrawerArrowDrawable().setColor(Color.WHITE);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
         View header = navigationView.getHeaderView(0);
        imageview=header.findViewById(R.id.imageViewprofile);
        progressname=header.findViewById(R.id.nameprogress);
        progressprofile=header.findViewById(R.id.profileprogress);
        progressname.setVisibility(View.VISIBLE);
        progressprofile.setVisibility(View.VISIBLE);
        text=header.findViewById(R.id.name);
        SwitchCompat drawerSwitch = (SwitchCompat) navigationView.getMenu().findItem(R.id.hideme).getActionView();
        drawerSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    Toast.makeText(getApplicationContext(), "Switch on", Toast.LENGTH_SHORT).show();
                    // do stuff
                } else {
                    // do other stuff
                    Toast.makeText(getApplicationContext(), "Switch off", Toast.LENGTH_SHORT).show();

                }
            }
        });
        getProfile();
        checkGeneralProfileData();
        checkBankDetailsData();
        checkSkillData();
        checkRegdDetails();

    }

    private void checkGeneralProfileData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String fname = sharedPreferences.getString(FNAME, null);
        String lname = sharedPreferences.getString(LNAME, null);
        String address = sharedPreferences.getString(ADDRESS, null);
        String area = sharedPreferences.getString(AREA, null);
        String city = sharedPreferences.getString(CITY, null);
        String dob = sharedPreferences.getString(DOB, null);
        String   prfimageurl = sharedPreferences.getString("prf", null);
        String   adharurl = sharedPreferences.getString("prff", null);
        String img = sharedPreferences.getString("alter",null);
        SharedPreferences sharedPreferences1=PreferenceManager.getDefaultSharedPreferences(this);
        Boolean Malebutton=sharedPreferences1.getBoolean("Male",false);
        Boolean Femalebutton=sharedPreferences1.getBoolean("Female",false);
        Boolean Otherbutton=sharedPreferences1.getBoolean("Others",false);

        SharedPreferences LastSelect=getSharedPreferences("LastSetting",Context.MODE_PRIVATE);
         int lastClick=LastSelect.getInt("lastClick",0);
        String gg=LastSelect.getString("ggg",null);

        if (fname!=null && lname!=null && address!=null && area!=null && city!=null && dob!=null && prfimageurl != null)
        {
            if (Malebutton.equals(true) || Femalebutton.equals(true) || Otherbutton.equals(true))
            {
                if (adharurl != null || (gg !=null && lastClick!= 0 && img != null) )
                {
                    imageView1.setVisibility(View.VISIBLE);
                    imageView2.setVisibility(View.GONE);
                }

            }

        }
        else
        {
            imageView1.setVisibility(View.GONE);
            imageView2.setVisibility(View.VISIBLE);
        }


    }
    private void checkBankDetailsData() {

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String bank = sharedPreferences.getString(BANKNAME, null);
        String   ifsc = sharedPreferences.getString(IFSCCODE, null);
        String  account = sharedPreferences.getString(ACCOUNT, null);
        String   branch = sharedPreferences.getString(BRANCHNAME, null);
        String    prfimageurl = sharedPreferences.getString("prf1", null);
        if (bank!=null && ifsc!=null && account!=null && branch!=null && prfimageurl!=null)
        {
            imageView3.setVisibility(View.VISIBLE);
            imageView4.setVisibility(View.GONE);
        }
        else
        {
            imageView3.setVisibility(View.GONE);
            imageView4.setVisibility(View.VISIBLE);
        }

    }
    private void checkSkillData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String  text = sharedPreferences.getString(PRIMARY, null);
        String   text1 = sharedPreferences.getString(SECONDRY, null);
        String    text2 = sharedPreferences.getString(LANGUAGE, null);
        String  text4 = sharedPreferences.getString(MYEXPERTISE, null);
        int LastClick=sharedPreferences.getInt("LastClick",0);
        text5 = sharedPreferences.getString(TEXT5, "");
        ss = sharedPreferences.getString(One, null);
        if (text!=null && text1!=null && text2!=null && text4!=null && LastClick!=0)
        {
            imageView5.setVisibility(View.VISIBLE);
            imageView6.setVisibility(View.GONE);
        }
        else
        {
            imageView5.setVisibility(View.GONE);
            imageView6.setVisibility(View.VISIBLE);
        }

    }
    private void checkRegdDetails() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String per_day = sharedPreferences.getString(PERDAY, null);
        String expt_day = sharedPreferences.getString(PERMONTH, null);
        int LastClick=sharedPreferences.getInt("lastclick",0);
        if (per_day!=null || expt_day!=null)
        {
            imageView7.setVisibility(View.VISIBLE);
            imageView8.setVisibility(View.GONE);
        }
        else
        {
            imageView7.setVisibility(View.GONE);
            imageView8.setVisibility(View.VISIBLE);
        }if (LastClick == 3){
            if (per_day!=null && expt_day!=null){
                imageView7.setVisibility(View.VISIBLE);
                imageView8.setVisibility(View.GONE);
            }else {
                imageView7.setVisibility(View.GONE);
                imageView8.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        getProfile();
        checkGeneralProfileData();
        checkBankDetailsData();
        checkRegdDetails();
        checkSkillData();


    }

    private void getProfile() {
//        final String cheque=chq_image.getDrawable().toString();
        StringRequest stringRequest=new StringRequest(Request.Method.GET, "http://newsunshinegroups.com/pepow/api/get_general_profile.php?user_id="+user_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("msg");
                    Log.d("oooo", String.valueOf(jsonObject));
                    if (status.equalsIgnoreCase("Success")){
                        progressname.setVisibility(View.GONE);
                        progressprofile.setVisibility(View.GONE);
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        String name =jsonObject1.getString("first_name");
                        String last =jsonObject1.getString("last_name");
                        String image =jsonObject1.getString("profile_pic");
                        Glide.with(HomeActivity.this).load(image).into(imageview);
                        if (name!=null && last!=null){text.setText(name+" "+last);
                            text.setTextSize(15);
                        }else {
                            progressname.setVisibility(View.GONE);
                            progressprofile.setVisibility(View.GONE);
                            text.setText("Your Name");
                            text.setTextSize(15);
                        }


                    }
                    else {
                        progressname.setVisibility(View.GONE);
                        progressprofile.setVisibility(View.GONE);
                    }
                    // Toast.makeText(MainActivity.this, status, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    progressname.setVisibility(View.GONE);
                    progressprofile.setVisibility(View.GONE);
                    e.printStackTrace();
                }



            }
        } ,new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressname.setVisibility(View.GONE);
                progressprofile.setVisibility(View.GONE);
            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",user_id);

                return params;

            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("Content-Type","application/x-www-form-urlencoded");
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        int socketTimeout = 60000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        stringRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(HomeActivity.this);
        requestQueue.add(stringRequest);

    }

    public static HomeActivity getInstance(){
        return instance;
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.feeforservice) {
          Intent intent = new Intent(HomeActivity.this,Fee_For_Service.class);
          startActivity(intent);
          finish();
        } else if (id == R.id.employment) {
            Intent intent = new Intent(HomeActivity.this,Employment.class);
            startActivity(intent);
            finish();
        }
        else if (id == R.id.logout) {
            sharedPreferences = getSharedPreferences(SHARED_PREFS,0);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.apply();
            editor.commit();
            this.appSharedPrefs = HomeActivity.this.getSharedPreferences(USER_PREFS, Activity.MODE_PRIVATE);
            appSharedPrefs.getString("user_id",null);
            this.prefsEditor = appSharedPrefs.edit();
             prefsEditor.clear();
             prefsEditor.apply();
            Intent intent=new Intent(HomeActivity.this,LoginActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void check(View view)
    {
        Intent intent =new Intent(HomeActivity.this,General_Profile.class);
        startActivity(intent);
    }

    public void check_banking(View view)
    {
        Intent intent =new Intent(HomeActivity.this,Bank_Details_regd.class);
        startActivity(intent);
    }

    public void check_skill(View view){
        Intent intent = new Intent(HomeActivity.this,Skill_Details.class);
        startActivity(intent);
    }


    public void check_regd(View view) {
        Intent intent = new Intent(HomeActivity.this, Compl_Regd_Details.class);
        startActivity(intent);
    }

}
