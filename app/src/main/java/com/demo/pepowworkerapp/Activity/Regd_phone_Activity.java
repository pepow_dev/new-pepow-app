package com.demo.pepowworkerapp.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Utils.InternetConnection;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Regd_phone_Activity extends AppCompatActivity {
    private EditText et_num;
    private Button submit;
    SharedPreferences sharedPreferences;
    private InternetConnection internetConnection;
    String phone_no;
    public static String NEW_OTP_URL= "http://newsunshinegroups.com/pepow/api/user_otp_validate.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regd_phone);
        et_num = findViewById(R.id.etnum);
        submit = findViewById(R.id.submit);
        internetConnection = new InternetConnection(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        //status bar colour change
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
        //Custom font style change
        fontChanged();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*  Verification method for validation of mobile number */
                verification();
                sharedPreferences=getApplicationContext().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.putString("phone",et_num.getText().toString());
                editor.apply();
            }

        });
    }
//custom font style
    private void fontChanged() {
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/Uber Move Text.ttf");
        et_num.setTypeface(font);
        submit.setTypeface(font);
    }

    /* validation started*/
    private void verification() {

        String phoneNumber = "";

        if (et_num.getText().toString().equals("")) {
            Snackbar.make(findViewById(android.R.id.content), "Enter your mobile no.", Snackbar.LENGTH_LONG).show();
            et_num.requestFocus();
            et_num.setCursorVisible(true);

        } else if (et_num.length() < 10) {
            Snackbar.make(findViewById(android.R.id.content), "Phone number should be 10 digits", Snackbar.LENGTH_LONG).show();
            et_num.requestFocus();
            et_num.setCursorVisible(true);
        } else if (phoneNumber.matches("^[7-9][0-9]{9}$")) {
            Snackbar.make(findViewById(android.R.id.content), "Invalid number ", Snackbar.LENGTH_LONG).show();
        } else {
            if (internetConnection.isConnectingToInternet()) {
                /* Heating on api for mobile verification with otp*/
                next();
            } else {
            }
        }
    }
    /* mobile verication with otp is started here*/
    private void next() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, NEW_OTP_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    Toast.makeText(getApplicationContext(), message + "  ",Toast.LENGTH_LONG).show();

                    if (status.equalsIgnoreCase("success")) {

                      phone_no = jsonObject.getString("phone_no");
                        String s1 = et_num.getText().toString();
                        Intent intent = new Intent(Regd_phone_Activity.this, OtpPinActivity.class);
                        intent.putExtra("9876543212", s1);
                        startActivity(intent);
                        finish();
                    }
                    else {
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(Regd_phone_Activity.this, "Check Your Internet Connection", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("phone_no", et_num.getText().toString());
                return params;
            }
        };
        stringRequest.setShouldCache(false);
        RequestQueue requestQueue = Volley.newRequestQueue(Regd_phone_Activity.this);
        requestQueue.add(stringRequest);
    }
    }


