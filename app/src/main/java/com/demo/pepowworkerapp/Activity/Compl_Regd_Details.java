package com.demo.pepowworkerapp.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Utils.InternetConnection;
import com.demo.pepowworkerapp.Utils.SharedPreferenceClass;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Compl_Regd_Details extends AppCompatActivity {
    //Complete Regd Code
    TextView textView2,chargetv,tvrate,tvexpexsal,tvpmonth,tvconditions;
    private String user_id;
    private SharedPreferenceClass sharedPreferenceClass;
    SharedPreferences sharedPreferences;
    private InternetConnection internetConnection;

    LinearLayout freeservice,employmentservice,conditions;
    EditText perday,permont;
    String selectedItemText;
    Button mButton;
    CheckBox mCheckBox;
    Spinner spinner;
    //Editable Functionality
    private MenuItem editmenu;
    private MenuItem savemenu;
    //--COMPLETE REGD PARAMETRES-------

    public static final String KEY_SPIN = "register_for";
    public static final String KEY_PER_DAY = "rate_per_day";
    public static final String KEY_EXPECTED = "expected_salary";
    //------sharedpreference keys-------//
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String SPINNER = "register_for";
    public static final String PERDAY = "rate_per_day";
    public static final String PERMONTH = "expected_salary";
    public static final String CHECKBOX = "expected_salary";
    private int mSpacing = 20, mButtonHeight = 0, mButtonWidth = 0, mButton1Bottom = 0, mButton1Left = 0, mButton2Bottom = 0, mButton2Left = 0, mButton3Bottom = 0, mButton3Left = 0, mTextViewHeight = 200, mTextViewTop = 0, mTextViewLeft = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compl_regd_details);
       windowmethod();
        //get the userid from sharedpreference class
        sharedPreferenceClass = new SharedPreferenceClass(this);
        sharedPreferenceClass = new SharedPreferenceClass(Compl_Regd_Details.this);
        user_id = sharedPreferenceClass.getValue_string("user_id");
        perday=findViewById(R.id.rateper_day);
        permont=findViewById(R.id.permonth);
        textView2=findViewById(R.id.textView2);
        tvrate=findViewById(R.id.tvrate);
        chargetv=findViewById(R.id.chargetv);
        tvexpexsal=findViewById(R.id.tvexpexsal);
        tvpmonth=findViewById(R.id.tvpmonth);
        tvconditions=findViewById(R.id.tvconditions);
        spinner =findViewById(R.id.id_spinner_regd);
        internetConnection = new InternetConnection(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        Toolbar toolbar_salary = (Toolbar) findViewById(R.id.toolbar_regd);
        setSupportActionBar(toolbar_salary);
        //give a heading to toolbar
        getSupportActionBar().setTitle("Complete Registration");
        toolbar_salary.setTitleTextColor(Color.WHITE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        freeservice=findViewById(R.id.freeservice);
        employmentservice=findViewById(R.id.employ);
        conditions=findViewById(R.id.conditions);
         mButton=(Button)findViewById( R.id.send);
        mButton.setEnabled(false);
        mButton.setBackground(getDrawable(R.drawable.round2));
         mCheckBox= ( CheckBox ) findViewById( R.id.check);


        mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    mButton.setEnabled(true);
               mButton.setBackground(getDrawable(R.drawable.background_add));

                }else{
                    mButton.setEnabled(false);
                    mButton.setBackground(getDrawable(R.drawable.round2));
                    Toast.makeText(Compl_Regd_Details.this, "please accept our terms and conditions", Toast.LENGTH_SHORT).show();
                }

            }
        });
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendFinal();
                saveData();
                loadData();
                mCheckBox.setEnabled(false);
            }
        });
        //backbutton implementation
        toolbar_salary.setNavigationIcon(R.drawable.arrowback);
        toolbar_salary.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // Get reference of widgets from XML layout
        final Spinner spinner = (Spinner) findViewById(R.id.id_spinner_regd);

        // Initializing a String Array
        String[] id = new String[]{
                "Choose any id",
                "Fee for service",
                "Employment",
                "Both"

        };
        final List<String> idskill = new ArrayList<>(Arrays.asList(id));

        // Initializing an ArrayAdapter

        final ArrayAdapter<String> idAdapter = new ArrayAdapter<String>(this,R.layout.spinner,idskill)
        {
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {

                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view=super.getDropDownView(position,convertView,parent);
                TextView tv_secondary_skill=(TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv_secondary_skill.setTextColor(Color.GRAY);
                }
                else {
                    tv_secondary_skill.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        idAdapter.setDropDownViewResource(R.layout.spinner);
        spinner.setAdapter(idAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                switch(position)
                {
                    case 1:
                    {
                        freeservice.setVisibility(View.VISIBLE);
                        conditions.setVisibility(View.VISIBLE);
                        employmentservice.setVisibility(View.GONE);

                    }
                    break;
                    case 2:
                    {
                        freeservice.setVisibility(View.GONE);
                        employmentservice.setVisibility(View.VISIBLE);
                        conditions.setVisibility(View.VISIBLE);
                    }
                    break;
                    case 3:
                    {
                        freeservice.setVisibility(View.VISIBLE);
                        employmentservice.setVisibility(View.VISIBLE);
                        conditions.setVisibility(View.VISIBLE);
                    }
                    break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        loadData();
        //Custom font style change
        fontChanged();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean firstOpened = preferences.getBoolean("ranBefore", true);
        if(firstOpened) {
            showOverlay();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("ranBefore", false);
            editor.apply();
        }

        SpannableString ss = new SpannableString(getResources().getString(R.string.terms));
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Toast.makeText(Compl_Regd_Details.this, "Clicked", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };
        ss.setSpan(clickableSpan, 15, 35, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.themecolourblue)), 15,35, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvconditions.setText(ss);
        tvconditions.setMovementMethod(LinkMovementMethod.getInstance());
        tvconditions.setHighlightColor(Color.TRANSPARENT);

            }

            private void windowmethod() {
                //status bar colour change
                Window window=getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
            }

            private void showOverlay() {
                final Dialog dialog = new Dialog(Compl_Regd_Details.this, android.R.style.Theme_Translucent_NoTitleBar);
                dialog.setContentView(R.layout.overlay_view);
                RelativeLayout layout = (RelativeLayout) dialog.findViewById(R.id.overlayLayout);
                layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        dialog.dismiss();
                    }
                });

                dialog.show();

                LinearLayout button1Layout = (LinearLayout) dialog.findViewById(R.id.button1Layout);
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(mButton3Left + mButtonWidth/2, mButton1Bottom + mButtonHeight + mSpacing, 0, 0);
                button1Layout.setLayoutParams(params);

            }

            private void fontChanged() {
                Typeface font = Typeface.createFromAsset(getAssets(),"fonts/Uber Move Text.ttf");
                perday.setTypeface(font);
                permont.setTypeface(font);
                textView2.setTypeface(font);
                tvrate.setTypeface(font);
                chargetv.setTypeface(font);
                tvexpexsal.setTypeface(font);
                tvpmonth.setTypeface(font);
                tvconditions.setTypeface(font);
                mButton.setTypeface(font);
            }

            /*save data in sharedpreferences*/
            private void saveData() {
                SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                if(perday.getText().toString().isEmpty())
                {
                    Toast.makeText(this, "Data not  saved", Toast.LENGTH_SHORT).show();

                }
                else
                {
                    editor.putString(PERDAY, perday.getText().toString());
                    Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
                }
                if(permont.getText().toString().isEmpty())
                {
                    Toast.makeText(this, "Data not  saved", Toast.LENGTH_SHORT).show();

                }
                else
                {
                    editor.putString(PERMONTH, permont.getText().toString());
                    Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
                }

                if( selectedItemText.toString().isEmpty())
                {
                    Toast.makeText(this, "Data not  saved", Toast.LENGTH_SHORT).show();

                }
                else
                {
                    editor.putString(SPINNER,  selectedItemText.toString());
                    Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
                }
                //editor.putInt("lastclick",Integer.parseInt(String.valueOf(spinner.getSelectedItemPosition())));
                editor.putInt("lastclick",spinner.getSelectedItemPosition());
                editor.apply();
            }
            /*load data from sharedpreferences*/
            private void loadData() {
                SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                String per_day = sharedPreferences.getString(KEY_PER_DAY, null);
                String expt_day = sharedPreferences.getString(KEY_EXPECTED, null);
                if(per_day != null)
                {
                    perday.setText(per_day);
                    perday.setEnabled(false);
                    mCheckBox.setEnabled(false);
                    spinner.setEnabled(false);
                    permont.setEnabled(false);
                }
                else
                {
                    perday.setEnabled(true);
                }
                if (expt_day != null)
                {
                    permont.setText(expt_day);
                    permont.setEnabled(false);
                    perday.setEnabled(false);
                    mCheckBox.setEnabled(false);
                    spinner.setEnabled(false);
                }
                else
                {
                    permont.setEnabled(true);
                }
                int LastClick=sharedPreferences.getInt("lastclick",0);
                spinner.setSelection(LastClick);


            }

            private void dataEnabled() {
                perday.setEnabled(true);
                permont.setEnabled(true);
                mCheckBox.setEnabled(true);
                spinner.setEnabled(true);
            }
            private void dataDisabled() {
                perday.setEnabled(false);
                permont.setEnabled(false);
            }
        /*posting data post api volley*/
            private void sendFinal() {
                sharedPreferences = getApplicationContext().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
                final String Perday = perday.getText().toString();
                final String Permont = permont.getText().toString();
                final String spinner = selectedItemText.toString();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://newsunshinegroups.com/pepow/api/worker_complete_registration.php",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.getString("status");
                                    String message = jsonObject.getString("msg");
                                    if (status.equalsIgnoreCase("Success")){

                                        saveData();
                                        Snackbar.make(getWindow().getDecorView().getRootView(), "Sucessfully Submitted", Snackbar.LENGTH_LONG).show();

                                    }else {
                                        Toast.makeText(Compl_Regd_Details.this, "Error", Toast.LENGTH_SHORT).show();
                                        Snackbar.make(getWindow().getDecorView().getRootView(), "Something went wrong", Snackbar.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e1) {
                                    Snackbar.make(getWindow().getDecorView().getRootView(), "Please try again", Snackbar.LENGTH_LONG).show();
                                    e1.printStackTrace();
                                }

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Snackbar.make(getWindow().getDecorView().getRootView(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                    }

            }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id", user_id);
                        params.put(KEY_PER_DAY, Perday);
                     params.put(KEY_EXPECTED,Permont);
                     params.put(KEY_SPIN,spinner);
                        return params;
                    }
                };
                stringRequest.setShouldCache(false);
                int socketTimeout = 60000; // 30 seconds. You can change it
                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                stringRequest.setRetryPolicy(policy);
                RequestQueue requestQueue = Volley.newRequestQueue(Compl_Regd_Details.this);
                requestQueue.add(stringRequest);
            }

            @Override
            public boolean onCreateOptionsMenu(Menu menu) {
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.edit_option, menu);
                editmenu = menu.findItem(R.id.edit);
                editmenu.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        dataEnabled();
                        mCheckBox.setEnabled(true);
                        return false;
                    }
                });


                return true;

            }


        }
