package com.demo.pepowworkerapp.Activity;
import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.Image;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.demo.pepowworkerapp.Adapter.AutoCompleteAdapter;
import com.demo.pepowworkerapp.Adapter.PlaceAutoSuggestAdapter;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.General_Repo;
import com.demo.pepowworkerapp.Utils.InternetConnection;
import com.demo.pepowworkerapp.Utils.SharedPreferenceClass;

import com.demo.pepowworkerapp.network.RetrofitInterface;
import com.demo.pepowworkerapp.network.ServiceGenerator;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

public class General_Profile extends AppCompatActivity {
    private static final String TAG = General_Profile.class.getSimpleName();
    //CHOOSE IMG PIC CODE
    public static final int REQUEST_IMAGE = 100;
    final int CODE_GALLERY_REQUEST_ID=999;
    public static final int IMAGE_ALTERNATE=101;
    private String Jsonresponse;
    private String user_id;
    public byte[] imageBytes;
    Uri selectedImage,selectedImage1;
    public byte[] imageBytes1;
    public byte[] imageBytes2;
    ImageView imgProfile;
    //ImageView imgid;
    ContentResolver resolver;
    RadioGroup radioGroup;
    TextView adharid;
    ImageView viewImage,alternate,removeimage;
    Uri uriprf;
    //Area autofill initialisation
    AutoCompleteAdapter adapter;
    PlacesClient placesClient;

    //---general profile key parameteres-------------
    public static String KEY_FIRST = "first_name";
    public static String KEY_lAST = "last_name";
    public static String KEY_ID_NO = "id_no";
    public static final String KEY_ID_TYPE = "id_type";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_AREA = "area";
    public static final String KEY_CITY = "city";
    public static final String KEY_DATE_OF_BIRTH = "date_of_birth";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_PROFILE = "profile_pic";
    public static final String KEY_ID_PROOF = "adhar_image";
    public static final String KEY_ID_IMAGE = "id_image";

    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String FNAME = "fname";
    public static final String LNAME = "lname";
    public static final String ADDRESS = "address";
    public static final String AREA = "area";
    public static final String CITY = "city";
    public static final String DOB = "dob";
    public static final String GENDER = "gender";
    private String fname;
    private String lname;
    private String address;
    private String area;
    private String city;
    private String dob;
    Bitmap bitmap,bitmap1,bitmap3,bitmap2;
    SharedPreferences sharedPreferences;
    private SharedPreferenceClass sharedPreferenceClass;
    private String selectedType="";
    TextInputLayout textInputLayoutfname,textInputLayoutlname;
    EditText etfname, etlname,etcity,idno;
    TextView etdob,etdob1;
    TextView textView,idpic,adhartextview,noadhar;
    TextView gender;
    private int mYear, mMonth, mDay;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private InternetConnection internetConnection;
    HomeActivity homeActivity;
    RadioButton Male,Female,Others;
    TextView idtv;
    String selectedItemText;
    String Id;
    AutoCompleteTextView autoCompleteTextView,areaautocompleteTextView;
    Dialog dialog;
    Button buttonok,img_save;
    Spinner spinner;
    String  prfimageurl,adharurl,alteridurl;
    String path,ss,alt;
    Uri uri,uri1;
    //Editable Functionality
    private MenuItem editmenu;
    private MenuItem savemenu;
    Geocoder geocoder;
    List<Address> addresses;
    LatLng p1 = null;
    FloatingActionButton floatingActionButton;
    ProgressDialog progressDialog;
    JsonObject jsonObject;
    private int mSpacing = 20, mButtonHeight = 0, mButtonWidth = 0, mButton1Bottom = 0, mButton1Left = 0, mButton2Bottom = 0, mButton2Left = 0, mButton3Bottom = 0, mButton3Left = 0, mTextViewHeight = 200, mTextViewTop = 0, mTextViewLeft = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general__profile);
        radioGroup=findViewById(R.id.radiogroup);
        adharid=findViewById(R.id.adhar_id);
       progressDialog=new ProgressDialog(General_Profile.this);
        etfname = findViewById(R.id.fname);
        viewImage=findViewById(R.id.adhar_image);
        etlname = findViewById(R.id.lnameTextInputEditText);
        etdob = findViewById(R.id.DateofbirthTextInputEditText);
        etdob.setTextColor(Color.BLACK);
        etdob1 = findViewById(R.id.DateofbirthTextInputEditText1);
        etdob1.setPaintFlags(etdob1.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        removeimage=findViewById(R.id.removeidimage);
        jsonObject = new JsonObject();

        Male = (RadioButton)findViewById(R.id.male);
        Female = (RadioButton)findViewById(R.id.female);
        Others = (RadioButton)findViewById(R.id.others);
        imgProfile=findViewById(R.id.img_profile);
        idtv=findViewById(R.id.ulternateid);
        idtv.setPaintFlags(idtv.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);

       /* File casted_image = new File(adharurl);
        if (casted_image.isHidden()){


        }*/
        areaautocompleteTextView=findViewById(R.id.auto);
        etcity=findViewById(R.id.city);
        adhartextview=findViewById(R.id.adhartextview);
        noadhar=findViewById(R.id.noadhar);
        gender=findViewById(R.id.Gender);
        Id=getString(R.string.not_uploaded);
        selectedItemText =getString(R.string.not_uploaded);
         autoCompleteTextView=findViewById(R.id.autocomplete_text);
        geocoder = new Geocoder(this, Locale.getDefault());

        /*google key initialization*/
        String apiKey = getString(R.string.api_key);
        if(apiKey.isEmpty()){
            etcity.setText(getString(R.string.error));
            return;
        }
        // Setup Places Client
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(),apiKey);
        }
        placesClient = Places.createClient(this);
        initAutoCompleteTextView();
        // autoCompleteTextView.setAdapter(new PlaceAutoSuggestAdapter(General_Profile.this,android.R.layout.simple_list_item_1));
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i==R.id.male)
                {
                    selectedType=Male.getText().toString();
                    Toast.makeText(General_Profile.this, "Male", Toast.LENGTH_SHORT).show();
                }
                else if(i==R.id.female) {
                    selectedType=Female.getText().toString();
                    Toast.makeText(General_Profile.this, "Female", Toast.LENGTH_SHORT).show();
                }
                else if(i==R.id.others) {
                    selectedType=Others.getText().toString();
                    Toast.makeText(General_Profile.this, "Others", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ButterKnife.bind(this);
        loadProfil();
       // loadAdhar();
        loadData();
        //Custom font style change
        fontChanged();
        homeActivity=HomeActivity.getInstance();
        sharedPreferenceClass = new SharedPreferenceClass(this);
        sharedPreferenceClass = new SharedPreferenceClass(General_Profile.this);
        user_id = sharedPreferenceClass.getValue_string("user_id");
        resolver = getContentResolver();
        internetConnection = new InternetConnection(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        Window window=getWindow();
        //status bar colour change
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
        //Floating action button setup
        floatingActionButton = findViewById(R.id.fab);
        /*validation of whole value and saving data in sharedpreferences click on floating button*/
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setTitle("Loading....");
               progressDialog.setMessage("Sending data to server");
               // progressDialog.setProgressStyle(progressDialog.STYLE_HORIZONTAL);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setProgress(0);
                progressDialog.setMax(20);
               progressDialog.show();
                profileValidation();
                saveData();
                loadData();
            }
        });
            //Toolbar setup
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_profile);
            setSupportActionBar(toolbar);
            //give a heading to toolbar
          getSupportActionBar().setTitle("General Profile");
          toolbar.setTitleTextColor(Color.WHITE);
          getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //backbutton implementation
        toolbar.setNavigationIcon(R.drawable.arrowback);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        removeimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                String ss = sharedPreferences.getString("prff",null);
                File casted_image = new File(adharurl);
                if (casted_image.exists()) {
                    editor.remove("prff");
                    editor.commit();
                    casted_image.deleteOnExit();
                    casted_image.delete();
                    removeimage.setVisibility(View.GONE);
                    Picasso.with(General_Profile.this).load(R.drawable.dd)
                            .into(viewImage);

                    //  loadAdhar();
                    Log.d("pathofimgae","if");

                }else {
                    Picasso.with(General_Profile.this).load(R. drawable.dd)
                            .into(viewImage);
                    removeimage.setVisibility(View.GONE);
                    Log.d("pathofimgae","else");
                }

            }
        });
//---------------Idmatch for all fields-------
        textInputLayoutfname=findViewById(R.id.textinputfname);
        textInputLayoutlname=findViewById(R.id.textinputlname);
        //   textInputLayoutidnum=findViewById(R.id.textinputidnum);
        //For TextInput Edit Text
        etfname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String FirstName=etfname.getText().toString();
                if (FirstName.length()>0 && FirstName.length()<3)
                {
                    textInputLayoutfname.setError("Minimum length 3 charecters");
                    textInputLayoutfname.setErrorEnabled(true);

                    if (!FirstName.matches("^[A-Za-z]+$"))
                    {
                        textInputLayoutfname.setError("Invalid Name ");
                        textInputLayoutfname.setErrorEnabled(true);
                    }

                }
                else
                {
                    textInputLayoutfname.setErrorEnabled(false);
                }

            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etlname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                final String LastName=etlname.getText().toString();
                if (LastName.length()>0 && LastName.length()<3)
                {
                    textInputLayoutlname.setError("Minimum length 3 charecters");
                    textInputLayoutlname.setErrorEnabled(true);

                    if (!LastName.matches("^[A-Za-z]+$"))
                    {
                        textInputLayoutlname.setError("Invalid Name ");
                        textInputLayoutlname.setErrorEnabled(true);
                    }
                }
                else
                {
                    textInputLayoutlname.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });
        //DatePicker Implementatiom
        etdob1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(General_Profile.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, mDateSetListener, mYear, mMonth, mDay);

                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();

            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                //   Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + dayOfMonth + "/" + year);
                String date = dayOfMonth + "/" + month + "/" + year;
                etdob1.setText(date);

            }
        };

        adharid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImagePickerOptions1();
            }
        });
        viewImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = new Dialog(General_Profile.this);
                dialog.setContentView(R.layout.adhar_dialog);
                ImageView imageView = dialog.findViewById(R.id.imgadhar);
                SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                adharurl = sharedPreferences.getString("prff", null);
//        Log.d("iiiii22",prfimageurl);
                if (adharurl != null) {
                    imageView.setImageURI(Uri.parse(adharurl));
                } else {
                    // imgProfile.setImageResource(R.drawable.uploadphoto);
                    Picasso.with(General_Profile.this).load(R. drawable.dd)
                            .into(imageView);
                }
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);

                dialog.show();
            }
        });
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean firstOpened = preferences.getBoolean("ranBefore", true);
        if(firstOpened) {
            showOverlay();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("ranBefore", false);
            editor.apply();
        }


        }



    private void showOverlay() {
            final Dialog dialog = new Dialog(General_Profile.this, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.setContentView(R.layout.overlay_view);
            RelativeLayout layout = (RelativeLayout) dialog.findViewById(R.id.overlayLayout);
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    dialog.dismiss();
                }
            });

            dialog.show();

            LinearLayout button1Layout = (LinearLayout) dialog.findViewById(R.id.button1Layout);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(mButton3Left + mButtonWidth/2, mButton1Bottom + mButtonHeight + mSpacing, 0, 0);
            button1Layout.setLayoutParams(params);

        }
        private void initAutoCompleteTextView() {
            areaautocompleteTextView = findViewById(R.id.auto);
            areaautocompleteTextView.setThreshold(3);
            areaautocompleteTextView.setOnItemClickListener(autocompleteClickListener);
            adapter = new AutoCompleteAdapter(this, placesClient);
            areaautocompleteTextView.setAdapter(adapter);
        }
        private AdapterView.OnItemClickListener autocompleteClickListener=new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    final AutocompletePrediction item=adapter.getItem(i);
                    String placeID = null;
                    if (item != null) {
                        placeID = item.getPlaceId();
                    }
    //                To specify which data types to return, pass an array of Place.Fields in your FetchPlaceRequest
    //                Use only those fields which are required.
                    List<com.google.android.libraries.places.api.model.Place.Field> placeFields = Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.ID, com.google.android.libraries.places.api.model.Place.Field.NAME, com.google.android.libraries.places.api.model.Place.Field.ADDRESS
                            , Place.Field.LAT_LNG);
                    FetchPlaceRequest request = null;
                    if (placeID != null) {
                        request = FetchPlaceRequest.builder(placeID, placeFields)
                                .build();
                    }
                    if (request != null)
                    {
                        placesClient.fetchPlace(request).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
                            @SuppressLint("SetTextI18n")
                            @Override
                            public void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
                                areaautocompleteTextView.setText(fetchPlaceResponse.getPlace().getName());
                                areaautocompleteTextView.dismissDropDown();

                                try {
                                    addresses = geocoder.getFromLocationName(fetchPlaceResponse.getPlace().getAddress(), 5);
                                    Address location = addresses.get(0);
                                    p1 = new LatLng(location.getLatitude(), location.getLongitude() );
                                    String ff = location.getLocality();
                                    etcity.setText(ff);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                e.printStackTrace();
                                etcity.setText(e.getMessage());
                            }
                        });
                    }


                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        private void fontChanged() {
            Typeface font = Typeface.createFromAsset(getAssets(),"fonts/Uber Move Text.ttf");
            etfname.setTypeface(font);
            etlname.setTypeface(font);
            etdob.setTypeface(font);
            etdob1.setTypeface(font);
            Male.setTypeface(font);
            Female.setTypeface(font);
            Others.setTypeface(font);
            idtv.setTypeface(font);
            areaautocompleteTextView.setTypeface(font);
            etcity.setTypeface(font);
            autoCompleteTextView.setTypeface(font);
            adhartextview.setTypeface(font);
            noadhar.setTypeface(font);
            adharid.setTypeface(font);
            gender.setTypeface(font);


        }
        /*load profile image activity in sharedpreference*/
        private void loadProfil() {
            SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
            prfimageurl = sharedPreferences.getString("prf", null);
    //        Log.d("iiiii22",prfimageurl);
            if (prfimageurl != null) {
                imgProfile.setImageURI(Uri.parse(prfimageurl));
                imgProfile.setEnabled(false);

            } else {
                imgProfile.setEnabled(true);
                //GlideApp.with(this).load(R. drawable.icon)
                      //  .into(imgProfile);
                Picasso.with(getApplicationContext())
                        .load(R.drawable.uploadphoto)
                        .into(imgProfile);
            }

        }
        /*load Adhar image activity in sharedpreference*/
        private void loadAdhar() {
            SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
            adharurl = sharedPreferences.getString("prff", null);
            Log.d("ssss",adharurl);
    //        Log.d("iiiii22",prfimageurl);
            if (!adharurl.isEmpty()){
                removeimage.setVisibility(View.GONE);
                Picasso.with(this).load(R. drawable.dd)
                        .into(viewImage);
            }
            if (adharurl!= null) {
                removeimage.setVisibility(View.VISIBLE);
                viewImage.setImageURI(Uri.parse(adharurl));
                adharid.setEnabled(false);
                idtv.setEnabled(false);
                adharid.setTextColor(Color.GRAY);
                idtv.setTextColor(Color.GRAY);
                Log.d("loadadhar","if  :"+adharurl);

            } else {
                removeimage.setVisibility(View.GONE);
                idtv.setEnabled(true);
                idtv.setTextColor(Color.parseColor("#3F51B5"));
                adharid.setEnabled(true);
                Log.d("loadadhar","else");
                // imgProfile.setImageResource(R.drawable.uploadphoto);
                Picasso.with(this).load(R. drawable.dd)
                        .into(viewImage);
            }

        }

    /*save radio button activity in sharedpreference*/
        private void saveRadioButtons() {
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("Male", Male.isChecked());
            editor.putBoolean("Female", Female.isChecked());
            editor.putBoolean("Others", Others.isChecked());
            editor.apply();
        }
        /*load radio button activity in sharedpreference*/
        private void loadRadioButtons() {
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            Boolean Malebutton=sharedPreferences.getBoolean("Male",false);
            Boolean Femalebutton=sharedPreferences.getBoolean("Female",false);
            Boolean Otherbutton=sharedPreferences.getBoolean("Others",false);
           if (Malebutton.equals(true))
            {
                Male.setEnabled(false);
                Male.setChecked(Malebutton);
                Female.setEnabled(false);
                Others.setEnabled(false);
            }
            else  if (Femalebutton.equals(true))
           {
               Others.setEnabled(false);
               Female.setChecked(Femalebutton);
               Female.setEnabled(false);
               Male.setEnabled(false);
           }
        else  if (Otherbutton.equals(true))
          {
              Others.setChecked(Otherbutton);
              Others.setEnabled(false);
              Male.setEnabled(false);
              Female.setEnabled(false);
          }
        else
           {
               Male.setEnabled(true);
               Female.setEnabled(true);
               Others.setEnabled(true);

             }
           }

        private void dataEnabled() {
            etfname.setEnabled(true);
            etlname.setEnabled(true);
            areaautocompleteTextView.setEnabled(true);
            etdob1.setEnabled(true);
            etdob1.setTextColor(Color.parseColor("#3F51B5"));
            autoCompleteTextView.setEnabled(true);
            Male.setEnabled(true);
            Female.setEnabled(true);
            Others.setEnabled(true);
            adharid.setEnabled(true);
            adharid.setTextColor(Color.parseColor("#3F51B5"));
            imgProfile.setEnabled(true);
            idtv.setEnabled(true);
            idtv.setTextColor(Color.parseColor("#3F51B5"));
        }
        /*load all edittext activity in sharedpreference*/
        private void loadData() {
            SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
            String fname = sharedPreferences.getString(FNAME, null);
            if (fname !=null)
            {
                etfname.setText(fname);
                etfname.setEnabled(false);

            }
            else
            {

                etfname.setEnabled(true);
            }
            String lname = sharedPreferences.getString(LNAME, null);
            if (lname !=null)
            {
                etlname.setText(lname);
                etlname.setEnabled(false);

            }
            else
            {
                etlname.setEnabled(true);
            }
            String address = sharedPreferences.getString(ADDRESS, null);
            if (address !=null)
            {
                autoCompleteTextView.setText(address);
                autoCompleteTextView.setEnabled(false);
            }
            else
            {
                autoCompleteTextView.setEnabled(true);
            }
            String area = sharedPreferences.getString(AREA, null);
            if (area !=null)
            {
                areaautocompleteTextView.setText(area);
                areaautocompleteTextView.setEnabled(false);
            }
            else
            {
                areaautocompleteTextView.setEnabled(true);
            }
            String city = sharedPreferences.getString(CITY, null);
            if (city !=null)
            {
                etcity.setText(city);
                etcity.setEnabled(false);

            }
            else
            {
                etcity.setEnabled(false);
            }
            String dob = sharedPreferences.getString(DOB, null);
            if (dob !=null)
            {
                etdob1.setText(dob);
                etdob1.setEnabled(false);
               etdob1.setTextColor(Color.GRAY);
            }
            else
            {

                etdob1.setEnabled(true);
                etdob1.setTextColor(Color.parseColor("#3F51B5"));
            }
            loadRadioButtons();

        }
        /*save all edittext activity in sharedpreference*/
        private void saveData() {

            SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            if(etfname.getText().toString().isEmpty() || etfname.getText().toString().length()<3)
            {
                Toast.makeText(this, "Data not saved", Toast.LENGTH_SHORT).show();
            }
            else
            {
                editor.putString(FNAME, etfname.getText().toString());
                Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
            }
            if ( etlname.getText().toString().isEmpty() || etlname.getText().toString().length()<3)
            {
                Toast.makeText(this, "Data not  saved", Toast.LENGTH_SHORT).show();
            }
            else
            {
                editor.putString(LNAME, etlname.getText().toString());
                Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
            }
            if (autoCompleteTextView.getText().toString().isEmpty())
            {
                Toast.makeText(this, "Data not  saved", Toast.LENGTH_SHORT).show();
            }
            else
            {
                editor.putString(ADDRESS, autoCompleteTextView.getText().toString());
            }
            if (areaautocompleteTextView.getText().toString().isEmpty())
            {
                Toast.makeText(this, "Data not  saved", Toast.LENGTH_SHORT).show();
            }
            else
            {
                editor.putString(AREA, areaautocompleteTextView.getText().toString());
                Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
            }
            if (etcity.getText().toString().isEmpty())
            {
                Toast.makeText(this, "Data not  saved", Toast.LENGTH_SHORT).show();
            }
            else
            {
                editor.putString(CITY, etcity.getText().toString());
                Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
            }

            if (etdob1.getText().toString().isEmpty())
            {
                Toast.makeText(this, "Data not  saved", Toast.LENGTH_SHORT).show();
            }
            else
            {
                editor.putString(DOB, etdob1.getText().toString());
                Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
            }
            editor.apply();
            saveRadioButtons();

           // Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
        }
        private void loadProfile(String url) {
            Log.d(TAG, "Image cache path:" + url);
            imgProfile.setImageBitmap(bitmap);
            Picasso.with(this).load(url)
                    .into(imgProfile);
        }
        /*popup for gallery and camera started here for Adhar id*/
        private void showImagePickerOptions1() {
            Image_Picker_Activity.showImagePickerOptions1(this, new Image_Picker_Activity.PickerOptionListener() {
                @Override
                public void onTakeCameraSelected() {
                    /*Lounching camera here*/
                    launchCameraIntent1();
                }

                @Override
                public void onChooseGallerySelected() {
                    /*lounching gallery here*/
                    launchGalleryIntent1();
                }
            });
        }
        /*camera started successfully*/
        private void launchCameraIntent1() {
            Intent intent = new Intent(General_Profile.this, Image_Picker_Activity.class);
            intent.putExtra(Image_Picker_Activity.INTENT_IMAGE_PICKER_OPTION, Image_Picker_Activity.REQUEST_IMAGE_CAPTURE);
            // setting aspect ratio
            intent.putExtra(Image_Picker_Activity.INTENT_LOCK_ASPECT_RATIO, true);
            intent.putExtra(Image_Picker_Activity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
            intent.putExtra(Image_Picker_Activity.INTENT_ASPECT_RATIO_Y, 1);
            // setting maximum bitmap width and height
            intent.putExtra(Image_Picker_Activity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
            intent.putExtra(Image_Picker_Activity.INTENT_BITMAP_MAX_WIDTH, 1000);
            intent.putExtra(Image_Picker_Activity.INTENT_BITMAP_MAX_HEIGHT, 1000);

            startActivityForResult(intent, CODE_GALLERY_REQUEST_ID);
        }
        /*gallery started successfully with request image*/
        private void launchGalleryIntent1() {
            Intent intent = new Intent(General_Profile.this, Image_Picker_Activity.class);
            intent.putExtra(Image_Picker_Activity.INTENT_IMAGE_PICKER_OPTION, Image_Picker_Activity.REQUEST_GALLERY_IMAGE);

            // setting aspect ratio
            intent.putExtra(Image_Picker_Activity.INTENT_LOCK_ASPECT_RATIO, true);
            intent.putExtra(Image_Picker_Activity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
            intent.putExtra(Image_Picker_Activity.INTENT_ASPECT_RATIO_Y, 1);
            startActivityForResult(intent, CODE_GALLERY_REQUEST_ID);


        }
        /*popup for gallery and camera started here for alternate id*/
        private void showImagePickerOptions2() {
            Image_Picker_Activity.showImagePickerOptions2(this, new Image_Picker_Activity.PickerOptionListener() {
                @Override
                public void onTakeCameraSelected() {
                    /*Lounching camera here*/
                    launchCameraIntent2();
                }

                @Override
                public void onChooseGallerySelected() {
                    /*lounching gallery here*/
                    launchGalleryIntent2();
                }
            });
        }
        /*camera started successfully*/
        private void launchCameraIntent2() {
            Intent intent = new Intent(General_Profile.this, Image_Picker_Activity.class);
            intent.putExtra(Image_Picker_Activity.INTENT_IMAGE_PICKER_OPTION, Image_Picker_Activity.REQUEST_IMAGE_CAPTURE);
            // setting aspect ratio
            intent.putExtra(Image_Picker_Activity.INTENT_LOCK_ASPECT_RATIO, true);
            intent.putExtra(Image_Picker_Activity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
            intent.putExtra(Image_Picker_Activity.INTENT_ASPECT_RATIO_Y, 1);
            // setting maximum bitmap width and height
            intent.putExtra(Image_Picker_Activity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
            intent.putExtra(Image_Picker_Activity.INTENT_BITMAP_MAX_WIDTH, 1000);
            intent.putExtra(Image_Picker_Activity.INTENT_BITMAP_MAX_HEIGHT, 1000);

            startActivityForResult(intent, IMAGE_ALTERNATE);
        }
        /*gallery started successfully with request image*/
        private void launchGalleryIntent2() {
            Intent intent = new Intent(General_Profile.this, Image_Picker_Activity.class);
            intent.putExtra(Image_Picker_Activity.INTENT_IMAGE_PICKER_OPTION, Image_Picker_Activity.REQUEST_GALLERY_IMAGE);

            // setting aspect ratio
            intent.putExtra(Image_Picker_Activity.INTENT_LOCK_ASPECT_RATIO, true);
            intent.putExtra(Image_Picker_Activity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
            intent.putExtra(Image_Picker_Activity.INTENT_ASPECT_RATIO_Y, 1);
            startActivityForResult(intent, IMAGE_ALTERNATE);


        }
        /*onlick image profile checking permission for gallery and camera*/
        @OnClick({R.id.img_profile})
        void onProfileImageClick() {
            Dexter.withActivity(this)
                    .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if (report.areAllPermissionsGranted()) {
                                /* if permission granted called this method*/
                                showImagePickerOptions();
                            }

                            if (report.isAnyPermissionPermanentlyDenied()) {
                                /*if permission deny opening setting page for accepting permission for gallery and camera*/
                                showSettingsDialog();
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            token.continuePermissionRequest();

                        }

                    }).check();
        }
        private void showImagePickerOptions() {
            Image_Picker_Activity.showImagePickerOptions(this, new Image_Picker_Activity.PickerOptionListener() {
                @Override
                public void onTakeCameraSelected() {
                    /*Lounching camera here*/
                    launchCameraIntent();
                }

                @Override
                public void onChooseGallerySelected() {
                    /*lounching gallery here*/
                    launchGalleryIntent();
                }
            });
        }
        /*camera started successfully*/
        private void launchCameraIntent() {
            Intent intent = new Intent(General_Profile.this, Image_Picker_Activity.class);
            intent.putExtra(Image_Picker_Activity.INTENT_IMAGE_PICKER_OPTION, Image_Picker_Activity.REQUEST_IMAGE_CAPTURE);
            // setting aspect ratio
            intent.putExtra(Image_Picker_Activity.INTENT_LOCK_ASPECT_RATIO, true);
            intent.putExtra(Image_Picker_Activity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
            intent.putExtra(Image_Picker_Activity.INTENT_ASPECT_RATIO_Y, 1);
            // setting maximum bitmap width and height
            intent.putExtra(Image_Picker_Activity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
            intent.putExtra(Image_Picker_Activity.INTENT_BITMAP_MAX_WIDTH, 1000);
            intent.putExtra(Image_Picker_Activity.INTENT_BITMAP_MAX_HEIGHT, 1000);

            startActivityForResult(intent, REQUEST_IMAGE);
        }
        /*gallery started successfully with request image*/
        private void launchGalleryIntent() {
            Intent intent = new Intent(General_Profile.this, Image_Picker_Activity.class);
            intent.putExtra(Image_Picker_Activity.INTENT_IMAGE_PICKER_OPTION, Image_Picker_Activity.REQUEST_GALLERY_IMAGE);

            // setting aspect ratio
            intent.putExtra(Image_Picker_Activity.INTENT_LOCK_ASPECT_RATIO, true);
            intent.putExtra(Image_Picker_Activity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
            intent.putExtra(Image_Picker_Activity.INTENT_ASPECT_RATIO_Y, 1);
            startActivityForResult(intent, REQUEST_IMAGE);


        }
        /*validation started here after succesfully validation calling nectbutton method for posting all value taken from general profile activity */
        private void profileValidation() {
            final String FirstName = etfname.getText().toString();
            final String LastName = etlname.getText().toString();
            if (FirstName.length() > 0 && FirstName.length() < 3) {
                textInputLayoutfname.setError("Minimum length 3 charecters");
                textInputLayoutfname.setErrorEnabled(true);
                Snackbar.make(findViewById(android.R.id.content), "Please Check First Name", Snackbar.LENGTH_LONG).show();
                progressDialog.dismiss();
                if (!FirstName.matches("^[A-Za-z]+$")) {
                    textInputLayoutfname.setError("Invalid Name ");
                    textInputLayoutfname.setErrorEnabled(true);
                    progressDialog.dismiss();
                }
            }

            else if (LastName.length() > 0 && LastName.length() < 3) {
                textInputLayoutlname.setError("Minimum length 3 charecters");
                textInputLayoutlname.setErrorEnabled(true);
                progressDialog.dismiss();
                Snackbar.make(findViewById(android.R.id.content), "Please Check Last Name", Snackbar.LENGTH_LONG).show();
                if (!LastName.matches("^[A-Za-z]+$")) {
                    textInputLayoutlname.setError("Invalid Name ");
                    textInputLayoutlname.setErrorEnabled(true);
                    progressDialog.dismiss();
                    Snackbar.make(findViewById(android.R.id.content), "Please Check Last Name", Snackbar.LENGTH_LONG).show();
                }
            }
            else
            {
                if (internetConnection.isConnectingToInternet()) {
                  nextButton();
                    //  postgeneral();
                }
                else
                {
                    progressDialog.dismiss();
                    Snackbar.make(findViewById(android.R.id.content), "Check your internet connection", Snackbar.LENGTH_LONG).show();
                }

            }
        }



    /*opening popup for alternate id image,id type and id number*/
        public void uploadid(View view)
        {
            String img;
            dialog = new Dialog(General_Profile.this);
            dialog.setContentView(R.layout.id_upload);
            // set values for custom dialog components - text, image and button
            idno = dialog.findViewById(R.id.idno);
            textView = dialog.findViewById(R.id.uploadbutton);
            idpic=dialog.findViewById(R.id.id);
            alternate=dialog.findViewById(R.id.imagealter);
            buttonok=dialog.findViewById(R.id.buttonok);
            SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
             img = sharedPreferences.getString("alter",null);
            Log.d("first",String.valueOf(img));
            if (img!=null){
                alternate.setImageURI(Uri.parse(img));

                Log.d("aaa",img);
            }else {

                Picasso.with(General_Profile.this).load(R. drawable.dd)
                        .into(alternate);
                Log.d("aaa","else");
            }
            alternate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   Dialog dialog = new Dialog(General_Profile.this);
                    dialog.setContentView(R.layout.alternateimageidupload);
                    ImageView imageView = dialog.findViewById(R.id.imageidalterupload);
                    if (img!=null){
                        imageView.setImageURI(Uri.parse(img));
                    }else {
                        Picasso.with(General_Profile.this).load(R. drawable.dd)
                                .into(imageView);
                    }
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
                    dialog.show();
                }
            });
            /*load dialog box  value from shared preference*/
            SharedPreferences LastSelect=getSharedPreferences("LastSetting",Context.MODE_PRIVATE);
            SharedPreferences.Editor editorsharedpref=LastSelect.edit();
             int lastClick=LastSelect.getInt("lastClick",0);
            String gg=LastSelect.getString("ggg",null);

            if (gg!=null){
                idno.setText(gg);
            }
            else {
                idno.setEnabled(true);

            }

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*on a text click openning popup for gallery and camera*/
                    //selectAlternateImage();
                    showImagePickerOptions2();
                }
            });
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
            dialog.show();
            // Get reference of widgets from XML layout
            final Spinner  spinner = (Spinner) dialog.findViewById(R.id.id_spinner);

            // Initializing a String Array
            String[] id = new String[]{
                    "Id type",
                    "Driving Liscence",
                    "Pan Card",
                    "Passport"

            };
            final List<String> idskill = new ArrayList<>(Arrays.asList(id));

            // Initializing an ArrayAdapter

          final    ArrayAdapter<String> idAdapter = new ArrayAdapter<String>(this,R.layout.spinner,idskill)
            {
                @Override
                public boolean isEnabled(int position) {
                    if(position == 0)
                    {
                        // Disable the first item from Spinner
                        // First item will be use for hint
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View view=super.getDropDownView(position,convertView,parent);
                    TextView tv_secondary_skill=(TextView) view;
                    if(position == 0){
                        // Set the hint text color gray
                        tv_secondary_skill.setTextColor(Color.GRAY);
                    }
                    else {
                        tv_secondary_skill.setTextColor(Color.BLACK);
                    }
                    return view;
                }
            };
            idAdapter.setDropDownViewResource(R.layout.spinner);
            spinner.setAdapter(idAdapter);
            spinner.setSelection(lastClick);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    editorsharedpref.putInt("lastClick",position).apply();
                    selectedItemText = (String) parent.getItemAtPosition(position);

                    // If user change the default selection
                    // First item is disable and it is used for hint
                    if(position > 0){
                        // Notify the selected item text
                        Toast.makeText
                                (getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                                .show();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            buttonok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Id = idno.getText().toString();
                    SharedPreferences sharedPreferences = getSharedPreferences("PREF_FROM_ALTER", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("idnumber",Id);
                    editor.putString("idname",selectedItemText);
                    editor.putString("idimage",img);
                    editor.apply();

                    if (Id.isEmpty()){
                        idtv.setEnabled(true);
                        idtv.setTextColor(Color.parseColor("#3F51B5"));
                        Toast.makeText(General_Profile.this, "id number not uploaded", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        idtv.setTextColor(Color.GRAY);
                        editorsharedpref.putString("ggg",Id);
                    }


                    editorsharedpref.apply();
                    Changealternatetext();
                    // dialog.dismiss();
                    dialog.cancel();

                }
            });
        }

    private void Changealternatetext() {
        SharedPreferences sharedPreferences = getSharedPreferences("PREF_FROM_ALTER", MODE_PRIVATE);
        String s = sharedPreferences.getString("idnumber",null);
        String p = sharedPreferences.getString("idname",null);
        String q = sharedPreferences.getString("idimage",null);
        if (s!=null && p!=null && q!=null){
            idtv.setText("Alternate Id Uploaded");

        }else {
            idtv.setText("Upload Alternate Id");
        }

    }


    @Override
    protected void onStart() {
        Changealternatetext();
      //  loadAdhar();
        super.onStart();
    }

    @Override
    protected void onResume() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        adharurl = sharedPreferences.getString("prff", null);
if (adharurl==null){
    Log.d("resume",adharurl+"  kuch");
    Picasso.with(General_Profile.this).load(R. drawable.dd)
            .into(viewImage);
    removeimage.setVisibility(View.GONE);

}else {
    loadAdhar();
    Log.d("resume","else");
}


        super.onResume();
    }

    /*posting through post api into server started here*/
        private void nextButton() {
            sharedPreferences = getApplicationContext().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
            final String firstname = etfname.getText().toString();
            final String lname = etlname.getText().toString();
            final String gender=selectedType.toString();
            final String dateofbirth = etdob1.getText().toString();
            final String adress = autoCompleteTextView.getText().toString();
            final String area = areaautocompleteTextView.getText().toString();
            final String city = etcity.getText().toString();
            //converting image to base64 string
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap = ((BitmapDrawable) imgProfile.getDrawable()).getBitmap();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            imageBytes = baos.toByteArray();
            final String imgProfile1 = Base64.encodeToString(imageBytes, Base64.DEFAULT);
            Log.d("converted to string",imgProfile1);
            ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
            bitmap3 = ((BitmapDrawable) viewImage.getDrawable()).getBitmap();
            bitmap3.compress(Bitmap.CompressFormat.JPEG, 100, baos1);
            imageBytes1 = baos1.toByteArray();
            final String imgid = Base64.encodeToString(imageBytes1, Base64.DEFAULT);
            Log.d("converted to string22",imgid);

            ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
            bitmap2 = ((BitmapDrawable) viewImage.getDrawable()).getBitmap();
            bitmap2.compress(Bitmap.CompressFormat.JPEG, 100, baos2);
            imageBytes2 = baos2.toByteArray();
            final String alternate = Base64.encodeToString(imageBytes2, Base64.DEFAULT);
            Log.d("converted to string33",alternate);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://newsunshinegroups.com/pepow/api/worker_general_profile.php", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("msg");
                        Toast.makeText(getApplicationContext(), status + "cccc " + message + "  ", Toast.LENGTH_LONG).show();
                        if (status.equalsIgnoreCase("Success")) {
                            if (etfname.equals(fname)){
                                etfname.setEnabled(false);
                                Log.d("sss","if");
                            }else if (etfname.equals("")){
                                etfname.setEnabled(true);
                                Log.d("sss","else");
                            }
                            progressDialog.dismiss();
                            imgProfile.setEnabled(false);
                            idtv.setEnabled(false);
                            idtv.setTextColor(Color.GRAY);
                            adharid.setTextColor(Color.GRAY);
                            floatingActionButton.setEnabled(true);
                            saveData();
                            loadData();
                            Toast.makeText(General_Profile.this, "success", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(General_Profile.this, Bank_Details_regd.class);
                            startActivity(intent);
                            finish();
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(General_Profile.this, "Please Try Again..", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e1) {
                        progressDialog.dismiss();
                        e1.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    Log.d("Somthing",error.toString());
                    Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put(KEY_FIRST, firstname);
                    params.put(KEY_lAST, lname);
                    params.put(KEY_ADDRESS, adress);
                    params.put(KEY_AREA,area);
                    params.put(KEY_CITY, city);
                    params.put(KEY_DATE_OF_BIRTH, dateofbirth);
                    params.put(KEY_GENDER, gender);
                    params.put(KEY_ID_NO, Id);
                    params.put(KEY_PROFILE, imgProfile1);
                    params.put(KEY_ID_PROOF, imgid);
                    params.put(KEY_ID_IMAGE,alternate);
                    params.put(KEY_ID_TYPE,selectedItemText);
                    Log.d("igm",imgid);
                    return params;
                }
            };
            stringRequest.setShouldCache(false);
            int socketTimeout = 5000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            stringRequest.setRetryPolicy(policy);
            RequestQueue requestQueue = Volley.newRequestQueue(General_Profile.this);
            requestQueue.add(stringRequest);

        }
        private void postgeneral() {
            //converting image to base64 string
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap = ((BitmapDrawable) imgProfile.getDrawable()).getBitmap();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            imageBytes = baos.toByteArray();
            final String imgProfile1 = Base64.encodeToString(imageBytes, Base64.DEFAULT);

            ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
            bitmap3 = ((BitmapDrawable) viewImage.getDrawable()).getBitmap();
            bitmap3.compress(Bitmap.CompressFormat.JPEG, 100, baos1);
            imageBytes1 = baos1.toByteArray();
            final String imgid = Base64.encodeToString(imageBytes1, Base64.DEFAULT);

            ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
            bitmap2 = ((BitmapDrawable) viewImage.getDrawable()).getBitmap();
            bitmap2.compress(Bitmap.CompressFormat.JPEG, 100, baos2);
            imageBytes2 = baos2.toByteArray();
            final String alternate = Base64.encodeToString(imageBytes2, Base64.DEFAULT);
            final String gender=selectedType.toString();


            jsonObject.addProperty("user_id",user_id);
            jsonObject.addProperty("first_name",etfname.getText().toString());
            jsonObject.addProperty("last_name",etlname.getText().toString());
            jsonObject.addProperty("id_no",Id);
            jsonObject.addProperty("id_type",selectedItemText);
            jsonObject.addProperty("address",autoCompleteTextView.getText().toString());
            jsonObject.addProperty("area",areaautocompleteTextView.getText().toString());
            jsonObject.addProperty("city",etcity.getText().toString());
            jsonObject.addProperty("date_of_birth",etdob1.getText().toString());
            jsonObject.addProperty("gender",gender);
            jsonObject.addProperty("profile_pic",imgProfile1);
            jsonObject.addProperty("adhar_image",imgid);
            jsonObject.addProperty("id_image",alternate);
          /*  HashMap<String,String> hashMap = new HashMap<>();
            hashMap.put("first_name",etfname.getText().toString());
            hashMap.put("last_name",etlname.getText().toString());
            hashMap.put("id_no",Id);
            hashMap.put("id_type",selectedItemText);
            hashMap.put("address",autoCompleteTextView.getText().toString());
            hashMap.put("area",areaautocompleteTextView.getText().toString());
            hashMap.put("city",etcity.getText().toString());
            hashMap.put("date_of_birth",etdob1.getText().toString());
            hashMap.put("gender",gender);
            hashMap.put("profile_pic",imgProfile1);
            hashMap.put("adhar_image",imgid);
            hashMap.put("id_image",alternate);*/
            RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://newsunshinegroups.com/pepow/api/");
            Call<General_Repo> call =jsonpost.postgeneral(jsonObject);
            call.enqueue(new Callback<General_Repo>() {
                @Override
                public void onResponse(Call<General_Repo> call, retrofit2.Response<General_Repo> response) {
                    if (response.isSuccessful()){
                        Toast.makeText(General_Profile.this, "success", Toast.LENGTH_SHORT).show();
                    }else {
                        Log.d("else",response.message());
                        Toast.makeText(General_Profile.this, "somthing wrong", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<General_Repo> call, Throwable t) {
                    Log.d("failed",t.getMessage());
                    Toast.makeText(General_Profile.this, "try again", Toast.LENGTH_SHORT).show();

                }
            });
        }


        @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent=new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent,"Select Image"),CODE_GALLERY_REQUEST_ID);

            }
            else {
                Toast.makeText(getApplicationContext(),"You dont have permission",Toast.LENGTH_LONG).show();
            }

            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        @Override
        protected void onActivityResult(int requestCode, int resultCode,  Intent data) {

    /*checking selected Profile_image from gallery/camera's requestcode is equal with request image or not if yes then go on*/
            if (requestCode == REQUEST_IMAGE) {

                if (resultCode == Activity.RESULT_OK) {
                    uriprf = data.getParcelableExtra("path");
                    try {
                        // You can update this bitmap to your server
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uriprf);
                        imgProfile.setImageBitmap(bitmap);
                         path= uriprf.getPath();
                        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("prf",path);
                        Log.d("iiiii",path);
                        editor.apply();
                        // loading profile image from local cache
                        loadProfile(uriprf.toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            /*checking selected Adhar_image from gallery/camera's requestcode is equal with request image or not if yes then go on*/
            if (requestCode==CODE_GALLERY_REQUEST_ID){
                uri=data.getParcelableExtra("path");
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                    viewImage.setImageBitmap(bitmap);
                    ss = uri.getPath();
                    SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("prff",ss);
                    Log.d("pathiii9i",ss);
                    editor.apply();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            /*checking selected Alternate_image_ID from gallery/camera's requestcode is equal with request image or not if yes then go on*/
            if (requestCode==IMAGE_ALTERNATE){
                uri1=data.getParcelableExtra("path");
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri1);
                    alternate.setImageBitmap(bitmap);
                     alt = uri1.getPath();
                    SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("alter",alt);
                   Log.d("alter",alt);
                   editor.apply();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            super.onActivityResult(requestCode, resultCode, data);
        }

     /*Open Setting Activity for pemission of open gallery/Camera*/
        private void showSettingsDialog() {
            AlertDialog.Builder builder = new AlertDialog.Builder(General_Profile.this);
            builder.setTitle(getString(R.string.dialog_permission_title));
            builder.setMessage(getString(R.string.dialog_permission_message));
            builder.setPositiveButton(getString(R.string.go_to_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    General_Profile.this.openSettings();
                }
            });
            builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.show();

        }
        // navigating user to app settings
        private void openSettings() {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", getPackageName(), null);
            intent.setData(uri);
            startActivityForResult(intent, 101);
        }
    /*data save or edited here*/
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.edit_option, menu);
            editmenu = menu.findItem(R.id.edit);
            editmenu.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem menuItem) {
                    dataEnabled();
                    etcity.setEnabled(false);
                    return false;
                }
            });
            return true;
        }
    }
