package com.demo.pepowworkerapp.Activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.Bank_Repo;
import com.demo.pepowworkerapp.Response.General_Repo;
import com.demo.pepowworkerapp.Utils.DialogLoader;
import com.demo.pepowworkerapp.Utils.InternetConnection;
import com.demo.pepowworkerapp.Utils.SharedPreferenceClass;
import com.demo.pepowworkerapp.network.RetrofitInterface;
import com.demo.pepowworkerapp.network.ServiceGenerator;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

import static com.demo.pepowworkerapp.Activity.General_Profile.REQUEST_IMAGE;

public class Bank_Details_regd extends AppCompatActivity {
    Button next;
    EditText et_bname,et_acc_num,et_ifsc,et_branch;
    EditText img_camera_id;
    TextView tvbnk,chequetextview;
    private String user_id;
    String path;
    ImageView chq_image,removecheque;
    TextInputLayout ifsctextinput,textinputbname,textinputaccntno,textinputbranch;
    //Editable Functionality
    private MenuItem editmenu;
    private MenuItem savemenu;
    //Image upload code----------------
    final int CODE_GALLERY_REQUEST=999;
    Bitmap bitmap,bitmap2;
    // DialogLoader dialogLoader;
    final int CODE_GALLERY_REQUEST_CHEQUE=1;
    private SharedPreferenceClass sharedPreferenceClass;
    SharedPreferences sharedPreferences;
    private InternetConnection internetConnection;
//----BANK DETAILS KEY PARAMETRES
    public static  String KEY_BANK_NAME = "bank_name";
    public static  String KEY_ACCOUNT_NUM = "account_no";
    public static  String KEY_IFSC_CODE = "ifsc_code";
    public static  String KEY_BRANCH = "branch";
    public static final String CHEQUE="cheque";
    String ifsccode;
    Uri uri;
    private String Jsonresponse;
    private String url = "https://ifsc.razorpay.com/";
    private int MY_SOCKET_TIMEOUT_MS = 500;
    //------sharedpreference keys-------//
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String BANKNAME = "bankname";
    public static final String IFSCCODE = "ifsccode";
    public static final String ACCOUNT = "account";
    public static final String BRANCHNAME = "branchname";
    String bank,branch,ifsc,account;
    String imgcheck;
    String  prfimageurl;
    Dialog dialog;
    JsonObject jsonObject;
    ProgressDialog progressDialog;
    FloatingActionButton floatingActionButton;
    private int mSpacing = 20, mButtonHeight = 0, mButtonWidth = 0, mButton1Bottom = 0, mButton1Left = 0, mButton2Bottom = 0, mButton2Left = 0, mButton3Bottom = 0, mButton3Left = 0, mTextViewHeight = 200, mTextViewTop = 0, mTextViewLeft = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank__details_regd);
        jsonObject = new JsonObject();
        statusmethod();
        //get the userid from sharedpreference class
        sharedPreferenceClass = new SharedPreferenceClass(this);
        sharedPreferenceClass = new SharedPreferenceClass(Bank_Details_regd.this);
        user_id = sharedPreferenceClass.getValue_string("user_id");
        internetConnection = new InternetConnection(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        tvbnk=findViewById(R.id.tvbnk);
        et_bname=findViewById(R.id.bname);
        et_acc_num=findViewById(R.id.account_num);
        et_ifsc=findViewById(R.id.ifsc_code);
        et_branch=findViewById(R.id.branch);
        chq_image=findViewById(R.id.cheque_img);
         img_camera_id=findViewById(R.id.img_camera_id);
        ifsctextinput=findViewById(R.id.textinputifsc);
        chequetextview=findViewById(R.id.chequetextview);
        textinputbname=findViewById(R.id.textinputbname);
        textinputaccntno=findViewById(R.id.textinputaccntno);
        textinputbranch=findViewById(R.id.textinputbranch);
        removecheque=findViewById(R.id.removechecqeimage);
        progressDialog=new ProgressDialog(Bank_Details_regd.this);
       // loadCheckDefault();
       // loadProfil();
        // next.setOnClickListener(this);
        Toolbar toolbar_bank = (Toolbar) findViewById(R.id.toolbar_bank);
        setSupportActionBar(toolbar_bank);
        //give a heading to toolbar
        getSupportActionBar().setTitle("Banking Details");
        toolbar_bank.setTitleTextColor(Color.WHITE);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Floating action button setup
       floatingActionButton=findViewById(R.id.fab_bank);
       flotingmethod();
        //backbutton implementation
        toolbar_bank.setNavigationIcon(R.drawable.arrowback);
        toolbar_bank.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        img_camera_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ActivityCompat.requestPermissions(Bank_Details_regd.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CODE_GALLERY_REQUEST_CHEQUE);
                showImagePickerOptions();
            }
        });
        removecheque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
               // prfimageurl
                File casted_image = new File(prfimageurl);
                if (casted_image.exists()) {
                    editor.remove("prf1");
                    editor.commit();
                    casted_image.deleteOnExit();
                    casted_image.delete();
                    removecheque.setVisibility(View.GONE);
                    Picasso.with(Bank_Details_regd.this).load(R.drawable.dd)
                            .into(chq_image);

                    //  loadAdhar();
                    Log.d("pathofimgae","if");

                }else {
                    Picasso.with(Bank_Details_regd.this).load(R. drawable.dd)
                            .into(chq_image);
                    removecheque.setVisibility(View.GONE);
                    Log.d("pathofimgae","else");
                }
            }
        });
        sharedPreferenceClass = new SharedPreferenceClass(this);
        internetConnection = new InternetConnection(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        et_ifsc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                checkDataEntered();
                ifsccode = et_ifsc.getText().toString();
                if(ifsccode.equals("") || ifsccode.length()>11 || ifsccode.length()<11){
                   // Toast.makeText(Bank_Details_regd.this, "please enter valid IFSC code", Toast.LENGTH_SHORT).show();
                    ifsctextinput.setError("Please enter valid ifsc code");
                    ifsctextinput.setErrorEnabled(true);
                    return;
                }else {
                    ifsctextinput.setErrorEnabled(false);
                    loadIFSCData();
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        loadData();
        //Custom font style change
        fontChanged();
        chq_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog = new Dialog(Bank_Details_regd.this);
                dialog.setContentView(R.layout.cheque_dialog);

                ImageView imageView = dialog.findViewById(R.id.imgadhar);
                SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                prfimageurl = sharedPreferences.getString("prf1", null);
//        Log.d("iiiii22",prfimageurl);
                if (prfimageurl != null) {
                    imageView.setImageURI(Uri.parse(prfimageurl));
                } else {
                    // imgProfile.setImageResource(R.drawable.uploadphoto);
                    Picasso.with(Bank_Details_regd.this).load(R. drawable.dd)
                            .into(imageView);
                }
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
                dialog.show();

            }
        });

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean firstOpened = preferences.getBoolean("ranBefore", true);
        if(firstOpened) {
            showOverlay();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("ranBefore", false);
            editor.apply();
        }
    }

    private void flotingmethod() {
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setTitle("Loading....");
                progressDialog.setMessage("Sending data to server");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setProgress(0);
                progressDialog.setMax(20);
                progressDialog.show();
                bankValidation();
                saveData();
                loadData();
            }
        });
    }

    private void statusmethod() {
        //status bar colour change
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
    }


    private void showOverlay() {
        final Dialog dialog = new Dialog(Bank_Details_regd.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.overlay_view);
        RelativeLayout layout = (RelativeLayout) dialog.findViewById(R.id.overlayLayout);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });

        dialog.show();

        LinearLayout button1Layout = (LinearLayout) dialog.findViewById(R.id.button1Layout);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(mButton3Left + mButtonWidth/2, mButton1Bottom + mButtonHeight + mSpacing, 0, 0);
        button1Layout.setLayoutParams(params);

    }

    private void fontChanged() {
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/Uber Move Text.ttf");
        tvbnk.setTypeface(font);
        et_bname.setTypeface(font);
        et_acc_num.setTypeface(font);
        et_ifsc.setTypeface(font);
        et_branch.setTypeface(font);
        img_camera_id.setTypeface(font);
        ifsctextinput.setTypeface(font);
        chequetextview.setTypeface(font);
        textinputbname.setTypeface(font);
        textinputaccntno.setTypeface(font);
        textinputbranch.setTypeface(font);
    }

    private void loadProfil() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        prfimageurl = sharedPreferences.getString("prf1", null);
        if (prfimageurl==null){
            removecheque.setVisibility(View.GONE);
            Picasso.with(this).load(R. drawable.dd)
                    .into(chq_image);
        }
//        Log.d("iiiii22",prfimageurl);
        if (prfimageurl != null) {
            removecheque.setVisibility(View.VISIBLE);
            chq_image.setImageURI(Uri.parse(prfimageurl));
        } else {
            removecheque.setVisibility(View.GONE);
            // imgProfile.setImageResource(R.drawable.uploadphoto);
            Picasso.with(this).load(R. drawable.dd)
                    .into(chq_image);
        }

    }

    private void dataEnable() {
        et_bname.setEnabled(true);
        et_acc_num.setEnabled(true);
        et_branch.setEnabled(true);
        et_ifsc.setEnabled(true);
    }
    /*load data from sharedpreference*/
    private void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        String bank = sharedPreferences.getString(BANKNAME, null);
        if (bank !=null)
        {
            et_bname.setText(bank);
            et_bname.setEnabled(false);

        }

        else
        {
            et_bname.setEnabled(true);
        }
        String   ifsc = sharedPreferences.getString(IFSCCODE, null);
        if (ifsc !=null)
        {
            et_ifsc.setText(ifsc);
            et_ifsc.setEnabled(false);

        }
        else
        {
            et_ifsc.setEnabled(true);
        }
        String  account = sharedPreferences.getString(ACCOUNT, null);
        if (account !=null)
        {
            et_acc_num.setText(account);
            et_acc_num.setEnabled(false);

        }
        else
        {
            et_acc_num.setEnabled(true);
        }

        String   branch = sharedPreferences.getString(BRANCHNAME, null);
        if (branch !=null)
        {
            et_branch.setText(branch);
            et_branch.setEnabled(false);
        }
        else
        {
            et_branch.setEnabled(false);
        }

    }
/*save data in sharedpreference*/
    private void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if(et_bname.getText().toString().isEmpty() )
        {
            Toast.makeText(this, "Data not  saved", Toast.LENGTH_SHORT).show();
        }
        else
        {
            editor.putString(BANKNAME, et_bname.getText().toString());
            Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
        }
        if(et_ifsc.getText().toString().isEmpty() || !et_ifsc.getText().toString().matches("[A-Za-z]{4}[0-9]{7}$"))
        {
            Toast.makeText(this, "Data not  saved", Toast.LENGTH_SHORT).show();
        }
        else
        {
            editor.putString(IFSCCODE, et_ifsc.getText().toString());
            Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
        }
        if(et_acc_num.getText().toString().isEmpty())
        {
            Toast.makeText(this, "Data not  saved", Toast.LENGTH_SHORT).show();
        }
        else
        {
            editor.putString(ACCOUNT, et_acc_num.getText().toString());
            Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
        }
        if(et_branch.getText().toString().isEmpty())
        {
            Toast.makeText(this, "Data not  saved", Toast.LENGTH_SHORT).show();

        }
        else
        {
            editor.putString(BRANCHNAME, et_branch.getText().toString());
            Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
        }
        editor.apply();

    }

    private void loadCheckDefault() {
        Picasso.with(this).load(R.drawable.dd)
                .into(chq_image);
    }
    private void showImagePickerOptions() {
        Image_Picker_Activity.showImagePickerOptions3(this, new Image_Picker_Activity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                /*Lounching camera here*/
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                /*lounching gallery here*/
                launchGalleryIntent();
            }
        });
    }
    /*gallery started successfully with request image*/
    private void launchGalleryIntent() {
        Intent intent = new Intent(Bank_Details_regd.this, Image_Picker_Activity.class);
        intent.putExtra(Image_Picker_Activity.INTENT_IMAGE_PICKER_OPTION, Image_Picker_Activity.REQUEST_GALLERY_IMAGE);
        // setting aspect ratio
        intent.putExtra(Image_Picker_Activity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(Image_Picker_Activity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(Image_Picker_Activity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }
    /*camera started successfully*/
    private void launchCameraIntent() {
        Intent intent = new Intent(Bank_Details_regd.this, Image_Picker_Activity.class);
        intent.putExtra(Image_Picker_Activity.INTENT_IMAGE_PICKER_OPTION, Image_Picker_Activity.REQUEST_IMAGE_CAPTURE);
        // setting aspect ratio
        intent.putExtra(Image_Picker_Activity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(Image_Picker_Activity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(Image_Picker_Activity.INTENT_ASPECT_RATIO_Y, 1);
        // setting maximum bitmap width and height
        intent.putExtra(Image_Picker_Activity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(Image_Picker_Activity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(Image_Picker_Activity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }
/*getting branch name from entered ifsc code from bank*/
    private void loadIFSCData() {
        String json_url = url.concat(ifsccode);
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Loading data....");
        dialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,json_url, null,  new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dialog.dismiss();

                try {
                    String branch = response.getString("BRANCH").toString();
                    String address = response.getString("ADDRESS").toString();
                    String contact = response.getString("CONTACT").toString();
                    String city = response.getString("CITY").toString();
                    String district = response.getString("DISTRICT").toString();
                    String state = response.getString("STATE").toString();
                    String bank = response.getString("BANK").toString();
                    String bankcode = response.getString("BANKCODE").toString();
                    String bank_ifsc = response.getString("IFSC").toString();

                    Jsonresponse = "";
                    Jsonresponse +=branch;
                    et_branch.setText(Jsonresponse);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                Toast.makeText(Bank_Details_regd.this, "Error while Loading...", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy( MY_SOCKET_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);
    }

    boolean isEmpty(EditText text)
    {
        CharSequence str=text.getText().toString();
        return TextUtils.isEmpty(str);
    }

    private boolean checkDataEntered() {
        if(isEmpty(et_ifsc)){
            et_ifsc.setError("Enter IFSC Code");
            return false;
        }

        return true;
    }
/*Validation of ifsc code*/
    private void bankValidation() {
       String ifsccode = et_ifsc.getText().toString();
        if (ifsccode.length()>0  && ifsccode.length()< 11) {
            ifsctextinput.setError("Please enter valid ifsc code");
            ifsctextinput.setErrorEnabled(true);
            progressDialog.dismiss();
            if (!ifsccode.matches("[A-Za-z]{4}[0-9]{7}$")) {
                ifsctextinput.setError("Invalid Ifsc code");
                ifsctextinput.setErrorEnabled(true);
                progressDialog.dismiss();
            }
            else
            {
                ifsctextinput.setErrorEnabled(false);
                loadIFSCData();
            }
        }
        else {
            if (internetConnection.isConnectingToInternet()) {
               save_bank();
                //postgeneral();
            } else {

                Snackbar.make(findViewById(android.R.id.content), "Check your internet connection", Snackbar.LENGTH_LONG).show();
            }
        }
    }

/*Post data*/
    private void save_bank() {
        sharedPreferences = getApplicationContext().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
        final String bname=et_bname.getText().toString();
        final String accntnum=et_acc_num.getText().toString();
        final String ifsc=et_ifsc.getText().toString();
        final String branch=et_branch.getText().toString();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap = ((BitmapDrawable) chq_image.getDrawable()).getBitmap();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
         imgcheck = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        Log.d("converted to string",imgcheck);
//        final String cheque=chq_image.getDrawable().toString();
        StringRequest stringRequest=new StringRequest(Request.Method.POST, "http://newsunshinegroups.com/pepow/api/worker_bank_details.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("msg");

                    Toast.makeText(getApplicationContext(), status + " " + message + "  ", Toast.LENGTH_LONG).show();

                    if (status.equalsIgnoreCase("Success")) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        String bank_name = jsonObject1.getString("bank_name");
                        String acnt_num = jsonObject1.getString("account_no");
                        String ifsc_code = jsonObject1.getString("ifsc_code");
                        String branch = jsonObject1.getString("branch");
                        String cheque_img = jsonObject1.getString("cheque");
                        progressDialog.dismiss();
                        saveData();
                        loadData();
                        //  String chequeimg = chequeimageToString(bitmap);
                        Intent intent=new Intent(Bank_Details_regd.this,Skill_Details.class);
                        startActivity(intent);
                        finish();
                        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(Bank_Details_regd.this);

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("bname", bank_name);
                        editor.putString("accntnum", acnt_num);
                        editor.putString("branch", branch);
                        editor.putString("ifsc",ifsc_code);
                        editor.apply();



                    } else {
                        progressDialog.dismiss();

                    }
                } catch (JSONException e1) {
                    progressDialog.dismiss();
                    e1.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();

            }
        })
        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //  String chequeimg = chequeimageToString(bitmap);
                params.put("user_id",user_id);
                params.put(KEY_BANK_NAME,bname);
                params.put(KEY_ACCOUNT_NUM,accntnum);
                params.put(KEY_IFSC_CODE,ifsc);
                params.put(KEY_BRANCH,branch);
                params.put(CHEQUE,imgcheck);
                return params;

            }
        };
        stringRequest.setShouldCache(false);
        int socketTimeout = 60000; // 30 seconds. You can change it
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        stringRequest.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(Bank_Details_regd.this);
        requestQueue.add(stringRequest);

    }
    private void postgeneral() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap = ((BitmapDrawable) chq_image.getDrawable()).getBitmap();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
        jsonObject.addProperty("user_id",user_id);
        imgcheck = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        jsonObject.addProperty("bank_name",et_bname.getText().toString());
        jsonObject.addProperty("account_no",et_acc_num.getText().toString());
        jsonObject.addProperty("ifsc_code",et_ifsc.getText().toString());
        jsonObject.addProperty("branch",et_branch.getText().toString());
        jsonObject.addProperty("cheque",imgcheck);
            HashMap<String,String> hashMap = new HashMap<>();
            hashMap.put("user_id",user_id);
            hashMap.put("bank_name",et_bname.getText().toString());
            hashMap.put("account_no",et_acc_num.getText().toString());
            hashMap.put("ifsc_code",et_ifsc.getText().toString());
            hashMap.put("branch",et_branch.getText().toString());
            hashMap.put("cheque",imgcheck);
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://newsunshinegroups.com/");
        Call<Bank_Repo> call =jsonpost.postbank(hashMap);
        call.enqueue(new Callback<Bank_Repo>() {
            @Override
            public void onResponse(Call<Bank_Repo> call, retrofit2.Response<Bank_Repo> response) {
                try (Bank_Repo responseBody = response.body()){
                    String s = responseBody.getStatus();
                    Log.d("s",s);
                    if (response.isSuccessful()){
                        Toast.makeText(Bank_Details_regd.this, "success", Toast.LENGTH_SHORT).show();
                    }else {
                        Log.d("else", String.valueOf(response.errorBody()));
                        Toast.makeText(Bank_Details_regd.this, "somthing wrong", Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<Bank_Repo> call, Throwable t) {
                Log.d("failed",t.getMessage());
                Toast.makeText(Bank_Details_regd.this, "try again", Toast.LENGTH_SHORT).show();

            }
        });
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Intent intent=new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent,"Select Image"),CODE_GALLERY_REQUEST);

        }
        else {
            Toast.makeText(getApplicationContext(),"You dont have permission",Toast.LENGTH_LONG).show();
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        /*checking selected Cheque image from gallery/camera's requestcode is equal with request image or not if yes then go on*/
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        switch (requestCode){
            case REQUEST_IMAGE:{
                if (resultCode==RESULT_OK)
                {
                     uri = data.getParcelableExtra("path");
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        chq_image.setImageBitmap(bitmap);
                        path= uri.getPath();
                        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("prf1",path);
                        Log.d("iiiii",path);
                        editor.apply();
                        Uri image = data.getData();
                        File file1 = new File("/storage/sdcard0/DCIM/Camera/"+uri+".jpg");
                        String filename1 = file1.getName();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_option, menu);
        editmenu = menu.findItem(R.id.edit);
        editmenu.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                dataEnable();
                et_branch.setEnabled(false);
                return false;
            }
        });
        return true;
    }


    @Override
    protected void onResume() {
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        prfimageurl = sharedPreferences.getString("prf1", null);
        if (prfimageurl==null){
            Log.d("resume",prfimageurl+"  kuch");
            Picasso.with(Bank_Details_regd.this).load(R. drawable.dd)
                    .into(chq_image);
            removecheque.setVisibility(View.GONE);

        }else {
            loadProfil();
            Log.d("resume","else");
        }


        super.onResume();
    }
}
