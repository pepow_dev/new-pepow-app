package com.demo.pepowworkerapp.Activity;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.demo.pepowworkerapp.Pager.pager;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Utils.SharedPreferenceClass;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Fee_For_Service extends AppCompatActivity implements TabLayout.OnTabSelectedListener,NavigationView.OnNavigationItemSelectedListener {
Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    ActionBarDrawerToggle toggle;
    ImageView imageview;
    Switch switchbutton;
    TextView text;
    private SharedPreferenceClass sharedPreferenceClass;
    String user_id;
    ProgressBar progressprofile,progressname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee__for__service);
        toolbar=findViewById(R.id.toolbar_feeforservice);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.feedrawer);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toggle.getDrawerArrowDrawable().setColor(Color.WHITE);
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_feeservice);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        imageview=header.findViewById(R.id.imageViewprofile);
        progressname=header.findViewById(R.id.nameprogress);
        progressprofile=header.findViewById(R.id.profileprogress);
        progressname.setVisibility(View.VISIBLE);
        progressprofile.setVisibility(View.VISIBLE);
        text=header.findViewById(R.id.name);
        toolbar.setTitle("Fee For Service");
        toolbar.setTitleTextColor(Color.WHITE);
        tabLayout=findViewById(R.id.tab_layout);
        viewPager=findViewById(R.id.viewpager);
        tabLayout.addTab(tabLayout.newTab().setText("Schedule"));
        tabLayout.addTab(tabLayout.newTab().setText("In_Progress"));
        tabLayout.addTab(tabLayout.newTab().setText("Completed"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        pager adapter = new pager(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
       // tabLayout.setupWithViewPager(viewPager);
       tabLayout.setOnTabSelectedListener(this);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        sharedPreferenceClass = new SharedPreferenceClass(this);
        sharedPreferenceClass = new SharedPreferenceClass(Fee_For_Service.this);
        user_id = sharedPreferenceClass.getValue_string("user_id");
        SwitchCompat drawerSwitch = (SwitchCompat) navigationView.getMenu().findItem(R.id.hideme).getActionView();
        drawerSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    Toast.makeText(getApplicationContext(), "Switch on", Toast.LENGTH_SHORT).show();
                    // do stuff
                } else {
                    // do other stuff
                    Toast.makeText(getApplicationContext(), "Switch off", Toast.LENGTH_SHORT).show();

                }
            }
        });

        getProfile();
            }
        /*getting profile image and name from server*/
            private void getProfile() {
        //        final String cheque=chq_image.getDrawable().toString();
                StringRequest stringRequest=new StringRequest(Request.Method.GET, "http://newsunshinegroups.com/pepow/api/get_general_profile.php?user_id="+user_id, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("msg");
                            Log.d("oooo", String.valueOf(jsonObject));
                            if (status.equalsIgnoreCase("Success")){
                                progressname.setVisibility(View.GONE);
                                progressprofile.setVisibility(View.GONE);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                String name =jsonObject1.getString("first_name");
                                String last =jsonObject1.getString("last_name");
                                String image =jsonObject1.getString("profile_pic");
                                text.setText(name+" "+last);
                                text.setTextSize(15);
                                Glide.with(Fee_For_Service.this).load(image).into(imageview);
                            }
                            // Toast.makeText(MainActivity.this, status, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            progressname.setVisibility(View.GONE);
                            progressprofile.setVisibility(View.GONE);
                            e.printStackTrace();
                        }




                    }
                } ,new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressname.setVisibility(View.GONE);
                        progressprofile.setVisibility(View.GONE);

                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id","30");

                        return params;

                    }
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<String, String>();
                        params.put("Content-Type","application/x-www-form-urlencoded");
                        return params;
                    }
                };
                stringRequest.setShouldCache(false);
                int socketTimeout = 60000; // 30 seconds. You can change it
                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

                stringRequest.setRetryPolicy(policy);
                RequestQueue requestQueue = Volley.newRequestQueue(Fee_For_Service.this);
                requestQueue.add(stringRequest);

            }
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
            @Override
            public boolean onOptionsItemSelected(MenuItem item)
            {
                if (toggle.onOptionsItemSelected(item))
                {
                    return true;
                }
                return super.onOptionsItemSelected(item);
            }
            /*navigation implememntion */
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id=menuItem.getItemId();
                if (id==R.id.feeforservice)
                {
                    Intent intent=new Intent(Fee_For_Service.this,Fee_For_Service.class);
                    startActivity(intent);
                    finish();
                }

                else if (id==R.id.employment)
                {
                    Intent intent=new Intent(Fee_For_Service.this,Employment.class);
                    startActivity(intent);
                    finish();
                }else if (id==R.id.profile){
                    Intent intent=new Intent(Fee_For_Service.this,HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.feedrawer);
                drawer.isDrawerOpen(GravityCompat.START);

                return true;
            }
        }
