package com.demo.pepowworkerapp.Activity;

import android.content.Intent;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.demo.pepowworkerapp.R;

public class Verified_Activity extends AppCompatActivity {
    TextView tv1,tv2;
    Button b1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verified_);
        tv1=findViewById(R.id.tvverify);
        tv2=findViewById(R.id.tvsuccverify);
        b1=findViewById(R.id.ok);

        //status bar colour change
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
//custom font style
        fontChanged();

    }

    private void fontChanged() {
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/Uber Move Text.ttf");
        tv1.setTypeface(font);
        tv2.setTypeface(font);
        b1.setTypeface(font);
    }


    public void okkay(View view) {
        Intent intent=new Intent(Verified_Activity.this,HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
