package com.demo.pepowworkerapp.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Handler;
import android.preference.PreferenceManager;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Utils.InternetConnection;
import com.demo.pepowworkerapp.Utils.Server_Links;
import com.demo.pepowworkerapp.Utils.SharedPreferenceClass;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class LoginActivity extends AppCompatActivity {
   // ProgressBar progressBar;
    Handler handler = new Handler();
    Runnable runnable;
    Timer timer;
    TextView  textView;
    TextView tv1,forgotpass;
    EditText etphone, etpassword;
    Button button;
    private InternetConnection internetConnection;
    private SharedPreferenceClass sharedPreferenceClass;
    private SharedPreferences sharedPreferences;
    public static final String USER_PREFS = "SeekingDaddie";
    public static final String KEY_PHONE = "phone_no";
    public static final String KEY_PASSWORD = "password";
    private  CircularProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//login

        //login
        internetConnection = new InternetConnection(this);
       sharedPreferenceClass = new SharedPreferenceClass(this);
        progressBar = findViewById(R.id.loginprogress);

        button = findViewById(R.id.btn_signUp);
        textView = findViewById(R.id.textView);
        textView.setPaintFlags(textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        etphone = findViewById(R.id.etphn);
        etpassword = findViewById(R.id.etpass);
        tv1 = findViewById(R.id.tv1);
        forgotpass = findViewById(R.id.forgottpass);
        forgotpass.setPaintFlags(forgotpass.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        //status bar colour change
        Window window=getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.themecolourblue));
        //custom font style
        Typeface font = Typeface.createFromAsset(getAssets(),"fonts/Uber Move Text.ttf");
        etphone.setTypeface(font);
        etpassword.setTypeface(font);
        tv1.setTypeface(font);
        textView.setTypeface(font);
        forgotpass.setTypeface(font);
        button.setTypeface(font);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/* On button click here, validating all entered data in edit text*/
                validate();
            }
        });

    
    }
/*validation started here*/
    private void validate() {
     String phonenew=etphone.getText().toString();
        String passnew=etpassword.getText().toString();
      //  final   String phno=sharedPreferences.getString("phone", "");

        if (phonenew.equalsIgnoreCase("") || passnew.equalsIgnoreCase(""))
        {

            if(phonenew.equalsIgnoreCase( "")){
                Snackbar.make(findViewById(android.R.id.content), "Please enter registered phone number", Snackbar.LENGTH_LONG).show();
                etphone.requestFocus();
                etphone.setCursorVisible(true);
            }
            else if(passnew.equalsIgnoreCase( "")){
                Snackbar.make(findViewById(android.R.id.content), "Please enter valid password", Snackbar.LENGTH_LONG).show();
                etpassword.requestFocus();
                etpassword.setCursorVisible(true);
            }

        }
        else {
            if (internetConnection.isConnectingToInternet()) {
                login();
            } else {
                Snackbar.make(findViewById(android.R.id.content), "Check your internet connection", Snackbar.LENGTH_LONG).show();
            }
        }

    }
/*posting usemobile and password in server*/
    private void login() {
     final String phonenew=etphone.getText().toString();
        final String passnew=etpassword.getText().toString();
       // sharedPreferences=getApplicationContext().getSharedPreferences("userinfo", Context.MODE_PRIVATE);
       // final   String phno=sharedPreferences.getString("phone", "");

        progressBar.setVisibility(View.VISIBLE);

        sharedPreferences = getApplicationContext().getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE);


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Server_Links.WORKER_LOGIN_URL, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("msg");
                  //  Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                    if (status.equalsIgnoreCase("success")) {

                        JSONObject jsonObject1=jsonObject.getJSONObject("data");
                        String user_id=jsonObject1.getString("id");

                       String  phone_no=jsonObject1.getString("phone_no");
                        Toast.makeText(getApplicationContext(),status + " "  +message + "  "+user_id+" " +phone_no+" " ,Toast.LENGTH_LONG).show();

                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor=sharedPreferences.edit();
                        editor.putString("phone","phone_no");
                        editor.commit();


                      //  sharedPreferenceClass.setPhone_num(phone_no);
                        sharedPreferenceClass.setValue_string("user_id",user_id);
                         progressBar.setVisibility(View.GONE);
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();

                    } else {
                        Snackbar.make(findViewById(android.R.id.content), "Invalid Login ", Snackbar.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                progressBar.setVisibility(View.GONE);
                Snackbar.make(findViewById(android.R.id.content), "Retry again!! ", Snackbar.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError
            {
                Map<String, String> params = new HashMap<String, String>();

                params.put(KEY_PHONE, phonenew);
                params.put(KEY_PASSWORD, passnew);
                return params;
            }
        };
        stringRequest.setShouldCache(false);

        int socketTimeout = 500;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


        etpassword.getText().clear();
        etphone.getText().clear();

    }
/*on button click here,simply open signup activity*/
    public void signup(View view) {
        Intent btnsignup = new Intent(LoginActivity.this, Regd_phone_Activity.class);
        startActivity(btnsignup);
        finish();
        handler = new Handler();
        runnable = new Runnable()
        {
            @Override
            public void run()
            {
                progressBar.setVisibility(View.GONE);
                timer.cancel();
            }
        };
        timer = new Timer();
        timer.schedule(new TimerTask()
        {
            @Override
            public void run()
            {
                handler.post(runnable);
            }
        },10000);

    }
    /*on button click here,simply open forgot password activity*/
    public void forgotpassword(View view) {
        Intent intent=new Intent(LoginActivity.this, ForgotpasswordPhoneActivity.class);
        startActivity(intent);
        finish();

    }
}
