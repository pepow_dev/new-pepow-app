package com.demo.pepowworkerapp.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.demo.pepowworkerapp.Adapter.Emp_Iprogress_Adapter;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.Emp_Data_Inpro;
import com.demo.pepowworkerapp.Response.Emp_Inprogress_Respo;
import com.demo.pepowworkerapp.network.RetrofitInterface;
import com.demo.pepowworkerapp.network.ServiceGenerator;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class In_Progress_Employment extends Fragment {
    public Emp_Iprogress_Adapter emp_iprogress_adapter;
    public RecyclerView recyclerView;
    ProgressDialog proDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_in__progress__employment, container, false);
        recyclerView=view.findViewById(R.id.employment_recycler_inprogress);
        fetchInProEmp();
        proDialog = ProgressDialog.show(getContext(), "Loding..", "Please wait");
        proDialog.create();
        return view;
    }
    /*fetching data from server*/
    private void fetchInProEmp() {
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://newsunshinegroups.com/");
        Call<Emp_Inprogress_Respo> call = jsonpost.postRawJSON4();
        call.enqueue(new Callback<Emp_Inprogress_Respo>() {
            @Override
            public void onResponse(Call<Emp_Inprogress_Respo> call, Response<Emp_Inprogress_Respo> response) {
                if (response.isSuccessful()){
                    proDialog.cancel();
                    List<Emp_Data_Inpro> datumlist= response.body().getData();
                    if (getActivity()!=null){
                        proDialog.cancel();
                        emp_iprogress_adapter=new Emp_Iprogress_Adapter(getContext(),datumlist);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        recyclerView.setAdapter(emp_iprogress_adapter);
                    }
                }else {
                    proDialog.cancel();
                    Log.i("onEmptyResponse", "Returned empty response"); }
            }

            @Override
            public void onFailure(Call<Emp_Inprogress_Respo> call, Throwable t) {
                proDialog.cancel();
                Toast.makeText(getContext(), "Connect Internet", Toast.LENGTH_SHORT).show();
                Log.d("Connect Internet", t.getMessage());
            }
        });
    }

}
