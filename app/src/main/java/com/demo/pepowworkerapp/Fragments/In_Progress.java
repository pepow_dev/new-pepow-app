package com.demo.pepowworkerapp.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demo.pepowworkerapp.Adapter.InProgress_Adapter;
import com.demo.pepowworkerapp.Adapter.Schedule_Adapter;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.Data;
import com.demo.pepowworkerapp.Response.Data_Inprogress;
import com.demo.pepowworkerapp.Response.InProgress_Response;
import com.demo.pepowworkerapp.Response.Payment_Inprogress;
import com.demo.pepowworkerapp.Response.Schedule_Response;
import com.demo.pepowworkerapp.network.RetrofitInterface;
import com.demo.pepowworkerapp.network.ServiceGenerator;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class In_Progress extends Fragment {
    public InProgress_Adapter inProgress_adapter;
    public RecyclerView recyclerView1;
    ProgressDialog proDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_in__progress, container, false);
        recyclerView1 = (RecyclerView) v.findViewById(R.id.recycler_in_progress);
        proDialog = ProgressDialog.show(getContext(), "Loding..", "Please wait");
        proDialog.create();
        fetchInPro();
        return v;
    }
    /*fetching data from server*/
    private void fetchInPro() {
        RetrofitInterface jsonPostService = ServiceGenerator.createService(RetrofitInterface.class, "http://newsunshinegroups.com/");
        Call<InProgress_Response> call = jsonPostService.postRawJSON1();
        call.enqueue(new Callback<InProgress_Response>() {
            @Override
            public void onResponse(Call<InProgress_Response> call, Response<InProgress_Response> response) {
                if (response.isSuccessful()) {
                    proDialog.cancel();
                    List<Data_Inprogress> datumList1 = response.body().getData();
                    /*for (int i = 0; i < datumList.size(); i++) {
                        datumList.add(response.body().getData().get(i));
                    }*/
                    if (getActivity()!=null){
                        proDialog.cancel();
                        inProgress_adapter = new InProgress_Adapter(getContext(),datumList1);
                        recyclerView1.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        //recyclerView.setHasFixedSize(true);
                        //recyclerView.notify();
                        recyclerView1.setAdapter(inProgress_adapter);
                    }



                }
            }

            @Override
            public void onFailure(Call<InProgress_Response> call, Throwable t) {
proDialog.cancel();
            }
        });
    }

}
