package com.demo.pepowworkerapp.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.demo.pepowworkerapp.Adapter.Schedule_Adapter;
import com.demo.pepowworkerapp.R;

import com.demo.pepowworkerapp.Response.Data;
import com.demo.pepowworkerapp.Response.Schedule_Response;
import com.demo.pepowworkerapp.network.RetrofitInterface;
import com.demo.pepowworkerapp.network.ServiceGenerator;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Schedule extends Fragment {
public Schedule_Adapter schedule_adapter;
public RecyclerView recyclerView;
    ProgressDialog proDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_schedule, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        fetchSchedule();
        proDialog = ProgressDialog.show(getContext(), "Loding..", "Please wait");
        proDialog.create();
        return view;
    }


    /*fetching data from server*/
    private void fetchSchedule() {
        RetrofitInterface jsonPostService = ServiceGenerator.createService(RetrofitInterface.class, "http://newsunshinegroups.com/");
        Call<Schedule_Response> call = jsonPostService.postRawJSON();
        call.enqueue(new Callback<Schedule_Response>() {
            @Override
            public void onResponse(Call<Schedule_Response> call, Response<Schedule_Response> response) {
                if (response.isSuccessful()) {
                    proDialog.cancel();
                    List<Data> datumList = response.body().getData();
                    /*for (int i = 0; i < datumList.size(); i++) {
                        datumList.add(response.body().getData().get(i));
                    }*/
                    if (getActivity()!=null){
                        proDialog.cancel();
                        schedule_adapter = new Schedule_Adapter(getContext(),datumList);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        //recyclerView.setHasFixedSize(true);
                        //recyclerView.notify();
                        recyclerView.setAdapter(schedule_adapter);
                    }



                }    else {
                    proDialog.cancel();
                    Log.i("onEmptyResponse", "Returned empty response"); }


            }

            @Override
            public void onFailure(Call<Schedule_Response> call, Throwable t) {
                proDialog.cancel();
                Toast.makeText(getContext(), "Connect Internet", Toast.LENGTH_SHORT).show();
                Log.d("Connect Internet", t.getMessage());
            }
        });
    }

}
