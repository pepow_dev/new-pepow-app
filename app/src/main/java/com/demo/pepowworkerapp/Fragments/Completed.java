package com.demo.pepowworkerapp.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.demo.pepowworkerapp.Adapter.Completed_Adapter;
import com.demo.pepowworkerapp.Adapter.InProgress_Adapter;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.Completed_Response;
import com.demo.pepowworkerapp.Response.Data_Completed;
import com.demo.pepowworkerapp.Response.Data_Inprogress;
import com.demo.pepowworkerapp.Response.InProgress_Response;
import com.demo.pepowworkerapp.network.RetrofitInterface;
import com.demo.pepowworkerapp.network.ServiceGenerator;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Completed extends Fragment {
    public RecyclerView recyclerView2;
    public Completed_Adapter completed_adapter;
    ProgressDialog proDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_completed, container, false);
        recyclerView2 = (RecyclerView) v.findViewById(R.id.recycler_completed);
        proDialog = ProgressDialog.show(getContext(), "Loding..", "Please wait");
        proDialog.create();
        fetchCompleted();
        return v;
    }
/*fetching data from server*/
    private void fetchCompleted() {
        RetrofitInterface jsonPostService = ServiceGenerator.createService(RetrofitInterface.class, "http://newsunshinegroups.com/");
        Call<Completed_Response> call = jsonPostService.postRawJSON2();
    call.enqueue(new Callback<Completed_Response>() {
        @Override
        public void onResponse(Call<Completed_Response> call, Response<Completed_Response> response) {
            if (response.isSuccessful()) {
                proDialog.cancel();
                List<Data_Completed> datumList2 = response.body().getData();
                    /*for (int i = 0; i < datumList.size(); i++) {
                        datumList.add(response.body().getData().get(i));
                    }*/
                    if (getActivity()!=null){
                        proDialog.cancel();
                        completed_adapter = new Completed_Adapter(getContext(),datumList2);
                        recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        recyclerView2.setAdapter(completed_adapter);
                    }
            }
        }

        @Override
        public void onFailure(Call<Completed_Response> call, Throwable t) {
            proDialog.cancel();
        }
    });
    }

}
