package com.demo.pepowworkerapp.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.demo.pepowworkerapp.Adapter.Emp_Completed_Adapter;
import com.demo.pepowworkerapp.Adapter.Emp_Iprogress_Adapter;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.Emp_Completed_Respo;
import com.demo.pepowworkerapp.Response.Emp_Data_Complt;
import com.demo.pepowworkerapp.network.RetrofitInterface;
import com.demo.pepowworkerapp.network.ServiceGenerator;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Completed_Employment extends Fragment {
    public Emp_Completed_Adapter emp_completed_adapter;
    public RecyclerView recyclerView;
    ProgressDialog proDialog;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_completed__employment, container, false);
        recyclerView=view.findViewById(R.id.employment_recycler_completed);
        fetchComplEmp();
        proDialog = ProgressDialog.show(getContext(), "Loding..", "Please wait");
        proDialog.create();
        return view;
    }
/*fetching data from server*/
    private void fetchComplEmp() {
        RetrofitInterface jsonpost = ServiceGenerator.createService(RetrofitInterface.class,"http://newsunshinegroups.com/");
        Call<Emp_Completed_Respo> call =jsonpost.postRawJSON5();
        call.enqueue(new Callback<Emp_Completed_Respo>() {
            @Override
            public void onResponse(Call<Emp_Completed_Respo> call, Response<Emp_Completed_Respo> response) {
                if (response.isSuccessful()){
                    List<Emp_Data_Complt>datumlist = response.body().getData();
                    if (getActivity()!=null){
                        proDialog.cancel();
                        emp_completed_adapter=new Emp_Completed_Adapter(getContext(),datumlist);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        recyclerView.setAdapter(emp_completed_adapter);
                    }
                }else {
                    proDialog.cancel();
                    Log.i("onEmptyResponse", "Returned empty response"); }
            }

            @Override
            public void onFailure(Call<Emp_Completed_Respo> call, Throwable t) {
                proDialog.cancel();
                Toast.makeText(getContext(), "Connect Internet", Toast.LENGTH_SHORT).show();
                Log.d("Connect Internet", t.getMessage());
            }
        });
    }

}
