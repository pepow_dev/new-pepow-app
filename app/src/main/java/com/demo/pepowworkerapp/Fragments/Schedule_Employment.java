package com.demo.pepowworkerapp.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.demo.pepowworkerapp.Adapter.Employment_Schedule_Adapter;
import com.demo.pepowworkerapp.Adapter.Schedule_Adapter;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.Emp_Data_Sched;
import com.demo.pepowworkerapp.Response.Emp_Schedule_Respo;
import com.demo.pepowworkerapp.network.RetrofitInterface;
import com.demo.pepowworkerapp.network.ServiceGenerator;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Schedule_Employment extends Fragment {
    public Employment_Schedule_Adapter employment_schedule_adapter;
    public RecyclerView recyclerView;
    ProgressDialog proDialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_schedule__employment, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.employment_recycler_schedule);
        fetchScheduleEmpt();
        proDialog = ProgressDialog.show(getContext(), "Loding..", "Please wait");
        proDialog.create();
        return view;

    }
    /*fetching data from server*/
    private void fetchScheduleEmpt() {
        RetrofitInterface jsonPost = ServiceGenerator.createService(RetrofitInterface.class,"http://newsunshinegroups.com/");
        Call<Emp_Schedule_Respo> call=jsonPost.postRawJSON3();
        call.enqueue(new Callback<Emp_Schedule_Respo>() {
            @Override
            public void onResponse(Call<Emp_Schedule_Respo> call, Response<Emp_Schedule_Respo> response) {
                if (response.isSuccessful()){
                    proDialog.cancel();
                    List<Emp_Data_Sched> datumlist = response.body().getData();
                    if (getActivity()!=null){
                        proDialog.cancel();
                        employment_schedule_adapter =new Employment_Schedule_Adapter(getContext(),datumlist);
                         recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                        recyclerView.setAdapter(employment_schedule_adapter);
                    }
                } else {
                    proDialog.cancel();
                    Log.i("onEmptyResponse", "Returned empty response"); }
            }
            @Override
            public void onFailure(Call<Emp_Schedule_Respo> call, Throwable t) {
                proDialog.cancel();
                Toast.makeText(getContext(), "Connect Internet", Toast.LENGTH_SHORT).show();
                Log.d("Connect Internet", t.getMessage());
            }
        });

    }

}
