package com.demo.pepowworkerapp.network;

import com.demo.pepowworkerapp.Response.Bank_Repo;
import com.demo.pepowworkerapp.Response.Completed_Response;
import com.demo.pepowworkerapp.Response.Emp_Completed_Respo;
import com.demo.pepowworkerapp.Response.Emp_Inprogress_Respo;
import com.demo.pepowworkerapp.Response.Emp_Schedule_Respo;
import com.demo.pepowworkerapp.Response.General_Repo;
import com.demo.pepowworkerapp.Response.InFo_Post;
import com.demo.pepowworkerapp.Response.InProgress_Response;
import com.demo.pepowworkerapp.Response.Info_Response;
import com.demo.pepowworkerapp.Response.Schedule_Response;
import com.google.gson.JsonObject;

import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface RetrofitInterface {

    @Headers({
            "Content-Type: application/json"
    })
    @GET("pepow/api/order_list.php/scheduled")
    Call<Schedule_Response> postRawJSON();

    @Headers({
            "Content-Type: application/json"
    })
    @GET("pepow/api/order_list.php/inprogress")
    Call<InProgress_Response> postRawJSON1();

    @Headers({
            "Content-Type: application/json"
    })
    @GET("pepow/api/order_list.php/completed")
    Call<Completed_Response> postRawJSON2();

    @Headers({
            "Content-Type: application/json"
    })
    @GET("pepow/api/employment.php/scheduled")
    Call<Emp_Schedule_Respo> postRawJSON3();

    @Headers({
            "Content-Type: application/json"
    })
    @GET("pepow/api/employment.php/inprogress")
    Call<Emp_Inprogress_Respo> postRawJSON4();

    @Headers({
            "Content-Type: application/json"
    })
    @GET("pepow/api/employment.php/completed")
    Call<Emp_Completed_Respo> postRawJSON5();

    @Headers({
            "Content-Type: application/json"
    })
    @GET("pepow/api/get_skill_info.php")
    Call<Info_Response> postRawJSON6(@QueryMap Map<String, String> param);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("worker_skill_details.php")
    Call<InFo_Post> post(@QueryMap Map<String, String> param);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("worker_general_profile.php")
    Call<General_Repo> postgeneral(@Body JsonObject jsonpost);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("pepow/api/worker_bank_details.php")
    Call<Bank_Repo> postbank(@QueryMap Map<String, String> param);
}

