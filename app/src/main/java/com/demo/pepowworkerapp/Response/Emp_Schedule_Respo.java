package com.demo.pepowworkerapp.Response;

import java.util.List;
/*response for employement scheduled*/
public class Emp_Schedule_Respo {
    private String status;
    private String message;
/*data list for employment scheduled*/
    private List<Emp_Data_Sched> data=null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Emp_Data_Sched> getData() {
        return data;
    }

    public void setData(List<Emp_Data_Sched> data) {
        this.data = data;
    }
}
