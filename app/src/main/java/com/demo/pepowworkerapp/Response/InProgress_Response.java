package com.demo.pepowworkerapp.Response;

import java.util.List;
/*response for inprogress*/
public class InProgress_Response {
    private String status;
    private String message;
    /*data list of response*/
    private List<Data_Inprogress> data=null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Data_Inprogress> getData() {
        return data;
    }

    public void setData(List<Data_Inprogress> data) {
        this.data = data;
    }
}
