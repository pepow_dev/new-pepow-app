package com.demo.pepowworkerapp.Response;

public class Emp_Data_Sched {
    private  String description;
    private int work_order_id;
    private  String locaion;
    private  String start_date;
    private  String interview_datetime;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getWork_order_id() {
        return work_order_id;
    }

    public void setWork_order_id(int work_order_id) {
        this.work_order_id = work_order_id;
    }

    public String getLocaion() {
        return locaion;
    }

    public void setLocaion(String locaion) {
        this.locaion = locaion;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getInterview_datetime() {
        return interview_datetime;
    }

    public void setInterview_datetime(String interview_datetime) {
        this.interview_datetime = interview_datetime;
    }
}
