package com.demo.pepowworkerapp.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data_Completed {

    private  String description;
    private int work_order_id;
    private  String locaion;
    private  String date;
    private  String duration;
    private  String user_name;
    private  String user_address;
    @SerializedName("payment_details")
    @Expose
    private List<Payment_Completed> paymentDetails=null;

    public List<Payment_Completed> getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(List<Payment_Completed> paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getWork_order_id() {
        return work_order_id;
    }

    public void setWork_order_id(int work_order_id) {
        this.work_order_id = work_order_id;
    }

    public String getLocaion() {
        return locaion;
    }

    public void setLocaion(String locaion) {
        this.locaion = locaion;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_address() {
        return user_address;
    }

    public void setUser_address(String user_address) {
        this.user_address = user_address;
    }

}
