package com.demo.pepowworkerapp.Response;

import java.util.List;
/*response for schedule*/
public class Schedule_Response {
    private String status;
    private String message;
/*list of data response*/
    private List<Data> data=null;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
