package com.demo.pepowworkerapp.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InFo_Post {
    private String status;
    private String msg;
    private DataModel data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataModel getData() {
        return data;
    }

    public void setData(DataModel data) {
        this.data = data;
    }

    private class DataModel {
        private String skill_years;
        private String skill_where;
        private String about_work;

        @SerializedName("skill_images")
        @Expose
        private List<String> skill_images = null;

        public List<String> getSkill_images() {
            return skill_images;
        }

        public void setSkill_images(List<String> skill_images) {
            this.skill_images = skill_images;
        }

        public String getSkill_years() {
            return skill_years;
        }

        public void setSkill_years(String skill_years) {
            this.skill_years = skill_years;
        }

        public String getSkill_where() {
            return skill_where;
        }

        public void setSkill_where(String skill_where) {
            this.skill_where = skill_where;
        }

        public String getAbout_work() {
            return about_work;
        }

        public void setAbout_work(String about_work) {
            this.about_work = about_work;
        }

    }
}
