package com.demo.pepowworkerapp.Response;

import java.util.List;
/*response for employement completed*/
public class Emp_Completed_Respo {

    private String status;
    private String message;
    /*response of data list of employement data completed*/
    private List<Emp_Data_Complt> data=null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Emp_Data_Complt> getData() {
        return data;
    }

    public void setData(List<Emp_Data_Complt> data) {
        this.data = data;
    }
}
