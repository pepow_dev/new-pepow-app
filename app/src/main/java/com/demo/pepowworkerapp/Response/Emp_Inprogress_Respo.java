package com.demo.pepowworkerapp.Response;

import java.util.List;
/*response for employement inprogress*/
public class Emp_Inprogress_Respo {
    private String status;
    private String message;
    /*response of data list of employement data*/
    private List<Emp_Data_Inpro> data=null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Emp_Data_Inpro> getData() {
        return data;
    }

    public void setData(List<Emp_Data_Inpro> data) {
        this.data = data;
    }
}
