package com.demo.pepowworkerapp.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataList {
    @SerializedName("skill_years")
    @Expose
    private String skill_years;
    @SerializedName("skill_where")
    @Expose
    private String skill_where;
    @SerializedName("about_work")
    @Expose
    private String about_work;
    @SerializedName("images")
    @Expose
    private List<String> images = null;

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getSkill_years() {
        return skill_years;
    }

    public void setSkill_years(String skill_years) {
        this.skill_years = skill_years;
    }

    public String getSkill_where() {
        return skill_where;
    }

    public void setSkill_where(String skill_where) {
        this.skill_where = skill_where;
    }

    public String getAbout_work() {
        return about_work;
    }

    public void setAbout_work(String about_work) {
        this.about_work = about_work;
    }

}
