package com.demo.pepowworkerapp.Response;

import java.util.List;
/*response for completed response*/
public class Completed_Response {
    private String status;
    private String message;
    /*data list response*/
    private List<Data_Completed> data=null;
    /*payment list response*/
    private List<Payment_Inprogress> data1=null;

    public List<Payment_Inprogress> getData1() {
        return data1;
    }

    public void setData1(List<Payment_Inprogress> data1) {
        this.data1 = data1;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Data_Completed> getData() {
        return data;
    }

    public void setData(List<Data_Completed> data) {
        this.data = data;
    }
}
