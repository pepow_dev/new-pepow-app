package com.demo.pepowworkerapp.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.pepowworkerapp.Activity.Skill_Details;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.DataList;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Info_Adapter extends RecyclerView.Adapter<Info_Adapter.InfoViewHolder> {
    private LayoutInflater inflater;
    private List<DataList> modelRecyclerArrayList;
    Context ctx;

    public Info_Adapter(Context ctx, List<DataList> modelRecyclerArrayList) {
        this.inflater = inflater.from(ctx);
        this.modelRecyclerArrayList = modelRecyclerArrayList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public InfoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = inflater.inflate(R.layout.info_adapter, viewGroup, false);
InfoViewHolder holder = new InfoViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Info_Adapter.InfoViewHolder holder, int i) {
        holder.skill_year.setText(modelRecyclerArrayList.get(i).getSkill_years());
        holder.skill_where.setText(modelRecyclerArrayList.get(i).getSkill_where());
        holder.aboutwork.setText(modelRecyclerArrayList.get(i).getAbout_work());
        Picasso.with(ctx).load(String.valueOf(modelRecyclerArrayList.get(i).getImages())).into(holder.thumbnail);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(ctx);
                dialog.setContentView(R.layout.info_dialog);
                TextView textView=dialog.findViewById(R.id.yr);
                TextView textView1=dialog.findViewById(R.id.whr);
                TextView textView2=dialog.findViewById(R.id.abt);
                String y = String.valueOf(((TextView) v.findViewById(R.id.skill_year)).getText());
                String x = String.valueOf(((TextView) v.findViewById(R.id.skill_where)).getText());
                String z = String.valueOf(((TextView) v.findViewById(R.id.about_work)).getText());
                textView.setText(y);
                textView1.setText(x);
                textView2.setText(z);
                dialog.show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return modelRecyclerArrayList.size();
    }

    public class InfoViewHolder extends RecyclerView.ViewHolder {
        TextView skill_year,skill_where,aboutwork;
        ImageView im1,thumbnail;
        public InfoViewHolder(@NonNull View itemView) {
            super(itemView);
            Typeface font = Typeface.createFromAsset(ctx.getAssets(),"fonts/Uber Move Text.ttf");
            skill_year=itemView.findViewById(R.id.skill_year);
            skill_where=itemView.findViewById(R.id.skill_where);
            aboutwork=itemView.findViewById(R.id.about_work);
            im1=itemView.findViewById(R.id.im1);
           thumbnail = (ImageView) itemView.findViewById(R.id.media_image);
           skill_where.setTypeface(font);
            skill_year.setTypeface(font);
            aboutwork.setTypeface(font);
        }
    }
}
