package com.demo.pepowworkerapp.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.Emp_Data_Sched;

import java.util.List;

public class Employment_Schedule_Adapter extends RecyclerView.Adapter<Employment_Schedule_Adapter.Emp_ScheduleViewHolder> {
    private LayoutInflater inflater;
    private List<Emp_Data_Sched> modelRecyclerArrayList;
    Context ctx;

    public Employment_Schedule_Adapter(Context ctx,List<Emp_Data_Sched> modelRecyclerArrayList) {
        this.inflater = inflater.from(ctx);
        this.modelRecyclerArrayList = modelRecyclerArrayList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public Employment_Schedule_Adapter.Emp_ScheduleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = inflater.inflate(R.layout.employment_schedule_adapter, viewGroup, false);
        Emp_ScheduleViewHolder viewHolder = new Emp_ScheduleViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull Employment_Schedule_Adapter.Emp_ScheduleViewHolder holder, int i) {
        holder.location.setText(modelRecyclerArrayList.get(i).getLocaion());
        holder.workerid.setText(String.valueOf(modelRecyclerArrayList.get(i).getWork_order_id()));
        holder.description.setText(modelRecyclerArrayList.get(i).getDescription());
        holder.startdate.setText(modelRecyclerArrayList.get(i).getStart_date());
        holder.interviewdatetime.setText(modelRecyclerArrayList.get(i).getInterview_datetime());
    }

    @Override
    public int getItemCount() {
        return modelRecyclerArrayList.size();
    }

    public class Emp_ScheduleViewHolder extends RecyclerView.ViewHolder {
        TextView workerid,description,location,startdate,interviewdatetime,
         tvwid,tvdesc, tvloc,tvstartdate,tvenddate;
        public Emp_ScheduleViewHolder(@NonNull View itemView) {
            super(itemView);
            Typeface font = Typeface.createFromAsset(ctx.getAssets(),"fonts/Uber Move Text.ttf");
            workerid=itemView.findViewById(R.id.empworkorderid);
            description=itemView.findViewById(R.id.empdescription);
            location=itemView.findViewById(R.id.emplocation);
            startdate=itemView.findViewById(R.id.empdate);
            interviewdatetime=itemView.findViewById(R.id.empinterviewdate_time);

            tvwid=itemView.findViewById(R.id.w);
            tvdesc=itemView.findViewById(R.id.d);
            tvloc=itemView.findViewById(R.id.l);
            tvstartdate=itemView.findViewById(R.id.sd);
            tvenddate=itemView.findViewById(R.id.ed);

            tvwid.setTypeface(font);
            tvdesc.setTypeface(font);
            tvloc.setTypeface(font);
            tvstartdate.setTypeface(font);
            tvenddate.setTypeface(font);


            workerid.setTypeface(font);
            description.setTypeface(font);
            location.setTypeface(font);
            startdate.setTypeface(font);
            interviewdatetime.setTypeface(font);
        }
    }
}
