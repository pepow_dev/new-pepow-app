package com.demo.pepowworkerapp.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.Data_Inprogress;
import com.demo.pepowworkerapp.Response.Payment_Inprogress;

import java.util.List;

public class InProgress_Adapter extends RecyclerView.Adapter<InProgress_Adapter.InProgressViewHolder> {
    private LayoutInflater inflater;
    private List<Data_Inprogress> modelRecyclerArrayList;
    Context ctx;

    public InProgress_Adapter( Context ctx, List<Data_Inprogress> modelRecyclerArrayList) {
        this.inflater = inflater.from(ctx);
        this.modelRecyclerArrayList = modelRecyclerArrayList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public InProgressViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = inflater.inflate(R.layout.in_progress_adapter, viewGroup, false);
        InProgressViewHolder holder = new InProgressViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull InProgressViewHolder holder, int i) {
            holder.location1.setText(modelRecyclerArrayList.get(i).getLocaion());
            holder.date1.setText(modelRecyclerArrayList.get(i).getDate());
            holder.duration1.setText(modelRecyclerArrayList.get(i).getDuration());
            holder.username1.setText(modelRecyclerArrayList.get(i).getUser_name());
            holder.workerid1.setText(String.valueOf(modelRecyclerArrayList.get(i).getWork_order_id()));
            holder.description1.setText(modelRecyclerArrayList.get(i).getDescription());
            holder.useraddress1.setText(modelRecyclerArrayList.get(i).getUser_address());
            holder.amttt.setText(String.valueOf(modelRecyclerArrayList.get(i).getPaymentDetails().get(0).getAmount()));
        holder.blnc.setText(String.valueOf(modelRecyclerArrayList.get(i).getPaymentDetails().get(0).getBalance()));
holder.dtt.setText(modelRecyclerArrayList.get(i).getPaymentDetails().get(0).getDate());
        holder.cs_on.setText(modelRecyclerArrayList.get(i).getPaymentDetails().get(0).getPayment_method());

    }

    @Override
    public int getItemCount() {
        return modelRecyclerArrayList.size();
    }

    public class InProgressViewHolder extends RecyclerView.ViewHolder {
          TextView location1,date1,username1,duration1,workerid1,description1,useraddress1
                ,tvorderid,tvdesc,tvusername,tvdate,tvlocation,tvdur,tvaddre,tvpay,tvamt,tvdt,tvcash,tvbal;
          TextView amttt,dtt,cs_on,blnc;
        public InProgressViewHolder(@NonNull View itemView) {
            super(itemView);
            Typeface font = Typeface.createFromAsset(ctx.getAssets(),"fonts/Uber Move Text.ttf");
            location1=itemView.findViewById(R.id.location1);
            date1=itemView.findViewById(R.id.date1);
            username1=itemView.findViewById(R.id.username1);
            duration1=itemView.findViewById(R.id.duration1);
            workerid1=itemView.findViewById(R.id.workorderid1);
            description1=itemView.findViewById(R.id.description1);
            useraddress1=itemView.findViewById(R.id.user_address1);

            tvorderid=itemView.findViewById(R.id.tvorderid);
            tvdesc=itemView.findViewById(R.id.tvdesc);
            tvusername=itemView.findViewById(R.id.tvusername);
            tvdate= itemView.findViewById(R.id.tvdate);
            tvlocation=itemView.findViewById(R.id.u);
            tvdur=itemView.findViewById(R.id.d);
            tvaddre=itemView.findViewById(R.id.a);
            tvpay=itemView.findViewById(R.id.pt);
            tvamt=itemView.findViewById(R.id.amt);
            tvdt=itemView.findViewById(R.id.dt);
            tvcash=itemView.findViewById(R.id.cash_online);
            tvbal=itemView.findViewById(R.id.Balance);

            amttt=itemView.findViewById(R.id.amttt);
            dtt=itemView.findViewById(R.id.dtt);
            cs_on=itemView.findViewById(R.id.cs_on);
            blnc=itemView.findViewById(R.id.blnc);

            amttt.setTypeface(font);
            dtt.setTypeface(font);
            cs_on.setTypeface(font);
            blnc.setTypeface(font);

            tvorderid.setTypeface(font);
            tvdesc.setTypeface(font);
            tvusername.setTypeface(font);
            tvdate.setTypeface(font);
            tvlocation.setTypeface(font);
            tvdur.setTypeface(font);
            tvaddre.setTypeface(font);
            tvpay.setTypeface(font);
            tvamt.setTypeface(font);
            tvdt.setTypeface(font);
            tvcash.setTypeface(font);
            tvbal.setTypeface(font);

            location1.setTypeface(font);
            date1.setTypeface(font);
            username1.setTypeface(font);
            duration1.setTypeface(font);
            workerid1.setTypeface(font);
            description1.setTypeface(font);
            useraddress1.setTypeface(font);
        }
    }
}
