package com.demo.pepowworkerapp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.Emp_Data_Complt;
import java.util.List;

public class Emp_Completed_Adapter extends RecyclerView.Adapter<Emp_Completed_Adapter.Emp_CompletedViewHolder> {
    private LayoutInflater inflater;
    private List<Emp_Data_Complt> modelRecyclerArrayList;
    Context ctx;

    public Emp_Completed_Adapter(Context ctx,List<Emp_Data_Complt> modelRecyclerArrayList) {
        this.inflater = inflater.from(ctx);
        this.modelRecyclerArrayList = modelRecyclerArrayList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public Emp_Completed_Adapter.Emp_CompletedViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = inflater.inflate(R.layout.employment_completed_adapter, viewGroup, false);
        Emp_CompletedViewHolder viewHolder = new Emp_CompletedViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull Emp_Completed_Adapter.Emp_CompletedViewHolder holder, int i) {
        holder.location.setText(modelRecyclerArrayList.get(i).getLocaion());
        holder.workerid.setText(String.valueOf(modelRecyclerArrayList.get(i).getWork_order_id()));
        holder.description.setText(modelRecyclerArrayList.get(i).getDescription());
        holder.startdate.setText(modelRecyclerArrayList.get(i).getStart_date());
        holder.interviewdatetime.setText(modelRecyclerArrayList.get(i).getInterview_datetime());
        holder.status.setText(modelRecyclerArrayList.get(i).getStatus());
        if (modelRecyclerArrayList.get(i).getStatus().equalsIgnoreCase("Selected")){
            holder.status.setTextColor(Color.parseColor("#4ec477"));
           // holder.status.setText(modelRecyclerArrayList.get(i).getStatus());
        }else if (modelRecyclerArrayList.get(i).getStatus().equalsIgnoreCase("Rejected"))
        {
            holder.status.setTextColor(Color.parseColor("#c44e4e"));
        }
    }

    @Override
    public int getItemCount() {
        return modelRecyclerArrayList.size();
    }

    public class Emp_CompletedViewHolder extends RecyclerView.ViewHolder {
        TextView workerid,description,location,startdate,interviewdatetime,status,
                tvwid,tvdesc, tvloc,tvstartdate,tvenddate,tvstatus;
        public Emp_CompletedViewHolder(@NonNull View itemView) {
            super(itemView);
            Typeface font = Typeface.createFromAsset(ctx.getAssets(),"fonts/Uber Move Text.ttf");
            workerid=itemView.findViewById(R.id.empworkorderid2);
            description=itemView.findViewById(R.id.empdescription2);
            location=itemView.findViewById(R.id.emplocation2);
            startdate=itemView.findViewById(R.id.empdate2);
            interviewdatetime=itemView.findViewById(R.id.empinterviewdate_time2);
            status=itemView.findViewById(R.id.empstatus);

            tvwid=itemView.findViewById(R.id.w);
            tvdesc=itemView.findViewById(R.id.d);
            tvloc=itemView.findViewById(R.id.l);
            tvstartdate=itemView.findViewById(R.id.sd);
            tvenddate=itemView.findViewById(R.id.u);
            tvstatus=itemView.findViewById(R.id.st);

            tvwid.setTypeface(font);
            tvdesc.setTypeface(font);
            tvloc.setTypeface(font);
            tvstartdate.setTypeface(font);
            tvenddate.setTypeface(font);
            tvstatus.setTypeface(font);





            workerid.setTypeface(font);
            description.setTypeface(font);
            location.setTypeface(font);
            startdate.setTypeface(font);
            interviewdatetime.setTypeface(font);
            status.setTypeface(font);
        }
    }
}
