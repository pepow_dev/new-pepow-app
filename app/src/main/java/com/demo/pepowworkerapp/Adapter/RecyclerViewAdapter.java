package com.demo.pepowworkerapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.pepowworkerapp.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {
    private List<String> itemsName,itemsName1,itemsName2;
    private AdapterView.OnItemClickListener onItemClickListener;
    private LayoutInflater layoutInflater;


    public RecyclerViewAdapter(Context context, ArrayList<String> nameList, ArrayList<String> nameList1,ArrayList<String> nameList2){
        layoutInflater = LayoutInflater.from(context);
        itemsName = nameList;
        itemsName1=nameList1;
        itemsName2=nameList2;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.textview_info,parent, false);
        return new MyViewHolder(itemView, this);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.setItemName(itemsName.get(position));
        holder.setItemName1(itemsName1.get(position));
        holder.setItemName2(itemsName2.get(position));
    }

    @Override
    public int getItemCount() {
        return itemsName.size();

    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener listener){
        onItemClickListener = listener;
    }

    public AdapterView.OnItemClickListener getOnItemClickListener(){
        return onItemClickListener;
    }
    public interface OnItemClickListener{
        public void onItemClick(MyViewHolder item, int position);
    }



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RecyclerViewAdapter parent;
        TextView textItemName,textItemName1,textItemName2;
        public MyViewHolder(@NonNull View itemView, RecyclerViewAdapter recyclerViewAdapter) {
            super(itemView);
            itemView.setOnClickListener((View.OnClickListener) this);
            this.parent = parent;
            textItemName = (TextView) itemView.findViewById(R.id.tvyear);
            textItemName1 = (TextView) itemView.findViewById(R.id.tvwhre);
            textItemName2 = (TextView) itemView.findViewById(R.id.tv3);
        }
        public void setItemName(String s) {
            textItemName.setText("Year:-  "+s);

        }
        public void setItemName1(String s) {
            textItemName1.setText("Where:-  "+s);

        }
        public void setItemName2(String s) {
            textItemName2.setText("About Work:-  "+s);

        }


        public CharSequence getItemName(){
            return textItemName.getText();
        }
        public CharSequence getItemName1(){
            return textItemName1.getText();
        }
        public CharSequence getItemName2(){
            return textItemName2.getText();
        }


        @Override
        public void onClick(View view) {

        }
    }
}
