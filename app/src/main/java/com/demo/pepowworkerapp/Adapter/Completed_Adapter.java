package com.demo.pepowworkerapp.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.Data_Completed;
import com.demo.pepowworkerapp.Response.Data_Inprogress;

import java.util.List;

public class Completed_Adapter extends RecyclerView.Adapter<Completed_Adapter.CompletedViewHolder> {
    private LayoutInflater inflater;
    private List<Data_Completed> modelRecyclerArrayList;
    Context ctx;

    public Completed_Adapter(Context ctx, List<Data_Completed> modelRecyclerArrayList) {
        this.inflater = inflater.from(ctx);
        this.modelRecyclerArrayList = modelRecyclerArrayList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public CompletedViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = inflater.inflate(R.layout.completed_adapter, viewGroup, false);
        CompletedViewHolder holder = new CompletedViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CompletedViewHolder holder, int i) {
        holder.location2.setText(modelRecyclerArrayList.get(i).getLocaion());
        holder.date2.setText(modelRecyclerArrayList.get(i).getDate());
        holder.duration2.setText(modelRecyclerArrayList.get(i).getDuration());
        holder.username2.setText(modelRecyclerArrayList.get(i).getUser_name());
        holder.workerid2.setText(String.valueOf(modelRecyclerArrayList.get(i).getWork_order_id()));
        holder.description2.setText(modelRecyclerArrayList.get(i).getDescription());
        holder.useraddress2.setText(modelRecyclerArrayList.get(i).getUser_address());
        holder.amttt.setText(String.valueOf(modelRecyclerArrayList.get(i).getPaymentDetails().get(0).getAmount()));
        holder.blnc.setText(String.valueOf(modelRecyclerArrayList.get(i).getPaymentDetails().get(0).getBalance()));
        holder.dtt.setText(modelRecyclerArrayList.get(i).getPaymentDetails().get(0).getDate());
        holder.cs_on.setText(modelRecyclerArrayList.get(i).getPaymentDetails().get(0).getPayment_method());
    }

    @Override
    public int getItemCount() {
        return modelRecyclerArrayList.size();
    }

    public class CompletedViewHolder extends RecyclerView.ViewHolder {
        TextView location2,date2,username2,duration2,workerid2,description2,useraddress2
                ,tvorderid,tvdesc,tvusername,tvdate,tvlocation,tvdur,tvaddre,tvpay,tvamt,tvdt,tvcash,tvbal;
        TextView amttt,dtt,cs_on,blnc;
        public CompletedViewHolder(@NonNull View itemView) {
            super(itemView);
            Typeface font = Typeface.createFromAsset(ctx.getAssets(),"fonts/Uber Move Text.ttf");
            location2=itemView.findViewById(R.id.location2);
            date2=itemView.findViewById(R.id.date2);
            username2=itemView.findViewById(R.id.username2);
            duration2=itemView.findViewById(R.id.duration2);
            workerid2=itemView.findViewById(R.id.workorderid2);
            description2=itemView.findViewById(R.id.description2);
            useraddress2=itemView.findViewById(R.id.user_address2);

            tvorderid=itemView.findViewById(R.id.tvwrk);
            tvdesc=itemView.findViewById(R.id.tvdes);
            tvusername=itemView.findViewById(R.id.tvuser);
            tvdate= itemView.findViewById(R.id.tvd);
            tvlocation=itemView.findViewById(R.id.u);
            tvdur=itemView.findViewById(R.id.d);
            tvaddre=itemView.findViewById(R.id.a);
            tvpay=itemView.findViewById(R.id.pt);
            tvamt=itemView.findViewById(R.id.amt);
            tvdt=itemView.findViewById(R.id.dt);
            tvcash=itemView.findViewById(R.id.cash_online);
            tvbal=itemView.findViewById(R.id.Balance);

            amttt=itemView.findViewById(R.id.amttt);
            dtt=itemView.findViewById(R.id.dtt);
            cs_on=itemView.findViewById(R.id.cs_on);
            blnc=itemView.findViewById(R.id.blnc);

            amttt.setTypeface(font);
            dtt.setTypeface(font);
            cs_on.setTypeface(font);
            blnc.setTypeface(font);

            tvorderid.setTypeface(font);
            tvdesc.setTypeface(font);
            tvusername.setTypeface(font);
            tvdate.setTypeface(font);
            tvlocation.setTypeface(font);
            tvdur.setTypeface(font);
            tvaddre.setTypeface(font);
            tvpay.setTypeface(font);
            tvamt.setTypeface(font);
            tvdt.setTypeface(font);
            tvcash.setTypeface(font);
            tvbal.setTypeface(font);


            location2.setTypeface(font);
            date2.setTypeface(font);
            username2.setTypeface(font);
            duration2.setTypeface(font);
            workerid2.setTypeface(font);
            description2.setTypeface(font);
            useraddress2.setTypeface(font);
        }
    }
}
