package com.demo.pepowworkerapp.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.Data;

import java.util.List;

public class Schedule_Adapter extends RecyclerView.Adapter<Schedule_Adapter.ScheduleViewHolder> {
    private LayoutInflater inflater;
    private List<Data> modelRecyclerArrayList;
    Context ctx;

    public Schedule_Adapter(Context ctx,List<Data> modelRecyclerArrayList) {
        this.inflater = inflater.from(ctx);
        this.modelRecyclerArrayList = modelRecyclerArrayList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public Schedule_Adapter.ScheduleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.schedule_adapter, viewGroup, false);
        ScheduleViewHolder holder = new ScheduleViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final Schedule_Adapter.ScheduleViewHolder holder, int i) {

        holder.location.setText(modelRecyclerArrayList.get(i).getLocaion());
        holder.date.setText(modelRecyclerArrayList.get(i).getDate());
        holder.duration.setText(modelRecyclerArrayList.get(i).getDuration());
        holder.username.setText(modelRecyclerArrayList.get(i).getUser_name());
        holder.workerid.setText(String.valueOf(modelRecyclerArrayList.get(i).getWork_order_id()));
        holder.description.setText(modelRecyclerArrayList.get(i).getDescription());
        holder.useraddress.setText(modelRecyclerArrayList.get(i).getUser_address());
    }

    @Override
    public int getItemCount() {
        return modelRecyclerArrayList.size();
    }

    public class ScheduleViewHolder extends RecyclerView.ViewHolder {
        TextView location,date,username,duration,workerid,description,useraddress,w,w1,w3,w4,u,d,a;
        public ScheduleViewHolder(@NonNull View itemView) {
            super(itemView);
            Typeface font = Typeface.createFromAsset(ctx.getAssets(),"fonts/Uber Move Text.ttf");
         location=itemView.findViewById(R.id.location);
            date=itemView.findViewById(R.id.date);
            username=itemView.findViewById(R.id.username);
            duration=itemView.findViewById(R.id.duration);
            workerid=itemView.findViewById(R.id.workorderid);
            description=itemView.findViewById(R.id.description);
            useraddress=itemView.findViewById(R.id.user_address);

            w=itemView.findViewById(R.id.w);
            w1=itemView.findViewById(R.id.w1);
            w3=itemView.findViewById(R.id.w3);
            w4=itemView.findViewById(R.id.w4);
            u=itemView.findViewById(R.id.u);
            d=itemView.findViewById(R.id.d);
            a=itemView.findViewById(R.id.a);

            location.setTypeface(font);
            date.setTypeface(font);
            username.setTypeface(font);
            duration.setTypeface(font);
            workerid.setTypeface(font);
            description.setTypeface(font);
            useraddress.setTypeface(font);

            w.setTypeface(font);
            w1.setTypeface(font);
            w3.setTypeface(font);
            w4.setTypeface(font);
            u.setTypeface(font);
            d.setTypeface(font);
            a.setTypeface(font);
        }
    }
}
