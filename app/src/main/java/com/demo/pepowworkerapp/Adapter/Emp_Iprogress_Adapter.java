package com.demo.pepowworkerapp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.pepowworkerapp.R;
import com.demo.pepowworkerapp.Response.Emp_Data_Inpro;

import java.util.List;

public class Emp_Iprogress_Adapter extends RecyclerView.Adapter<Emp_Iprogress_Adapter.Emp_InprogressViewHolder> {
    private LayoutInflater inflater;
    private List<Emp_Data_Inpro> modelRecyclerArrayList;
    Context ctx;

    public Emp_Iprogress_Adapter(Context ctx,List<Emp_Data_Inpro> modelRecyclerArrayList) {
        this.inflater = inflater.from(ctx);
        this.modelRecyclerArrayList = modelRecyclerArrayList;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public Emp_Iprogress_Adapter.Emp_InprogressViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = inflater.inflate(R.layout.employment_inprogress_adapter, viewGroup, false);
        Emp_InprogressViewHolder viewHolder = new Emp_InprogressViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull Emp_Iprogress_Adapter.Emp_InprogressViewHolder holder, int i) {
        holder.location.setText(modelRecyclerArrayList.get(i).getLocaion());
        holder.workerid.setText(String.valueOf(modelRecyclerArrayList.get(i).getWork_order_id()));
        holder.description.setText(modelRecyclerArrayList.get(i).getDescription());
        holder.startdate.setText(modelRecyclerArrayList.get(i).getStart_date());
        holder.interviewdatetime.setText(modelRecyclerArrayList.get(i).getInterview_datetime());
        holder.status.setText(modelRecyclerArrayList.get(i).getStatus());
        if (modelRecyclerArrayList.get(i).getStatus().equalsIgnoreCase("Inprogress")){
            holder.status.setTextColor(Color.parseColor("#FFC300"));
            // holder.status.setText(modelRecyclerArrayList.get(i).getStatus());
        }else if (modelRecyclerArrayList.get(i).getStatus().equalsIgnoreCase("Scheduled"))
        {
            holder.status.setTextColor(Color.parseColor("#4ec477"));
        }


    }

    @Override
    public int getItemCount() {
        return modelRecyclerArrayList.size();
    }

    public class Emp_InprogressViewHolder extends RecyclerView.ViewHolder {
        TextView workerid,description,location,startdate,interviewdatetime,status,
                tvwid,tvdesc, tvloc,tvstartdate,tvenddate,tvstatus;
        public Emp_InprogressViewHolder(@NonNull View itemView) {
            super(itemView);
            Typeface font = Typeface.createFromAsset(ctx.getAssets(),"fonts/Uber Move Text.ttf");
            workerid=itemView.findViewById(R.id.empworkorderid1);
            description=itemView.findViewById(R.id.empdescription1);
            location=itemView.findViewById(R.id.emplocation1);
            startdate=itemView.findViewById(R.id.empdate1);
            interviewdatetime=itemView.findViewById(R.id.empinterviewdate_time1);
            status=itemView.findViewById(R.id.status);

            tvwid=itemView.findViewById(R.id.wi);
            tvdesc=itemView.findViewById(R.id.des);
            tvloc=itemView.findViewById(R.id.loc);
            tvstartdate=itemView.findViewById(R.id.sdt);
            tvenddate=itemView.findViewById(R.id.u);
            tvstatus=itemView.findViewById(R.id.st);

            tvwid.setTypeface(font);
            tvdesc.setTypeface(font);
            tvloc.setTypeface(font);
            tvstartdate.setTypeface(font);
            tvenddate.setTypeface(font);
            tvstatus.setTypeface(font);

            workerid.setTypeface(font);
            description.setTypeface(font);
            location.setTypeface(font);
            startdate.setTypeface(font);
            interviewdatetime.setTypeface(font);
            status.setTypeface(font);
        }
    }
}
