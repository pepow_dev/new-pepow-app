package com.demo.pepowworkerapp.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.ImageView;

public class SharedPreferenceClass {
    private static final String USER_PREFS = "SeekingDaddie";
    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor prefsEditor;

    String first_name,last_name,ph_no,adhar_num,address,dateofbirth,cheque,profile_pic,user_id;
    String imgid;

    // private String user_name = "user_name_prefs";
    // String user_id = "user_id_prefs";

    public SharedPreferenceClass(Context context)
    {
        this.appSharedPrefs = context.getSharedPreferences(USER_PREFS, Activity.MODE_PRIVATE);
        this.prefsEditor = appSharedPrefs.edit();
    }


    public String getValue_string(String stringKeyValue) {
        return appSharedPrefs.getString(stringKeyValue, "");
    }
    public void setValue_string(String stringKeyValue, String _stringValue) {

        prefsEditor.putString(stringKeyValue, _stringValue).commit();

    }

    public String getCheque() {
        return appSharedPrefs.getString("cheque",cheque);
    }

    public void setCheque(String cheque) {
        this.cheque = cheque;
        prefsEditor.putString("cheque",cheque);
        prefsEditor.commit();
    }

    public String getProfile_pic() {
        return appSharedPrefs.getString("profile_pic",profile_pic);
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
        prefsEditor.putString("profile_pic",profile_pic);
        prefsEditor.commit();
    }

    public void clearData()
    {
        prefsEditor.clear().commit();

    }


    public String getFirst_name() {
        return appSharedPrefs.getString("first_name",first_name);
    }

    public void setFirst_name(String first_name) {

        this.first_name = first_name;
        prefsEditor.putString("first_name",first_name);
        prefsEditor.commit();
    }

    public String getLast_name() {
        return appSharedPrefs.getString("last_name",last_name);
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
        prefsEditor.putString("last_name",last_name);
        prefsEditor.commit();
    }

    public String getPhone_num() {
        return appSharedPrefs.getString("ph_no",ph_no);
    }

    public void setPhone_num(String ph_no) {
        this.ph_no = ph_no;
        prefsEditor.putString("ph_no",ph_no);
        prefsEditor.commit();
    }

    public String getAdhar_num() {
        return appSharedPrefs.getString("adhaar_number",adhar_num);
    }

    public void setAdhar_num(String adhar_num) {
        this.adhar_num = adhar_num;
        prefsEditor.putString("adhaar_number",adhar_num);
        prefsEditor.commit();
    }

    public String getAddress() {
        return appSharedPrefs.getString("address",address);
    }

    public void setAddress(String address) {
        this.address = address;
        prefsEditor.putString("address",address);
        prefsEditor.commit();
    }

    public String getDateofbirth() {
        return appSharedPrefs.getString("date_of_birth",dateofbirth);
    }

    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
        prefsEditor.putString("date_of_birth",dateofbirth);
        prefsEditor.commit();
    }
}
